import * as aws from "aws-sdk";
import * as multer from "multer";
import * as multerS3 from "multer-s3";
import { get } from "config";
import { Router } from "express";

aws.config.update({
  accessKeyId: get("aws.staticImgs.config.acccessKeyId"),
  secretAccessKey: get("aws.staticImgs.config.secretAcessKey"),
});

const s3 = new aws.S3();
export const router = Router();
const imgObject = function (path) {
  return multer({
    storage: multerS3({
      s3,
      bucket: get("aws.staticImgs.bucketName") + path,
      acl: "public-read",
      metadata(req, file, cb) {
        cb(null, { fieldName: file.originalname });
      },
      key(req, file, cb) {
        cb(null, file.originalname);
      },
    }),
  });
};

export function uploadMiddleware(fieldName: string, path) {
  return imgObject(path).single(fieldName);
}
