import * as Joi from "@hapi/joi";
import { ValidatedRequestSchema, ContainerTypes } from "express-joi-validation";
import "joi-extract-type";

export const userNameSchema = Joi.object({
  userName: Joi.string().required(),
});

export interface tournamentSchema extends ValidatedRequestSchema {
  [ContainerTypes.Params]: Joi.extractType<typeof userNameSchema>;
}

export const emailSchema = Joi.object({
  email: Joi.string().email().required(),
});

export interface gamewiseLeaderbord extends ValidatedRequestSchema {
  [ContainerTypes.Params]: Joi.extractType<typeof emailSchema>;
}

export const uploadSchema = Joi.object({
  source: Joi.any(),
});

export interface UploadSchema extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Joi.extractType<typeof uploadSchema>;
}

export const registerUser = Joi.object({
  emailId: Joi.string().email().required(),
  password: Joi.string().required(),
  ProfileDisplayName: Joi.string().required(),
});

export interface AuthSchema extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Joi.extractType<typeof registerUser>;
}

export const loginSchema = Joi.object({
  emailId: Joi.string().required(),
  password: Joi.string().required(),
});

export interface AutloginSchema extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Joi.extractType<typeof loginSchema>;
}

export const generateSchema = Joi.object({
  email: Joi.string().email().required(),
});

export interface generateSchema extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Joi.extractType<typeof generateSchema>;
}

export const verifyEmailSchema = Joi.object({
  userName: Joi.string().required(),
  otp: Joi.string().required(),
});

export interface verifyEmailSchema extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Joi.extractType<typeof verifyEmailSchema>;
}

export const verifyOtpAndUserNameSchema = Joi.object({
  email: Joi.string().email().required(),
  otp: Joi.string().required(),
});

export interface verifyOtpAndUserNameSchema extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Joi.extractType<typeof verifyOtpAndUserNameSchema>;
}

export const changePasswordSchema = Joi.object({
  email: Joi.string().required(),
  otp: Joi.string().required(),
  password: Joi.string().required(),
});

export interface changePasswordSchema extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Joi.extractType<typeof changePasswordSchema>;
}

export const userProfileSchema = Joi.object({
  UserID: Joi.string().required(),
});

export interface userProfileSchema extends ValidatedRequestSchema {
  [ContainerTypes.Params]: Joi.extractType<typeof userProfileSchema>;
}

export const userProfileUpdateSteamSchema = Joi.object({
  UserID: Joi.string().required(),
  SteamID: Joi.string().required(),
  personaName: Joi.string().required().allow(""),
  realName: Joi.string().required().allow(""),
});

export interface userProfileUpdateSteamSchema extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Joi.extractType<typeof userProfileUpdateSteamSchema>;
}

export const userProfileUpdateSchema = Joi.object({
  UserID: Joi.string().required(),
  UserName: Joi.string().allow("").allow(null),
  ProfileDisplayName: Joi.string().allow("").allow(null),
  ProfilePictureUrl: Joi.string().allow("").allow(null),
  FacebookProfile: Joi.string().allow("").allow(null),
  TwitterProfile: Joi.string().allow("").allow(null),
  InstagramProfile: Joi.string().allow("").allow(null),
  SteamProfile: Joi.string().allow("").allow(null),
  DiscordProfile: Joi.string().allow("").allow(null),
  XboxProfile: Joi.string().allow("").allow(null),
  PsProfile: Joi.string().allow("").allow(null),
  About: Joi.string().allow("").allow(null),
  InGameUserID: Joi.number().allow("").allow(null),
});

export interface userProfileUpdateSchema extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Joi.extractType<typeof userProfileUpdateSchema>;
}

export const ProfileImageSchema = Joi.object({
  HeaderImageID: Joi.string().required().allow("").allow(null),
  ProfileImageID: Joi.string().required().allow("").allow(null),
});

export interface ProfileImageSchema extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Joi.extractType<typeof ProfileImageSchema>;
}

export const CountryandStateSchema = Joi.object({
  CountryNState: Joi.string().required().allow("").allow(null),
});

export interface CountryandStateSchema extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Joi.extractType<typeof ProfileImageSchema>;
}

export const UserProfileSchema1 = Joi.object({
  UserID: Joi.string().required(),
});

export interface UserProfileSchema extends ValidatedRequestSchema {
  [ContainerTypes.Params]: Joi.extractType<typeof UserProfileSchema1>;
}

export const UserNameUpdateSchema = Joi.object({
  ProfileDisplayName: Joi.string().required(),
});

export interface UserNameUpdateSchema extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Joi.extractType<typeof UserNameUpdateSchema>;
}


export const gameStringSchema = Joi.object({
  InGameString : Joi.string().required(),
});

export interface gameStringSchema extends ValidatedRequestSchema {
  [ContainerTypes.Body]: Joi.extractType<typeof gameStringSchema>;
}