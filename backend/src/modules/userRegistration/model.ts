import { Int, VarChar, Bit, Char, Numeric } from "mssql";
import { app } from "../../../server";
import { emailChecker } from "./controller";

class UserRegistration {
  async userNameChecker(userName: any) {
    // this function checking username is available or not
    try {
      const result = await app.pool
        .request()
        .query(
          `select 1 from common.Users u where u.UserName = '${userName}' COLLATE SQL_Latin1_General_Cp1_CS_AS`
        );
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          statuscode: 200,
          message: `userName found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `userName Not found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async userProfile(UserID: any) {
    // this function give userProfile data
    try {
      const result = await app.pool.request().query(
        `SELECT u.UserID, u.EmailID , u.UserName, u.ProfileDisplayName, u.ProfilePictureUrl
        , u.FacebookProfile, u.TwitterProfile, u.InstagramProfile
        , u.SteamProfile , u.DiscordProfile, u.XboxProfile, u.PsProfile
        , u.About, igud.InGameUserID, u.SteamID,u.InGameString
          FROM common.Users u
          LEFT OUTER JOIN common.InGameUserDetails igud
          ON u.UserID = igud.UserID
          WHERE u.UserID = '${UserID}'`
      );
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          statuscode: 200,
          message: `user profile found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `user profile Not found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async userProfileUpdatesteam(
    UserID: any,
    SteamID: any,
    personaName: any,
    realName: any
  ) {
    console.log("steam data ", UserID, SteamID, personaName, realName);
    // this function give userProfile data
    try {
      const result = await app.pool.request().query(
        ` UPDATE common.Users 
          SET SteamID = '${SteamID}'
          , UserName = '${personaName}'
          , RealName = '${realName}'
          WHERE UserID  = '${UserID}'`
      );
      console.log("result", result);
      if (result.rowsAffected[0] !== 0) {
        return {
          success: 1,
          statuscode: 200,
          message: `user profile updated`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `user profile Not found`,
        data: result.recordset,
      };
    } catch (err) {
      console.log(err);
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async emailChecker(email: any) {
    // this function checking emailChecker is available or not

    try {
      const result = await app.pool
        .request()
        .query(
          `select 1 from common.Users u where u.EmailID = '${email}' COLLATE SQL_Latin1_General_Cp1_CS_AS`
        );
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          statuscode: 200,
          message: `emailID found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `emailID Not found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async userProfileUpdate(data) {
    // this function userProfile data
    try {
      const result = await app.pool
        .request()
        .input("UserID", Int, data.UserID)
        .input("UserName", VarChar, data.UserName)
        .input("ProfileDisplayName", VarChar, data.ProfileDisplayName)
        .input("ProfilePictureUrl", VarChar, data.ProfilePictureUrl)
        .input("FacebookProfile", VarChar, data.FacebookProfile)
        .input("TwitterProfile", VarChar, data.TwitterProfile)
        .input("InstagramProfile", VarChar, data.InstagramProfile)
        .input("SteamProfile", VarChar, data.SteamProfile)
        .input("DiscordProfile", VarChar, data.DiscordProfile)
        .input("XboxProfile", VarChar, data.XboxProfile)
        .input("PsProfile", VarChar, data.PsProfile)
        .input("About", VarChar, data.About)
        .input("InGameUserID", Numeric, data.InGameUserID)
        .execute("common.crud_UsersUpdate");
      if (
        result.recordset.length !== null &&
        result.recordset.length !== 0 &&
        result.recordset[0].msg === "Record updated successfully!"
      ) {
        return {
          success: 1,
          statuscode: 200,
          message: `user profile updated `,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `User profile not found! `,
        data: result.recordset,
      };
    } catch (err) {
      console.log(err, "err");
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async countryNames() {
    // this function will give all country names data with id

    try {
      const result = await app.pool
        .request()
        .query(`select CountryID,CountryName from common.Country`);
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `data Not found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async login(userName: string) {
    // this function will give all country names data with id
    try {
      const result = await app.pool.request()
        .query(`SELECT UserID,EmailID,Pwd FROM common.Users WHERE UserName=ISNULL('${userName}','') COLLATE SQL_Latin1_General_Cp1_CS_AS OR
      EmailID='${userName}'`);
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `data Not found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async generateOtp(email: string, OTP: any) {
    // this function will generateOtp by email
    try {
      const result = await app.pool.request().query(`
      DECLARE @Val VARCHAR(256), @otp VARCHAR(6)
      SET @Val='${email}'
      SET @otp='${OTP}'
      UPDATE common.Users SET OTP=@otp WHERE EmailID=@Val
      SELECT @@ROWCOUNT AS 'RowCount'`);
      if (result.recordset[0].RowCount !== 1) {
        return {
          success: 1,
          statuscode: 404,
          message: `data not found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 200,
        message: `data found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
        data: [],
      };
    }
  }

  async verifyemail(userName: string, otp: string) {
    // this function will v1erifyemail
    try {
      const result = await app.pool.request().query(`
          UPDATE common.Users SET IsEmailVerified=1,OTP=NULL WHERE (UserName = '${userName}' OR EmailID = '${userName}') AND OTP= '${otp}' 
          SELECT @@ROWCOUNT AS  Result`);
      if (result.recordset.length !== 0 && result.recordset[0].Result !== 0) {
        return {
          success: 1,
          statuscode: 200,
          message: `email verified`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 403,
        message: `email not verified`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async verifyotp(email: string, otp: string) {
    // this function will verifyotp
    try {
      const result = await app.pool.request().query(
        `DECLARE @Val VARCHAR(256), @otp VARCHAR(6)
          SET @Val='${email}'
          SET @otp='${otp}'
          SELECT 1 AS Result FROM common.Users WHERE EmailID=@Val AND OTP=@otp`
      );
      if (result.recordset.length !== 0 && result.recordset[0].Result !== 0) {
        return {
          success: 1,
          statuscode: 200,
          message: `username and otp is correct`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 403,
        message: `username and otp is incorrect`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async changepasword(email: string, otp: string, password: string) {
    // this function will help to changepasword
    try {
      const result = await app.pool.request()
        .query(`DECLARE @Val VARCHAR(256), @otp VARCHAR(6), @pwd VARCHAR(2000)				
        SET @Val='${email}'
        SET @otp='${otp}'
        SET @pwd='${password}'
        UPDATE common.Users SET Pwd=@pwd, OTP = NULL WHERE EmailID=@Val AND OTP=@otp
        SELECT @@ROWCOUNT AS 'RowCount' `);
      if (result.recordset[0].RowCount !== 0) {
        return {
          success: 1,
          statuscode: 200,
          message: `Password updated successfully `,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 403,
        message: `username and otp is incorrect`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async userRegistration(EmailID, Pwd, ProfileDisplayName) {
    // this function will register user
    try {
      const result = await app.pool
        .request()
        .input("EmailID", VarChar, EmailID)
        .input("Pwd", VarChar, Pwd)
        .input("ProfileDisplayName", VarChar, ProfileDisplayName)
        .execute("common.crud_Users");
      if (
        result.recordset[0].msg === "Already email id present in the system!" &&
        result.recordset[0] !== null
      ) {
        return {
          success: 1,
          statuscode: 409,
          message: result.recordset[0].msg,
          data: result.recordset,
        };
      }
      if (
        result.recordset[0].msg === "Already username in the system!" &&
        result.recordset[0] !== null
      ) {
        return {
          success: 1,
          statuscode: 408,
          message: result.recordset[0].msg,
          data: result.recordset,
        };
      }
      if (
        result.recordset.length !== 0 &&
        result.recordset[0] !== null &&
        result.recordset[0].msg !== "Already email id present in the system!"
      ) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: result.recordset,
        };
      }
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async verifyemail1(email) {
    // this function will verifyotp
    try {
      const result = await app.pool.request().query(
        `UPDATE common.Users
          SET IsEmailVerified = 1
          WHERE EmailID = '${email}' `
      );
      if (result.rowsAffected[0] === 1) {
        return {
          success: 1,
          statuscode: 200,
          message: `your email is verified`,
          data: result.recordset,
        };
      }
      return {
        success: 0,
        statuscode: 403,
        message: `username and otp is incorrect`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async profile() {
    // this function will register user
    try {
      const result = await app.pool.request()
        .query(`SELECT ProfileImageID, ProfileImageURL
         FROM common.ProfileImages`);
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 400,
        message: `no data found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async header() {
    // this function will register user
    try {
      const result = await app.pool.request()
        .query(`SELECT HeaderImageID, HeaderImageURL
        FROM common.HeaderImages`);
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 400,
        message: `no data found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async headerimageUpdate(HeaderImageID, ProfileImageID, UserID) {
    // this function will register user
    try {
      const result = await app.pool.request()
        .query(`DECLARE @ProfileImageID INT,@HeaderImageID INT,@UserID INT
        UPDATE common.Users SET ProfileImageID='${ProfileImageID}',HeaderImageID='${HeaderImageID}' WHERE UserID='${UserID}'
        SELECT @@ROWCOUNT AS 'RowCount'`);
      console.log(result);
      if (
        result.recordset.length !== 0 &&
        result.recordset[0] !== null &&
        result.recordset[0].RowCount === 1
      ) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 400,
        message: `no data found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async CountryandState(CountryNState, UserID) {
    // this function will register user
    try {
      const result = await app.pool.request()
        .query(`DECLARE @tempTable TABLE (CountryNState VARCHAR(500))
        UPDATE common.Users
		SET CountryNState='${CountryNState}'
		OUTPUT inserted.CountryNState INTO @tempTable (CountryNState)
		WHERE UserID='${UserID}'
        SELECT @@ROWCOUNT AS 'RowCount' , CountryNState FROM @tempTable`);
      console.log(result);
      if (
        result.recordset.length !== 0 &&
        result.recordset[0] !== null &&
        result.recordset[0].RowCount === 1
      ) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 400,
        message: `no data found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async userProfileget(UserID) {
    // this function will register user
    try {
      const result = await app.pool.request().query(`SELECT u.UserID,
      u.UserName,u.ProfileDisplayName,u.ProfileImageID,p.ProfileImageURL,
      u.HeaderImageID, h.HeaderImageURL,u.CountryNState,u.SteamID,u.SteamProfile,u.InGameString
      FROM common.Users u
      LEFT OUTER JOIN common.ProfileImages p ON u.ProfileImageID=p.ProfileImageID
      LEFT OUTER JOIN common.HeaderImages h ON u.HeaderImageID=h.HeaderImageID
      WHERE u.UserID='${UserID}'`);
      if (
        result.recordset.length !== 0 &&
        result.recordset[0] !== null &&
        result.recordset.length === 1
      ) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 400,
        message: `no data found`,
        data: result.recordset,
      };
    } catch (err) {
      console.log(err);
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async userNameUpdate(UserID, ProfileDisplayName) {
    // this function will register user
    try {
      const result = await app.pool
        .request()
        .input("UserID", Int, UserID)
        .input("ProfileDisplayName", VarChar, ProfileDisplayName)
        .execute("common.crud_UpdateIELUserName");
      if (
        result.recordset[0].msg === "IELUserName already taken" && // 409
        result.recordset[0] !== null
      ) {
        return {
          success: 1,
          statuscode: 409,
          message: result.recordset[0].msg,
          data: result.recordset,
        };
      }
      if (
        result.recordset[0].msg === "No User with UserID in system'" && // 404
        result.recordset[0] !== null
      ) {
        return {
          success: 1,
          statuscode: 404,
          message: result.recordset[0].msg,
          data: result.recordset,
        };
      }
      if (
        result.recordset.length !== 0 &&
        result.recordset[0] !== null &&
        result.recordset[0].msg === "IEL Username updated successfuly" // 200
      ) {
        console.log("true 200");
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: result.recordset,
        };
      }
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async gameStringQuery(UserID,InGameString) {
    // this function userProfile data
    try {
      const result = await app.pool
        .request()
        .input("UserID", Int, UserID)
        .input("InGameString", VarChar, InGameString)
        .execute("common.crud_UpdateInGameString");
        return {
          success: 1,
          statuscode: 200,
          message: `user profile updated `,
          data: result.recordset,
        };
    } catch (err) {
      console.log(err, "err");
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }
}

export const userRegistrationMD = new UserRegistration();
