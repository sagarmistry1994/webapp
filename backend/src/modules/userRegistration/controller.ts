import { userRegistrationMD } from "./model";
import { encrypt } from "../../../src/middelware/encryptions/encryptions";
import {
  compare,
  generateToken,
  generateTokenfor60Min,
} from "../../../src/middelware/mix_Middelware/jwtgenerate";
import { get } from "config";
import { sendEmail } from "../../modules/email/controller";
import {
  normalResponse,
  internalSererErrorResponse,
} from "../../middelware/mix_Middelware/response";
import * as jwt from "jsonwebtoken";
import * as path from "path";
export const userNameChecker = async (req, res) => {
  //  This api chek username is available or not
  const userName = req.params.userName;
  const result = await userRegistrationMD.userNameChecker(userName);
  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const userProfile = async (req, res) => {
  //  This api get userProfile data
  const UserID = req.params.UserID;
  const result = await userRegistrationMD.userProfile(UserID);
  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const userProfileUpdatesteam = async (req, res) => {
  //  This api get userProfile data
  const SteamID = req.body.SteamID;
  console.log("SteamID", SteamID);

  const result = await userRegistrationMD.userProfileUpdatesteam(
    req.body.UserID,
    SteamID,
    req.body.personaName,
    req.body.realName
  );
  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const userProfileUpdate = async (req, res) => {
  //  This api update userProfile data

  const user = {
    UserID: parseInt(req.body.UserID),
    UserName: req.body.UserName,
    ProfileDisplayName: req.body.ProfileDisplayName,
    ProfilePictureUrl: req.body.ProfilePictureUrl,
    FacebookProfile: req.body.FacebookProfile,
    TwitterProfile: req.body.TwitterProfile,
    InstagramProfile: req.body.InstagramProfile,
    DiscordProfile: req.body.DiscordProfile,
    XboxProfile: req.body.XboxProfile,
    PsProfile: req.body.PsProfile,
    About: req.body.About,
    SteamProfile: req.body.SteamProfile,
    InGameUserID: parseInt(req.body.InGameUserID),
  };
  const result = await userRegistrationMD.userProfileUpdate(user);
  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const emailChecker = async (req, res) => {
  //  This api chek email is available or not
  const email = req.params.email;
  const result = await userRegistrationMD.emailChecker(email);

  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const countryNames = async (req, res) => {
  //  This api will give you country Name
  const result = await userRegistrationMD.countryNames();

  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const uploadBannerImg = async (req, res) => {
  res.send({ imageUrl: req.file, statuscode: 200 });
  return;
};

export const login = async (req, res) => {
  // will see in last
  //  This api will use for login
  const result = await userRegistrationMD.login(req.body.emailId);
  if (result.message === "data Not found") {
    return res.status(404).send({ message: "data Not found", statuscode: 404 });
  } else {
    const compareCondition = compare(req.body.password, result.data[0].Pwd);
    if (compareCondition === false) {
      return res.status(401).send({ message: "unauthorized", statuscode: 401 });
    }
    if (1 === result.success) {
      const userObj = {
        EmailID: result.data[0].EmailID,
        UserID: result.data[0].UserID,
      };
      const token = generateToken(userObj, get("auth.jwt.secret"));
      delete result.data[0].Pwd;
      return res.status(result.statuscode).json({
        data: result.data,
        message: result.message,
        success: result.success,
        token: token,
        statuscode: result.statuscode,
      });
    }
    return internalSererErrorResponse(req, res, result.statuscode, result);
  }
};

export const userRegistration = async (req, res) => {
  //  This api will registerUser
  const pwd = encrypt(req.body.password);
  const result = await userRegistrationMD.userRegistration(
    req.body.emailId,
    pwd,
    req.body.ProfileDisplayName
  );
  if (1 === result.success) {
    const userObj = {
      EmailID: req.body.emailId,
    };
    const token = generateTokenfor60Min(userObj, get("auth.jwt.secret"));
    if (200 === result.statuscode) {
      await sendEmail(
        req.body.emailId,
        "IEL E-mail Verification",
        `<div>
        <div>Hi there,</div>
        <div>&nbsp;</div>
        <div>Welcome to IEL, to start participating in exciting leagues please verify your email by clicking this link</div>
        <div>&nbsp;</div>
        <div><a href="https://backend.indieesportsleague.com/api/v1/users/token/${token}" target="_blank" rel="noopener">click here </a></div>
        <div>&nbsp;</div>
        <div><strong>Note: this link will be valid only for 2 Hours </strong></div>
        <div>&nbsp;</div>
        <div>Thanks</div>
        <div>IEL team</div>
        </div>`,
        get("sendgridEmailVerification.from")
      );
    }
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const generateOtp = async (req, res) => {
  const otp = Math.floor(1000 + Math.random() * 9000);
  const result = await userRegistrationMD.generateOtp(req.body.email, otp);
  if (1 === result.success && result.statuscode === 200) {
    await sendEmail(
      req.body.email,
      get("sendgridEmailVerification.subject"),
      `Yout OTP is ${otp}.`,
      get("sendgridEmailVerification.from")
    );
    console.log(result);
    return res.status(result.statuscode).json({
      data: result.data,
      message: `${req.body.email} and email sent on your register EmailID `,
      success: result.success,
      statuscode: result.statuscode,
    });
  } else if (result.statuscode === 404) {
    return res.status(result.statuscode).json({
      data: result.data,
      message: `data not found `,
      success: result.success,
      statuscode: result.statuscode,
    });
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const verifyemail = async (req, res) => {
  //  This api will verifyemail
  const result = await userRegistrationMD.verifyemail(
    req.body.userName,
    req.body.otp
  );
  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const verifyotp = async (req, res) => {
  //  This api will verifyotp
  const result = await userRegistrationMD.verifyotp(
    req.body.email,
    req.body.otp
  );
  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const changepasword = async (req, res) => {
  //  This api will changepasword
  const pwd = encrypt(req.body.password);
  const result = await userRegistrationMD.changepasword(
    req.body.email,
    req.body.otp,
    pwd
  );
  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const verifytoken = async (req, res) => {
  //  This api will verifytoken
  var decoded: any = jwt.decode(req.params.token, { complete: true });
  jwt.verify(req.params.token, get("auth.jwt.secret"), async function (
    err,
    decoded1
  ) {
    if (null === err) {
      let emailId = decoded.payload.EmailID;
      const result = await userRegistrationMD.verifyemail1(emailId);
      if (1 === result.success) {
        return res.sendFile(path.join(__dirname, "../../views/verified.html"));
      }
    }
    return res.sendFile(path.join(__dirname, "../../views/expired.html"));
  });
};

export const sendmail = async (req, res) => {
  const result = await sendEmail(
    "bhavesh@funddreamer.com",
    "TEST",
    "test",
    "bhavesh@funddreamer.com"
  );
};

export const profile = async (req, res) => {
  //  This api will give you country Name
  const result = await userRegistrationMD.profile();

  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const header = async (req, res) => {
  //  This api will give you country Name
  const result = await userRegistrationMD.header();
  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const headerimageUpdate = async (req, res) => {
  //  This api will changepasword
  const result = await userRegistrationMD.headerimageUpdate(
    req.body.HeaderImageID,
    req.body.ProfileImageID,
    req.user.UserID
  );
  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const CountryandState = async (req, res) => {
  //  This api will changepasword
  const result = await userRegistrationMD.CountryandState(
    req.body.CountryNState,
    req.user.UserID
  );
  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const userProfileget = async (req, res) => {
  //  This api will changepasword
  const result = await userRegistrationMD.userProfileget(req.params.UserID);
  if (1 === result.success) {
    delete result.data[0].UserName;
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const userNameUpdate = async (req, res) => {
  //  This api will changepasword
  const result = await userRegistrationMD.userNameUpdate(
    req.user.UserID,
    req.body.ProfileDisplayName
  );
  if (1 === result.success) {
    delete result.data[0].UserName;
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const gameString = async (req, res) => {
  //  This api get gameString data
  const result = await userRegistrationMD.gameStringQuery(
    req.user.UserID,
    req.body.InGameString
  );
  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};