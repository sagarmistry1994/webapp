import { Router } from "express";
import {
  PlayerByTournamentId,
  getFeaturedLeaderbord,
  gamewiseLeaderbord,
  newGamewiseLeaderbord,
  newLeaderbord,
  newGamewiseLeaderbord1,
} from "./controller";
import { createValidator } from "express-joi-validation";
import {
  tournamentSchema,
  gamewiseLeaderbordSchema,
  newGamewiseLeaderbordSchema,
} from "./req.validation";

export const createRouter = () => {
  const router = Router();
  const validator = createValidator();
  router.get(
    "/tournament/:tournamentId",
    [validator.params(tournamentSchema)],
    PlayerByTournamentId
  );
  router.get("/featuredtournament", getFeaturedLeaderbord);
  router.get(
    "/gamewise/:gameID/:sort",
    [validator.params(gamewiseLeaderbordSchema)],
    gamewiseLeaderbord
  );

  // new updated api 23/04/20 as per new flow
  router.get("/lb/:gameId", newGamewiseLeaderbord);

  // new updated api 22/06/20 as per new flow
  router.get("/lb/new/:levelId/:tournamentId/:gameId", newGamewiseLeaderbord1);

  router.get(
    "/lb/:ability/:NoOfRecords/:levelId/:gameId/:PageNumber",
    [validator.params(newGamewiseLeaderbordSchema)],
    newLeaderbord
  );

  return router;
};
