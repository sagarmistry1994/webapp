import { Int } from "mssql";
import { app } from "../../../server";

class Leaderboard {
  async get10PlayerByTournamentId(id) {
    // get top 10 player by tournament id
    try {
      const result = await app.pool
        .request()
        .input("TournamentID", Int, id)
        .execute("tournament.get_LeaderbordSummary");
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `no data found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async getFeaturedLeaderbord() {
    // get top 4 Featured Tournament Leaderbord
    try {
      const result = await app.pool
        .request()
        .execute("tournament.get_FeaturedLeaderbord");
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `no data found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async gamewiseLeaderbord(gameid, sort) {
    // get Leaderbord by gameid
    try {
      const result = await app.pool
        .request()
        .input("GameID", Int, gameid)
        .input("Sorting", Int, sort)
        .execute("tournament.get_GameWiseLeaderbord");
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `no data found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async imageData(pageValue) {
    // page value be line
    // 1 for landing page
    //  2 leader board by sort
    // 3 for detailed leaderboard
    try {
      const result = await app.pool
        .request()
        .query(
          `SELECT ImageUrl FROM common.WebPagewiseImages p INNER JOIN common.Images i ON p.ImageID=i.ImageID WHERE WebPageID=${pageValue}`
        );
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `no data found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async newGamewiseLeaderbord(GameID) {
    // get Leaderbord by gameid
    try {
      const result = await app.pool
        .request()
        .input("GameID", Int, GameID)
        .execute("common.get_AllLeaderboardInfo");
      let abc = result.recordset[0].DATA.filter(
        (value) => Object.keys(value).length !== 0
      );
      const level = await app.pool.request()
        .query(`SELECT LevelID AS levelId, LevelName AS levelName
        FROM common.GameLevels
        WHERE GameID=${GameID}`);
      let x = 0;
      abc.map((xyz) => {
        abc[x].levels = level.recordsets;
        x++;
      });
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: abc,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `no data found`,
        data: result.recordset[0].DATA,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async newGamewiseLeaderbord1(levelId, tournamentId, gameId) {
    // get Leaderbord by gameid
    try {
      const result = await app.pool
        .request()
        .input("LevelID", Int, levelId)
        .input("tournamentID", Int, tournamentId)
        .input("GameID", Int, gameId)
        .execute("common.get_AllLeaderboardInfo_LEVEL");
      console.log(result);
      let abc = result.recordset[0].DATA.filter(
        (value) => Object.keys(value).length !== 0
      );
      const level = await app.pool.request()
        .query(`SELECT LevelID AS levelId, LevelName AS levelName
        FROM common.GameLevels
        WHERE GameID=${gameId}`);
      let x = 0;
      abc.map((xyz) => {
        abc[x].levels = level.recordsets;
        x++;
      });
      if (
        result.recordset[0].DATA.length !== 0 &&
        result.recordset[0].DATA.length !== null
      ) {
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: abc,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `no data found`,
        data: result.recordset[0].DATA,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }

  async newDetailsLeaderbord(FilterCriteria, NoOfRecords, levelId, gameId,PageNumber) {
    // get Leaderbord by gameid
    try {
      const result = await app.pool
        .request()
        .input("FilterCriteria", Int, FilterCriteria)
        .input("NoOfRecords", Int, NoOfRecords)
        .input("levelID", Int, levelId)
        .input("GameID", Int, gameId)
        .input("PageNumber", Int, PageNumber)
        .execute("common.get_LeaderboardDetails");
      const level = await app.pool.request()
        .query(`SELECT LevelID AS levelId, LevelName AS levelName
          FROM common.GameLevels
          WHERE GameID=${gameId}`);
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        let resultDetials = result.recordset[0].DATA;
        resultDetials[0].levels = level.recordset;
        return {
          success: 1,
          statuscode: 200,
          message: `data found`,
          data: resultDetials,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `no data found`,
        data: result.recordset,
      };
    } catch (err) {
      console.log(err);
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }
}

export const leaderboardMd = new Leaderboard();
