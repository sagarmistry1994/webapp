import { leaderboardMd } from "./model";
import {
  withImageSendResponse,
  internalSererErrorResponse,
  normalResponse,
} from "../../middelware/mix_Middelware/response";

export const PlayerByTournamentId = async (req, res) => {
  //  This api will give top 10 player by tournament id
  const id = req.params.tournamentId;
  const result = await leaderboardMd.get10PlayerByTournamentId(id);
  const images = await leaderboardMd.imageData(3);
  if (1 === result.success) {
    return withImageSendResponse(req, res, result.statuscode, result, images);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const getFeaturedLeaderbord = async (req, res) => {
  //  This api will give featured tournament with 5 player details
  const result = await leaderboardMd.getFeaturedLeaderbord();
  const images = await leaderboardMd.imageData(1);
  if (1 === result.success) {
    return withImageSendResponse(req, res, result.statuscode, result, images);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const gamewiseLeaderbord = async (req, res) => {
  //  This api will gives you Leaderbord by gameid
  const gameId = req.params.gameID;
  const sort = req.params.sort;

  const result = await leaderboardMd.gamewiseLeaderbord(gameId, sort);
  const images = await leaderboardMd.imageData(2);
  if (1 === result.success) {
    return withImageSendResponse(req, res, result.statuscode, result, images);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const newGamewiseLeaderbord = async (req, res) => {
  //  This api will gives you Leaderbord by gameid
  const result = await leaderboardMd.newGamewiseLeaderbord(req.params.gameId);
  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const newGamewiseLeaderbord1 = async (req, res) => {
  //  This api will gives you Leaderbord by gameid
  const result = await leaderboardMd.newGamewiseLeaderbord1(
    req.params.levelId,
    req.params.tournamentId,
    req.params.gameId
  );
  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};

export const newLeaderbord = async (req, res) => {
  //  This api will gives you details Leaderbord by gameid
  const ability = req.params.ability;
  const NoOfRecords = req.params.NoOfRecords;
  const PageNumber = req.params.PageNumber;

  const result = await leaderboardMd.newDetailsLeaderbord(
    ability,
    NoOfRecords,
    req.params.levelId,
    req.params.gameId,
    PageNumber
  );
  console.log(result)
  if (1 === result.success) {
    return normalResponse(req, res, result.statuscode, result);
  }
  return internalSererErrorResponse(req, res, result.statuscode, result);
};
