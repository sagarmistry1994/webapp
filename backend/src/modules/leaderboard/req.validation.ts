import * as Joi from "@hapi/joi";
import { ValidatedRequestSchema, ContainerTypes } from "express-joi-validation";
import "joi-extract-type";

export const tournamentSchema = Joi.object({
  tournamentId: Joi.string().required(),
});

export interface tournamentSchema extends ValidatedRequestSchema {
  [ContainerTypes.Params]: Joi.extractType<typeof tournamentSchema>;
}

export const gamewiseLeaderbordSchema = Joi.object({
  gameID: Joi.string().required(),
  sort: Joi.string().required(),
});

export interface gamewiseLeaderbord extends ValidatedRequestSchema {
  [ContainerTypes.Params]: Joi.extractType<typeof gamewiseLeaderbordSchema>;
}

export const newGamewiseLeaderbordSchema = Joi.object({
  ability: Joi.number().min(1).max(45).required(),
  NoOfRecords: Joi.string().required(),
  levelId: Joi.string().required(),
  gameId: Joi.string().required(),
  PageNumber: Joi.string().required()
});

export interface newGamewiseLeaderbordSchema extends ValidatedRequestSchema {
  [ContainerTypes.Params]: Joi.extractType<typeof newGamewiseLeaderbordSchema>;
}
