import { app } from "../../../server";

class Game {
  async getGameData() {
    // this function will give all game data
    try {
      const result = await app.pool
        .request()
        .query("SELECT * from common.Games");
      if (result.recordset.length !== 0 && result.recordset[0] !== null) {
        return {
          success: 1,
          message: `data found`,
          statuscode: 200,
          data: result.recordset,
        };
      }
      return {
        success: 1,
        statuscode: 404,
        message: `no data found`,
        data: result.recordset,
      };
    } catch (err) {
      return {
        success: 0,
        statuscode: 500,
        message: `server error`,
      };
    }
  }
}

export const gameMd = new Game();
