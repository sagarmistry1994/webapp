﻿CREATE VIEW vw_BulletsHits
AS
SELECT TOP 10 1 AS Seq,'BulletsHits' AS BulletsHits,ROW_NUMBER() OVER (ORDER BY l.Score DESC) AS [Rank],u.ProfileDisplayName,l.BulletsHit,l.Score 
FROM common.leaderboard l
LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID
ORDER BY l.Score DESC