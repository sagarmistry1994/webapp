﻿CREATE PROCEDURE tournament.get_FeaturedLeaderbord
AS
BEGIN
	SELECT l.TournamentID,t.[Name] as TournamentName,
	JSON_QUERY
	(CONCAT	
		('[',
			(STRING_AGG
				(CONCAT
					('{', '"PlayerName": "', u.ProfileDisplayName, '"'
					, ',"Score": ', l.Score, ''
					, '}'
					)
				, ','
				) WITHIN GROUP (ORDER BY l.Score DESC)
			)
		, ']'
		)
	) AS players
FROM common.Leaderboard l
INNER JOIN common.Users u ON l.UserID=u.UserID
INNER JOIN tournament.Tournaments t ON l.TournamentID=t.TournamentID
WHERE t.IsTournamentFeatured=1 AND Rank<=5 
GROUP BY l.TournamentID,t.[Name]
ORDER BY l.TournamentID
FOR JSON PATH, INCLUDE_NULL_VALUES
END