﻿CREATE PROCEDURE tournament.get_LeaderbordSummary
(
	@TournamentID  INT
)
AS
BEGIN
	SELECT
	Rank,ProfileDisplayName AS Player,LastCompletedLevel,Time,
	BulletsTotal,BulletsHit,BulletHitPerc+'%' AS BulletHitPerc,Kills,Deaths,Score 
	FROM common.Leaderboard l
	INNER JOIN common.Users u ON l.UserID=u.UserID
	WHERE l.TournamentID=@TournamentID
	ORDER BY Rank
END