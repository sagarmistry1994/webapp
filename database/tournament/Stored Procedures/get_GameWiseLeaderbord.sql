﻿CREATE PROCEDURE tournament.get_GameWiseLeaderbord
(
	@GameID INT,
	@Sorting INT=1 --1=Weekly, 2=Monthly, 3=Yearly
)
AS
BEGIN
IF @Sorting=1
BEGIN
	SELECT l.TournamentID,t.[Name] as TournamentName,
	JSON_QUERY
	(CONCAT	
		('[',
			(STRING_AGG
				(CONCAT
					('{', '"PlayerName": "', u.ProfileDisplayName, '"'
					, ',"Score": ', l.Score, ''
					, '}'
					)
				, ','
				) WITHIN GROUP (ORDER BY l.Score DESC)
			)
		, ']'
		)
	) AS players 
	FROM common.Leaderboard l
	INNER JOIN common.Users u ON l.UserID=u.UserID
	INNER JOIN tournament.Tournaments t ON l.TournamentID=t.TournamentID
	WHERE l.GameID=@GameID AND Rank<=5 AND 
	CAST(EndTime AS DATE) >= dateadd(day, 2-datepart(dw, getdate()), CONVERT(date,getdate())) AND 
	CAST(EndTime AS DATE) <= dateadd(day, 8-datepart(dw, getdate()), CONVERT(date,getdate()))
	GROUP BY l.TournamentID,t.[Name]
	ORDER BY l.TournamentID
	FOR JSON PATH, INCLUDE_NULL_VALUES
END
ELSE IF @Sorting=2
BEGIN
	SELECT l.TournamentID,t.[Name] as TournamentName,
	JSON_QUERY
	(CONCAT	
		('[',
			(STRING_AGG
				(CONCAT
					('{', '"PlayerName": "', u.ProfileDisplayName, '"'
					, ',"Score": ', l.Score, ''
					, '}'
					)
				, ','
				) WITHIN GROUP (ORDER BY l.Score DESC)
			)
		, ']'
		)
	) AS players 
	FROM common.Leaderboard l
	INNER JOIN common.Users u ON l.UserID=u.UserID
	INNER JOIN tournament.Tournaments t ON l.TournamentID=t.TournamentID
	WHERE l.GameID=@GameID AND Rank<=5 AND CAST(EndTime AS DATE) BETWEEN CAST(DATEADD(dd,-(DAY(GETDATE())-1),GETDATE()) AS DATE) AND
	CAST(DATEADD(dd,-(DAY(DATEADD(mm,1,GETDATE()))),DATEADD(mm,1,GETDATE())) AS DATE)
	GROUP BY l.TournamentID,t.[Name]
	ORDER BY l.TournamentID
	FOR JSON PATH, INCLUDE_NULL_VALUES
END
ELSE
BEGIN
	SELECT l.TournamentID,t.[Name] as TournamentName,
	JSON_QUERY
	(CONCAT	
		('[',
			(STRING_AGG
				(CONCAT
					('{', '"PlayerName": "', u.ProfileDisplayName, '"'
					, ',"Score": ', l.Score, ''
					, '}'
					)
				, ','
				) WITHIN GROUP (ORDER BY l.Score DESC)
			)
		, ']'
		)
	) AS players  
	FROM common.Leaderboard l
	INNER JOIN common.Users u ON l.UserID=u.UserID
	INNER JOIN tournament.Tournaments t ON l.TournamentID=t.TournamentID
	WHERE l.GameID=@GameID AND Rank<=5 AND YEAR(EndTime)=YEAR(GETDATE())
	GROUP BY l.TournamentID,t.[Name]
	ORDER BY l.TournamentID
	FOR JSON PATH, INCLUDE_NULL_VALUES
END
END