﻿CREATE TABLE [dblog].[CRUDErrors] (
    [CRUDErrorID]        BIGINT         IDENTITY (1, 1) NOT NULL,
    [CallerUserID]       INT            NULL,
    [CalledSpName]       VARCHAR (1000) NULL,
    [SpParameter_String] NVARCHAR (MAX) NULL,
    [SpErrorMsg]         NVARCHAR (MAX) NULL,
    [SpCalledOn]         DATETIME       CONSTRAINT [DF_dtl_CRUDErrors] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CRUDErrors] PRIMARY KEY CLUSTERED ([CRUDErrorID] ASC)
);

