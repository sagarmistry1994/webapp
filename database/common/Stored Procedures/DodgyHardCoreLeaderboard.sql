﻿CREATE PROCEDURE common.DodgyHardCoreLeaderboard
(
	@Level VARCHAR(500)='SpaceLevel 1'
)
AS
BEGIN
	DECLARE @DodgyHardCoreJson NVARCHAR(MAX) = ''

	IF OBJECT_ID('tempdb..#DodgyHardCore') IS NOT NULL
	BEGIN
		DROP TABLE #DodgyHardCore
	END

	CREATE TABLE #DodgyHardCore
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Score INT NOT NULL
	)

	INSERT INTO #DodgyHardCore
	SELECT TOP 25 
	1 ,'DodgyMcDodgersonHardCore',
	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Score) AS Score 
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON  l.UserName = u.SteamID or l.UserID=u.UserID
	WHERE l.LastCompletedLevel=@Level AND l.isHardcore=1 AND l.hasBeenHit=0
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY MAX(l.Score) DESC

	SELECT @DodgyHardCoreJson = 
	(
	SELECT v.[Name]
	, 17 AS 'tournamentID'
	, 'Score' AS columnName
	, 'Score' AS description
	, 'DODGY McDODGERSON (HardCore)' AS title
	,	JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{', '"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Score": ', v.Score, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Score DESC)
				)
			, ']'
			) 
		) AS players
	FROM #DodgyHardCore v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)

	SELECT
    JSON_QUERY(
    CONCAT(
    '[' 
	, JSON_QUERY(@DodgyHardCoreJson)
    ,']')) 
	AS DATA
    FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
END