﻿
CREATE PROCEDURE [common].[crud_otp] (
	@userName VARCHAR(256)='',
	@value INT -- 1=email verification, 2=forgot pwd
)
AS 
BEGIN
BEGIN TRY
	DECLARE @msgCode INT = 0
	DECLARE @msg VARCHAR(2000) = ''
	DECLARE @upd_cnt INT = 0
	DECLARE @upd_fgtpwd_cnt INT = 0
	DECLARE @emailID VARCHAR(256) = ''
	DECLARE @isemailverified BIT
	
	SELECT @emailID=EmailID FROM common.Users WHERE UserName=@userName OR EmailID=@userName

	SELECT @isemailverified=IsEmailVerified FROM common.Users WHERE UserName=@userName OR EmailID=@userName
	
	IF (@emailID='')
	BEGIN
		SELECT @msg='Username/Email not found',@msgCode=1
		GOTO OUTMSG
	END

	IF @value=1 AND @isemailverified=1
	BEGIN
		SELECT @msg='Already email has been verified',@msgCode=1
		GOTO OUTMSG
	END

	IF @value NOT IN(1,2)
	BEGIN
		SELECT @msg='Valid parameter value should be pass',@msgCode=1
		GOTO OUTMSG
	END
	
	BEGIN
		BEGIN TRANSACTION	
		
		DECLARE @OTP INT = convert(numeric(6),rand() * 999999) + 100000
		UPDATE common.Users 
		SET OTP=@OTP
		WHERE UserName = @userName OR EmailID = @userName
		SELECT @upd_cnt=@@ROWCOUNT
	
		IF @upd_cnt=1
			BEGIN
				COMMIT TRANSACTION
				SELECT @msg='OTP generated succesfuly'
				GOTO OUTMSG
			END
		ELSE
			BEGIN
				SELECT @msg = CONCAT('Something''s wrong while updating record. Record count is ', @upd_cnt, '!');
				THROW 51000, @msg, 1;
			END
			
		OUTMSG:
			SELECT @msg AS msg, @msgCode AS msgCode, @OTP AS OTP, @emailID AS EmailID
		
	END
	
	
	
END TRY	
BEGIN CATCH
IF @@TRANCOUNT > 0
	ROLLBACK TRANSACTION

	DECLARE @CRUDErrorsID INT = 0
	INSERT INTO dblog.CRUDErrors(CallerUserID, CalledSpName, SpParameter_String, SpErrorMsg, SpCalledOn) 
	SELECT 1,'common.crud_otp', CONCAT( '@userName', ' = ''', @userName,''''
	)
	, ERROR_MESSAGE(), GETDATE()
	SELECT @CRUDErrorsID = SCOPE_IDENTITY()

	SELECT CONCAT('Awww snap.. something went wrong. Please contact administrator or try again later. Error reference ID - ', @CRUDErrorsID, '!') AS msg
	, 2 AS msgCode
END CATCH	
END