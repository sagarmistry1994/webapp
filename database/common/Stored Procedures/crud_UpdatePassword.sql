﻿
CREATE PROCEDURE [common].[crud_UpdatePassword]
(
	@UserNameEmail VARCHAR(200),
	@Pwd VARCHAR(2000),
	@OTP VARCHAR(6)
)
AS 
BEGIN
BEGIN TRY

	DECLARE @MsgCode INT = 0
	DECLARE @Msg VARCHAR(2000) = ''
	DECLARE @upd_cnt INT = 0

	IF (@Pwd=NULL OR @Pwd='')
	BEGIN
		SELECT @Msg='Password should not be blank or null!',@MsgCode=1
		GOTO OUTMSG
	END

	BEGIN
		BEGIN TRANSACTION

			UPDATE common.Users SET Pwd=@Pwd,OTP=NULL WHERE (UserName =@UserNameEmail OR EmailID =@UserNameEmail) AND OTP=@OTP 
			SELECT @upd_cnt=@@ROWCOUNT
	
		IF @upd_cnt=1
			BEGIN
				COMMIT TRANSACTION
				SELECT @Msg='Password updated successfully'
				GOTO OUTMSG
			END
		ELSE
			BEGIN
				SELECT @Msg = CONCAT('Something''s wrong while updating record. Record count is ', @upd_cnt, '!');
				THROW 51000, @Msg, 1;
			END
			
		OUTMSG:
			SELECT @Msg AS Msg, @msgCode AS MsgCode, @UserNameEmail AS UserNameEmail
	END
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
	ROLLBACK TRANSACTION

	DECLARE @CRUDErrorsID INT = 0
	INSERT INTO dblog.CRUDErrors(CallerUserID, CalledSpName, SpParameter_String, SpErrorMsg, SpCalledOn) 
	SELECT 1,'ccommon.crud_UpdatePassword', CONCAT( '@UserNameEmail', ' = ''', @UserNameEmail,''','
	, '@Pwd', ' = ''', @Pwd,''', '
	, '@OTP', ' = ''', @OTP,''''
	)
	, ERROR_MESSAGE(), GETDATE()
	SELECT @CRUDErrorsID = SCOPE_IDENTITY()

	SELECT CONCAT('Awww snap.. something went wrong. Please contact administrator or try again later. Error reference ID - ', @CRUDErrorsID, '!') AS Msg
	, 2 AS MsgCode
END CATCH	
END