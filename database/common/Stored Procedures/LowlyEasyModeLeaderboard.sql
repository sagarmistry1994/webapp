﻿CREATE PROCEDURE common.LowlyEasyModeLeaderboard
(
	@Level VARCHAR(500)='SpaceLevel 1'
)
AS
BEGIN
	DECLARE @LowlyEazyModeJson NVARCHAR(MAX) = ''

	IF OBJECT_ID('tempdb..#TheLowlyEazyMode') IS NOT NULL
	BEGIN
		DROP TABLE #TheLowlyEazyMode
	END

	CREATE TABLE #TheLowlyEazyMode
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Score INT NOT NULL
	)

	INSERT INTO #TheLowlyEazyMode
	SELECT TOP 25 
	1 ,'TheLowlyEazyMode',
	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MIN(l.Score) AS Score 
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON  l.UserName = u.SteamID or l.UserID=u.UserID
	WHERE l.LastCompletedLevel=@Level AND isEazyMode=1
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY MIN(l.Score) ASC

	SELECT @LowlyEazyModeJson = 
	(
	SELECT v.[Name]
	, 14 AS 'tournamentID'
	, 'Score' AS columnName
	, 'Score' AS description
	, 'THE LOWLY (EazyMode)' AS title
	,	JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{', '"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Score": ', v.Score, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Score ASC)
				)
			, ']'
			) 
		) AS players
	FROM #TheLowlyEazyMode v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)

	SELECT
    JSON_QUERY(
    CONCAT(
    '[' 
	, JSON_QUERY(@LowlyEazyModeJson)
    ,']')) 
	AS DATA
    FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
END