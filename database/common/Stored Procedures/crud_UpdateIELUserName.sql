﻿CREATE PROCEDURE [common].[crud_UpdateIELUserName]
	@UserID INT,
	@ProfileDisplayName VARCHAR(200)
AS
BEGIN
BEGIN TRY
	DECLARE @msg VARCHAR(2000) = ''
	DECLARE @msgCode TINYINT = 0
	DECLARE @usr_upd_cnt TINYINT = 0

	BEGIN
		IF NOT EXISTS (SELECT 1 FROM common.Users WHERE UserID=@UserID)
			BEGIN
				SELECT @msg = 'No User with UserID in system', @msgCode=1
				GOTO OUTMSG
			END
		IF EXISTS (SELECT 1 FROM common.Users WHERE ProfileDisplayName=@ProfileDisplayName AND UserID <> @UserID) --COLLATE SQL_Latin1_General_CP1_CS_AS
			BEGIN
				SELECT @msg = 'IELUserName already taken', @msgCode=1
				GOTO OUTMSG
			END
	END

	BEGIN TRANSACTION
		
	BEGIN	
		--TO update profile display name ->query are as given below
		UPDATE common.Users
		SET ProfileDisplayName=@ProfileDisplayName
		WHERE UserID=@UserID
		SELECT @usr_upd_cnt=@@ROWCOUNT

		IF @usr_upd_cnt = 1
			BEGIN
				SELECT @msg='IEL Username updated successfuly', @msgCode=0
				COMMIT TRANSACTION
			END
		ELSE IF @usr_upd_cnt > 1
			BEGIN
				SELECT @msg = CONCAT('Something''s wrong while inserting record. New UserID is ', @UserID, '!');
				THROW 51000, @msg, 1;
			END

		OUTMSG:
			SELECT @msg AS msg, @msgCode as msgCode, @UserID as UserID, @ProfileDisplayName as ProfileDisplayName
	END
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
	ROLLBACK TRANSACTION

	DECLARE @CRUDErrorsID INT = 0
	INSERT INTO dblog.CRUDErrors(CallerUserID, CalledSpName, SpParameter_String, SpErrorMsg, SpCalledOn) 
	SELECT 1,'common.crud_UpdateIELUserName'
	, CONCAT( '@UserID', ' = ''', @UserID,''','
	, '@ProfileDisplayName', ' = ''', @ProfileDisplayName,''', '
	)
	, ERROR_MESSAGE(), GETDATE()
	SELECT @CRUDErrorsID = SCOPE_IDENTITY()

	SELECT CONCAT('Awww snap.. something went wrong. Please contact administrator or try again later. Error reference ID - ', @CRUDErrorsID, '!') AS msg
	, 2 AS msgCode
END CATCH

END