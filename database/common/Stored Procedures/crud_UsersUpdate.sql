﻿CREATE PROCEDURE [common].[crud_UsersUpdate]
(
	@UserID INT=0,
	@UserName VARCHAR(200) = '',
	@ProfileDisplayName VARCHAR(200) ='',
	@ProfilePictureUrl VARCHAR(3000)='',
	@FacebookProfile VARCHAR(500)='',
	@TwitterProfile VARCHAR(500)='',
	@InstagramProfile VARCHAR(500)='',
	@SteamProfile VARCHAR(500)='',
	@DiscordProfile VARCHAR(500)='',
	@XboxProfile VARCHAR(500)='',
	@PsProfile VARCHAR(500)='',
	@About VARCHAR(2000)='',
	@InGameUserID NUMERIC(20)=0
)
AS
BEGIN
SET NOCOUNT ON

DECLARE @usr_ins_cnt INT = 0
DECLARE @usr_upd_cnt INT = 0
DECLARE @UserIDNew INT = 0
DECLARE @ErrMsg VARCHAR(MAX)
DECLARE @MaxUserID INT
DECLARE @IsHiddenEmailID BIT=0
DECLARE @Gender CHAR(1)=''
DECLARE	@CountryID SMALLINT=0
DECLARE	@GooglePlusProfile VARCHAR(500)=''
DECLARE	@WebsiteUrl VARCHAR(2048)=''
DECLARE	@IsEmailVerified BIT=0
DECLARE	@StateID INT=0
DECLARE @EmailID VARCHAR(256)=''
DECLARE	@Pwd VARCHAR(2000)=''
DECLARE @SteamID NUMERIC(20)=0
DECLARE @TempTable TABLE(SteamID NUMERIC(20))
DECLARE	@action CHAR(1)='U' --'I'=Insert ,-'U'=Update

BEGIN TRY
	BEGIN -- Validation Starts
		DECLARE @SameProfileDisplayName INT,@SameEmailID INT,@Email VARCHAR(256)

		SET @Email=(SELECT EmailID FROM common.Users WHERE UserID=@UserID)
		
		SET @SameProfileDisplayName=(SELECT COUNT(ProfileDisplayName) FROM common.Users WHERE ProfileDisplayName=@ProfileDisplayName COLLATE SQL_Latin1_General_Cp1_CS_AS AND UserID<>@UserID)
		
		SET @SameEmailID=(SELECT COUNT(EmailID) FROM common.Users WHERE EmailID=LOWER(@EmailID) AND UserID<>@UserID)
		
		IF @SameProfileDisplayName=1
		BEGIN
			SELECT 'ProfileDisplayName already in the system!' AS msg, 1 AS msgCode, @UserID AS UserID
			RETURN
		END

		IF @SameEmailID=1
		BEGIN
			SELECT 'Already email id present in the system!' AS msg, 1 AS msgCode, @UserID AS UserID
			RETURN
		END
	END ----Validation Ends
	BEGIN
		BEGIN TRANSACTION
			IF @action='U'
			BEGIN
				--Updation of common.Users table
				UPDATE u
				SET 
				--UserName=@UserName
				ProfileDisplayName=@ProfileDisplayName
				,ProfilePictureUrl=@ProfilePictureUrl
				,FacebookProfile=@FacebookProfile
				,TwitterProfile=@TwitterProfile
				,InstagramProfile=@InstagramProfile
				,SteamProfile=@SteamProfile
				,DiscordProfile=@DiscordProfile
				,XboxProfile=@XboxProfile
				,PsProfile=@PsProfile
				,About=@About
                OUTPUT INSERTED.SteamID INTO @TempTable
				FROM common.Users u
                WHERE UserID=@UserID
				SELECT @usr_upd_cnt = @@ROWCOUNT

                SET @SteamID = (SELECT SteamID FROM @TempTable)
	
				IF @usr_upd_cnt > 1
				BEGIN
					SELECT @ErrMsg = CONCAT('Something''s went wrong while updating record. Record count is ', @usr_upd_cnt, '!');
					THROW 51000, @ErrMsg, 1;
				END
				IF @usr_upd_cnt = 1
					BEGIN
						SELECT 'Record updated successfully!' AS msg, 0 AS msgCode, @UserID AS UserID,
						@Email AS EmailID,
						@UserName AS UserName,
						@ProfileDisplayName AS ProfileDisplayName,
						@ProfilePictureUrl AS ProfilePictureUrl,
						@FacebookProfile AS FacebookProfile,
						@TwitterProfile AS TwitterProfile,
						@InstagramProfile AS InstagramProfile, 
						@SteamProfile AS SteamProfile,
						@DiscordProfile AS DiscordProfile, 
						@XboxProfile AS XboxProfile,
						@PsProfile AS PsProfile,
						@About AS About,
						@InGameUserID AS InGameUserID,
                        @SteamID AS SteamID
					END
				IF @usr_upd_cnt = 0
					BEGIN
						SELECT 'UserID not found!' AS msg, 1 AS msgCode, @UserID AS UserID
					END
	
			END
		COMMIT TRANSACTION
	END
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
	ROLLBACK TRANSACTION
	DECLARE @CRUDErrorsID INT = 0
	INSERT INTO dblog.CRUDErrors(CallerUserID, CalledSpName, SpParameter_String, SpErrorMsg, SpCalledOn) 
	SELECT 1,'common.crud_Users', CONCAT( '@UserID', ' = ''', @UserID,''', '
	, '@action', ' = ''', @action,''', '
	, '@UserName', ' = ''', @UserName,''', '
	, '@EmailID', ' = ''', @EmailID,''', '
	, '@IsHiddenEmailID', ' = ''', @IsHiddenEmailID ,''', '
	, '@Pwd', ' = ''', @Pwd,''', '
	, '@ProfileDisplayName', ' = ''', @ProfileDisplayName,''', '
	, '@ProfilePictureUrl', ' = ''', @ProfilePictureUrl ,''', '
	, '@Gender', ' = ''', @Gender ,''', '
	, '@CountryID', ' = ''', @CountryID ,''', '
	, '@FacebookProfile', ' = ''', @FacebookProfile ,''', '
	, '@TwitterProfile', ' = ''', @TwitterProfile ,''', '
	, '@WebsiteUrl', ' = ''', @WebsiteUrl ,''', '
	, '@IsEmailVerified', ' = ''', @IsEmailVerified,''''
	)
	, ERROR_MESSAGE(), GETDATE()
	SELECT @CRUDErrorsID = SCOPE_IDENTITY()

	SELECT CONCAT('Awww snap.. something went wrong. Please contact administrator or try again later. Error reference ID - ', @CRUDErrorsID, '!') AS msg
	, 2 AS msgCode, @UserID AS UserID
END CATCH
END