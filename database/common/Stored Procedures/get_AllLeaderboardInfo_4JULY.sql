﻿CREATE PROCEDURE [common].[get_AllLeaderboardInfo_4JULY]
AS
BEGIN

	DECLARE @HighScoreHardCoreJson NVARCHAR(MAX) = ''
	DECLARE @HighScoreEazyModeJson NVARCHAR(MAX) = ''
	DECLARE @HighScoreSkilledModeJson NVARCHAR(MAX) = ''

	DECLARE @RichestHardCoreJson NVARCHAR(MAX) = ''
	DECLARE @RichestEasyModeJson NVARCHAR(MAX) = ''
	DECLARE @RichestSkilledModeJson NVARCHAR(MAX) = ''

    DECLARE @KDRHardCoreJson NVARCHAR(MAX) = ''
    DECLARE @KDRSkilledModeJson NVARCHAR(MAX) = ''
    DECLARE @KDREasyModeJson NVARCHAR(MAX) = ''

    DECLARE @DeathScoreEasyModeJson NVARCHAR(MAX) = ''
    DECLARE @DeathScoreSkilledModeJson NVARCHAR(MAX) = ''

	DECLARE @DeathlessHardCoreJson NVARCHAR(MAX) =''
	DECLARE @StingyBastardHCJson NVARCHAR(MAX) = ''

	--hasLevel=1
	DECLARE @DodgyHardCoreJson NVARCHAR(MAX) = '' 
	DECLARE @LowlyEazyModeJson NVARCHAR(MAX) = ''
    DECLARE @MotherlodeEasyModeJson NVARCHAR(MAX) = ''
    DECLARE @MotherlodeSkilledJson NVARCHAR(MAX) = ''
    DECLARE @MotherlodeHardCoreJson NVARCHAR(MAX) = ''

	--column json
	DECLARE @HighScoreColumns NVARCHAR(MAX) =  '[{"columnName":"#","description":"#"},{"columnName":"PlayerName","description":"Player"},{"columnName":"Score","description":"Score"}]'
	DECLARE @RichestColumns NVARCHAR(MAX) =  '[{"columnName":"#","description":"#"},{"columnName":"PlayerName","description":"Player"},{"columnName":"Cash","description":"Cash"}]'
	DECLARE @DeathlessColumns NVARCHAR(MAX) = '[{"columnName":"#","description":"#"},{"columnName":"PlayerName","description":"Player"},{"columnName":"Score","description":"Score"}]'
	DECLARE @StingyBastardColumns NVARCHAR(MAX) = '[{"columnName":"#","description":"#"},{"columnName":"PlayerName","description":"Player"},{"columnName":"Cash","description":"Cash"}]'
	DECLARE @DodgyColumns NVARCHAR(MAX) = '[{"columnName":"#","description":"#"},{"columnName":"PlayerName","description":"Player"},{"columnName":"Score","description":"Score"}]' 
	DECLARE @LowlyColumns NVARCHAR(MAX) = '[{"columnName":"#","description":"#"},{"columnName":"PlayerName","description":"Player"},{"columnName":"Score","description":"Score"}]' 
    DECLARE @KDRColumns NVARCHAR(MAX) = '[{"columnName":"#","description":"#"},{"columnName":"PlayerName","description":"Player"},{"columnName":"Kills","description":"Kills"},{"columnName":"Deaths","description":"Deaths"},{"columnName":"KDR","description":"KDR"}]'
    DECLARE @DeathScoreColumns NVARCHAR(MAX) = '[{"columnName":"#","description":"#"},{"columnName":"PlayerName","description":"Player"},{"columnName":"Score","description":"Score"},{"columnName":"Deaths","description":"Deaths"},{"columnName":"DeathScore","description":"Score/Death Ratio"}]'
    DECLARE @MotherlodeColumns NVARCHAR(MAX) = '[{"columnName":"#","description":"#"},{"columnName":"PlayerName","description":"Player"},{"columnName":"Pickups","description":"Pickups"},{"columnName":"Cash","description":"Cash"},{"columnName":"Total","description":"Total"}]'


	DECLARE @Level VARCHAR(100) = ''

	SELECT @Level = gl.LevelName
	FROM common.GameLevels gl
	WHERE gl.GameID=1 AND gl.LevelID=1

	


--High Score

	IF OBJECT_ID('tempdb..#HighScoreHardCore') IS NOT NULL
	BEGIN
		DROP TABLE #HighScoreHardCore
	END

	CREATE TABLE #HighScoreHardCore
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Score INT NOT NULL
	)
	INSERT INTO #HighScoreHardCore
	SELECT TOP 25 
	ROW_NUMBER() OVER(ORDER BY MAX(l.Score) DESC) AS Seq,'HighScoreHardCore',
	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Score) AS Score  
	FROM (
    SELECT MAX(l.Score) as Score, MAX(l.UserID) as UserID, MAX(l.UserName) as UserName
    FROM common.Leaderboard l
    WHERE l.isHardcore=1
    GROUP BY l.UserName, l.LastCompletedLevel
	) l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY MAX(l.Score) DESC

	SELECT @HighScoreHardCoreJson = 
	(
	SELECT v.[Name]
	, 1 AS 'tournamentID'
	, 'Score' AS columnName
	, 'Score' AS description
	, JSON_QUERY(@HighScoreColumns) AS 'columns'
	, 'High Score (HardCore)' AS title
	, 'Highest score' AS subtitle
	,  CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{'
						, '"#": ', v.Seq, ''
						, ',"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Score": ', v.Score, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Seq ASC)
				)
			, ']'
			) 
		) AS players
	FROM #HighScoreHardCore v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)

	IF OBJECT_ID('tempdb..#HighScoreEazyMode') IS NOT NULL
	BEGIN
		DROP TABLE #HighScoreEazyMode
	END

	CREATE TABLE #HighScoreEazyMode
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Score INT NOT NULL
	)
	INSERT INTO #HighScoreEazyMode
	SELECT TOP 25 
	ROW_NUMBER() OVER(ORDER BY MAX(l.Score) DESC) AS Seq,'HighScoreEazyMode',
	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Score) AS Score  
	FROM (
    SELECT MAX(l.Score) as Score, MAX(l.UserID) as UserID, MAX(l.UserName) as UserName
    FROM common.Leaderboard l
    WHERE l.isEazyMode=1
    GROUP BY l.UserName, l.LastCompletedLevel
	) l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY MAX(l.Score) DESC

	SELECT @HighScoreEazyModeJson = 
	(
	SELECT v.[Name]
	, 3 AS 'tournamentID'
	, 'Score' AS columnName
	, 'Score' AS description
	, 'High Score (Rookie)' AS title
	, 'Highest score' AS subtitle
    , JSON_QUERY(@HighScoreColumns) AS 'columns'
	,  CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{'
						, '"#": ', v.Seq, ''
						, ',"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Score": ', v.Score, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Seq ASC)
				)
			, ']'
			) 
		) AS players
	FROM #HighScoreEazyMode v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)


	IF OBJECT_ID('tempdb..#HighScoreSkilledMode') IS NOT NULL
	BEGIN
		DROP TABLE #HighScoreSkilledMode

	END

	CREATE TABLE #HighScoreSkilledMode
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Score INT NOT NULL
	)
	INSERT INTO #HighScoreSkilledMode
	SELECT TOP 25 
	ROW_NUMBER() OVER(ORDER BY MAX(l.Score) DESC) AS Seq ,'HighScoreSkilledMode',
	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Score) AS Score  
	FROM (
    SELECT MAX(l.Score) as Score, MAX(l.UserID) as UserID, MAX(l.UserName) as UserName
    FROM common.Leaderboard l
    WHERE l.isHardcore=0 AND l.isEazyMode=0
    GROUP BY l.UserName, l.LastCompletedLevel
	) l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY MAX(l.Score) DESC

	SELECT @HighScoreSkilledModeJson = 
	(
	SELECT v.[Name]
	, 2 AS 'tournamentID'
	, 'Score' AS columnName
	, 'Score' AS description
	, 'High Score (Skilled)' AS title
	, 'Highest score' AS subtitle
    , JSON_QUERY(@HighScoreColumns) AS 'columns'
	,  CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{'
						, '"#": ', v.Seq, ''
						, ',"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Score": ', v.Score, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Seq ASC)
				)
			, ']'
			) 
		) AS players
	FROM #HighScoreSkilledMode v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)


--Richest (HardCore)
--Richest (EazyMode)
--DECLARE @RichestHardCoreJson NVARCHAR(MAX) = ''
--DECLARE @RichestEasyModeJson NVARCHAR(MAX) = ''

	IF OBJECT_ID('tempdb..#RichestHardCore') IS NOT NULL
	BEGIN
		DROP TABLE #RichestHardCore
	END

	CREATE TABLE #RichestHardCore
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Cash INT NOT NULL
	)
	INSERT INTO #RichestHardCore
	SELECT TOP 25 
	ROW_NUMBER() OVER(ORDER BY MAX(l.Cash) DESC) AS Seq
    ,'RichestHardCore'
    ,ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Cash) AS Cash  
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	WHERE l.isHardcore=1
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY MAX(l.Cash) DESC

	SELECT @RichestHardCoreJson = 
	(
	SELECT v.[Name]
	, 4 AS 'tournamentID'
	, 'Cash' AS columnName
	, 'Cash' AS description
	, 'Richest (HardCore)' AS title
	, 'Most cash accrued' AS subtitle
    , JSON_QUERY(@RichestColumns) AS 'columns'
	,  CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{'
                       , '"#": ', v.Seq, ''
                        , ',"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Cash": ', v.Cash, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Seq ASC)
				)
			, ']'
			) 
		) AS players
	FROM #RichestHardCore v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)

	IF OBJECT_ID('tempdb..#RichestEazyMode') IS NOT NULL
	BEGIN
		DROP TABLE #RichestEazyMode
	END

	CREATE TABLE #RichestEazyMode
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Cash INT NOT NULL
	)
	INSERT INTO #RichestEazyMode
	SELECT TOP 25 
	ROW_NUMBER() OVER(ORDER BY MAX(l.Cash) DESC) AS Seq ,'RichestEazyMode',
	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Cash) AS Cash  
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	WHERE l.isEazyMode=1
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY Seq ASC

	SELECT @RichestEasyModeJson = 
	(
	SELECT v.[Name]
	, 6 AS 'tournamentID'
	, 'Cash' AS columnName
	, 'Cash' AS description
	, 'Richest (Rookie)' AS title
	, 'Most cash accrued' AS subtitle
    , JSON_QUERY(@RichestColumns) AS 'columns'
	,  CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{'
                        , '"#": ', v.Seq, ''
                        , ',"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Cash": ', v.Cash, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Seq ASC)
				)
			, ']'
			) 
		) AS players
	FROM #RichestEazyMode v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)


	--@RichestSkilledModeJson

	IF OBJECT_ID('tempdb..#RichestSkilledMode') IS NOT NULL
	BEGIN
		DROP TABLE #RichestSkilledMode
	END

	CREATE TABLE #RichestSkilledMode
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Cash INT NOT NULL
	)
	INSERT INTO #RichestSkilledMode
	SELECT TOP 25 
	ROW_NUMBER() OVER(ORDER BY MAX(l.Cash) DESC) AS Seq ,'RichestSkilledMode',
	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Cash) AS Cash  
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	WHERE l.isEazyMode=0 AND l.isHardcore=0
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY Seq ASC

	SELECT @RichestSkilledModeJson = 
	(
	SELECT v.[Name]
	, 5 AS 'tournamentID'
	, 'Cash' AS columnName
	, 'Cash' AS description
	, 'Richest (Skilled)' AS title
	, 'Most cash accrued' AS subtitle
    , JSON_QUERY(@RichestColumns) AS 'columns'
	,  CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{'
                        ,'"#": ', v.Seq, ''
                        , ',"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Cash": ', v.Cash, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Seq ASC)
				)
			, ']'
			) 
		) AS players
	FROM #RichestSkilledMode v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)



	-- DECLARE @KDRHardCoreJson NVARCHAR(MAX) = ''
    -- DECLARE @KDRSkilledModeJson NVARCHAR(MAX) = ''
    -- DECLARE @KDREasyModeJson NVARCHAR(MAX) = ''

    IF OBJECT_ID('tempdb..#KDRHardCore') IS NOT NULL
	BEGIN
		DROP TABLE #KDRHardCore
	END

	CREATE TABLE #KDRHardCore
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Kills INT NOT NULL,
        Deaths INT NOT NULL,
        KDR INT NOT NULL
	)
	INSERT INTO #KDRHardCore
SELECT TOP 25 
	ROW_NUMBER() OVER(ORDER BY MAX(l.Kills/ISNULL(NULLIF(l.Deaths,0),1)) DESC) AS Seq
    ,'KDRHardCore'
    ,ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Kills) AS Kills
    ,MAX(l.Deaths) AS Deaths  
    ,MAX(l.Kills/ISNULL(NULLIF(l.Deaths,0),1)) AS KDR
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	WHERE l.isHardcore=1
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY Seq ASC

	SELECT @KDRHardCoreJson = 
	(
	SELECT v.[Name]
	, 7 AS 'tournamentID'
	, 'KDR' AS columnName
	, 'KDR' AS description
	, 'KDR (HardCore)' AS title
	, 'Best kills to deaths ratio' AS subtitle
    , JSON_QUERY(@KDRColumns) AS 'columns'
	, CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{'
                        ,'"#": ', v.Seq, ''
                        , ',"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Kills": ', v.Kills, ''
                        , ',"Deaths": ', v.Deaths, ''
                        , ',"KDR": ', v.KDR, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Seq ASC)
				)
			, ']'
			) 
		) AS players
	FROM #KDRHardCore v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)

  IF OBJECT_ID('tempdb..#KDRSkilledMode') IS NOT NULL
	BEGIN
		DROP TABLE #KDRSkilledMode
	END

	CREATE TABLE #KDRSkilledMode
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Kills INT NOT NULL,
        Deaths INT NOT NULL,
        KDR INT NOT NULL
	)
	INSERT INTO #KDRSkilledMode
SELECT TOP 25 
	ROW_NUMBER() OVER(ORDER BY MAX(l.Kills/ISNULL(NULLIF(l.Deaths,0),1)) DESC) AS Seq
    ,'KDRSkilledMode'
    ,ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Kills) AS Kills
    ,MAX(l.Deaths) AS Deaths  
    ,MAX(l.Kills/ISNULL(NULLIF(l.Deaths,0),1)) AS KDR
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	WHERE l.isHardcore=0 AND l.isEazyMode=0
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY Seq ASC

	SELECT @KDRSkilledModeJson = 
	(
	SELECT v.[Name]
	, 8 AS 'tournamentID'
	, 'KDR' AS columnName
	, 'KDR' AS description
	, 'KDR (Skilled)' AS title
	, 'Best kills to deaths ratio' AS subtitle
    , JSON_QUERY(@KDRColumns) AS 'columns'
	, CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{'
                        ,'"#": ', v.Seq, ''
                        , ',"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Kills": ', v.Kills, ''
                        , ',"Deaths": ', v.Deaths, ''
                        , ',"KDR": ', v.KDR, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Seq ASC)
				)
			, ']'
			) 
		) AS players
	FROM #KDRSkilledMode v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)

 IF OBJECT_ID('tempdb..#KDREasyMode') IS NOT NULL
	BEGIN
		DROP TABLE #KDREasyMode
	END

	CREATE TABLE #KDREasyMode
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Kills INT NOT NULL,
        Deaths INT NOT NULL,
        KDR INT NOT NULL
	)
	INSERT INTO #KDREasyMode
    SELECT TOP 25 
	ROW_NUMBER() OVER(ORDER BY MAX(l.Kills/ISNULL(NULLIF(l.Deaths,0),1)) DESC) AS Seq
    ,'KDREasyMode'
    ,ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Kills) AS Kills
    ,MAX(l.Deaths) AS Deaths  
    ,MAX(l.Kills/ISNULL(NULLIF(l.Deaths,0),1)) AS KDR
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	WHERE l.isEazyMode=1
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY Seq ASC

	SELECT @KDREasyModeJson = 
	(
	SELECT v.[Name]
	, 9 AS 'tournamentID'
	, 'KDR' AS columnName
	, 'KDR' AS description
	, 'KDR (Rookie)' AS title
	, 'Best kills to deaths ratio' AS subtitle
    , JSON_QUERY(@KDRColumns) AS 'columns'
	, CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{'
                        ,'"#": ', v.Seq, ''
                        , ',"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Kills": ', v.Kills, ''
                        , ',"Deaths": ', v.Deaths, ''
                        , ',"KDR": ', v.KDR, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Seq ASC)
				)
			, ']'
			) 
		) AS players
	FROM #KDREasyMode v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)

    -- DECLARE @DeathScoreEasyModeJson NVARCHAR(MAX) = ''
    -- DECLARE @DeathScoreSkilledModeJson NVARCHAR(MAX) = ''

    IF OBJECT_ID('tempdb..#DeathScoreEasyMode') IS NOT NULL
	BEGIN
		DROP TABLE #DeathScoreEasyMode
	END

	CREATE TABLE #DeathScoreEasyMode
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Score INT NOT NULL,
        Deaths INT NOT NULL,
        DeathScore INT NOT NULL
	)
	INSERT INTO #DeathScoreEasyMode
    SELECT TOP 25 
	ROW_NUMBER() OVER(ORDER BY MAX(l.Score/ISNULL(NULLIF(l.Deaths,0),1)) DESC) AS Seq
    ,'DeathScoreEasyMode'
    ,ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Score) AS Score
    ,MAX(l.Deaths) AS Deaths  
    ,MAX(l.Score/ISNULL(NULLIF(l.Deaths,0),1)) AS DeathScore
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	WHERE l.isEazyMode=1
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY Seq ASC

	SELECT @DeathScoreEasyModeJson = 
	(
	SELECT v.[Name]
	, 10 AS 'tournamentID'
	, 'DeathScore' AS columnName
	, 'DeathScore' AS description
	, 'DEATHSCORE (Rookie)' AS title
	, 'Highest score to deaths ration' AS subtitle
    , JSON_QUERY(@DeathScoreColumns) AS 'columns'
	, CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{'
                        ,'"#": ', v.Seq, ''
                        , ',"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Score": ', v.Score, ''
                        , ',"Deaths": ', v.Deaths, ''
                        , ',"DeathScore": ', v.DeathScore, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Seq ASC)
				)
			, ']'
			) 
		) AS players
	FROM #DeathScoreEasyMode v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)

IF OBJECT_ID('tempdb..#DeathScoreSkilledMode') IS NOT NULL
	BEGIN
		DROP TABLE #DeathScoreSkilledMode
	END

	CREATE TABLE #DeathScoreSkilledMode
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Score INT NOT NULL,
        Deaths INT NOT NULL,
        DeathScore INT NOT NULL
	)
	INSERT INTO #DeathScoreSkilledMode
    SELECT TOP 25 
	ROW_NUMBER() OVER(ORDER BY MAX(l.Score/ISNULL(NULLIF(l.Deaths,0),1)) DESC) AS Seq
    ,'DeathScoreSkilledMode'
    ,ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Score) AS Score
    ,MAX(l.Deaths) AS Deaths  
    ,MAX(l.Score/ISNULL(NULLIF(l.Deaths,0),1)) AS DeathScore
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	WHERE l.isEazyMode=0 AND l.isHardcore=0
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY Seq ASC

	SELECT @DeathScoreSkilledModeJson = 
	(
	SELECT v.[Name]
	, 11 AS 'tournamentID'
	, 'DeathScore' AS columnName
	, 'DeathScore' AS description
	, 'DEATHSCORE (Skilled)' AS title
	, 'Highest score to deaths ration' AS subtitle
    , JSON_QUERY(@DeathScoreColumns) AS 'columns'
	, CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{'
                        ,'"#": ', v.Seq, ''
                        , ',"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Score": ', v.Score, ''
                        , ',"Deaths": ', v.Deaths, ''
                        , ',"DeathScore": ', v.DeathScore, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Seq ASC)
				)
			, ']'
			) 
		) AS players
	FROM #DeathScoreSkilledMode v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)


	--DEATHLESS (HardCore)	Yes	HardCore	Highest score upon completing the final level without dying.	rank, player, score	isHardcore = 1 and IF((int deaths = 0) AND (string level = [last game level])) THEN RANK (int score)
	IF OBJECT_ID('tempdb..#DeathlessHardCore') IS NOT NULL
	BEGIN
		DROP TABLE #DeathlessHardCore
	END

	CREATE TABLE #DeathlessHardCore
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Score INT NOT NULL
	)
	INSERT INTO #DeathlessHardCore
	SELECT TOP 25 
	ROW_NUMBER() OVER(ORDER BY MAX(l.Score) DESC) AS Seq ,'DeathlessHardCore',
	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Score) AS Score  
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	WHERE l.isHardcore=1 AND l.Deaths=0 AND l.LastCompletedLevel='Final'
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY MAX(l.Score) DESC

	SELECT @DeathlessHardCoreJson = 
	(
	SELECT v.[Name]
	, 12 AS 'tournamentID'
	, 'Score' AS columnName
	, 'Score' AS description
	, 'DEATHLESS (HardCore)' AS title
	, 'Highest score upon completing the final level without dying' AS subtitle
    , JSON_QUERY(@DeathlessColumns) AS 'columns'
	,  CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{'
                        , '"#": ', v.Seq, ''
                        , ',"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Score": ', v.Score, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Seq ASC)
				)
			, ']'
			) 
		) AS players
	FROM #DeathlessHardCore v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)


-- stingybastardwa == kanjoos makhhichoos

    IF OBJECT_ID('tempdb..#StingyBastard') IS NOT NULL
    BEGIN
        DROP TABLE #StingyBastard
    END
    CREATE TABLE #StingyBastard
    (
        Seq INT NOT NULL,
        [Name] VARCHAR(50) NOT NULL,
        ProfileDisplayName VARCHAR(200) NULL,
        UserID INT NULL,
        Cash INT NOT NULL
    )
    INSERT INTO #StingyBastard
    SELECT TOP 25 
    ROW_NUMBER() OVER(ORDER BY MAX(l.Cash) DESC) AS Seq,'StingyBastardHardCore',
    ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
    ,MAX(ISNULL(u.UserID,0)) AS UserID
    ,MAX(l.Cash) AS Cash 
    FROM common.leaderboard l
    LEFT OUTER JOIN common.Users u ON  l.UserName = u.SteamID or l.UserID=u.UserID
    WHERE l.isHardcore=1 AND NULLIF(l.weaponBottom,'') IS NULL AND NULLIF(l.weaponTop,'') IS NULL AND NULLIF(l.weaponLeft,'') IS NULL and NULLIF(weaponRight,'') IS NULL AND 
    l.shipClass=0 AND l.shipID='mosquito'
    GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
    ORDER BY MAX(l.Cash) DESC

    SELECT @StingyBastardHCJson = 
    (
    SELECT v.[Name]
    , 13 AS 'tournamentID'
    , 'Cash' AS columnName
    , 'Cash' AS description
    , 'STINGY BASTARD (HardCore)' AS title
	, 'Most cash accrued with no upgrades' AS subtitle
    , JSON_QUERY(@StingyBastardColumns) AS 'columns'
	,  CAST(0 AS BIT) AS 'hasLevels'
    ,   JSON_QUERY
        (CONCAT 
            ('[',
                (STRING_AGG
                    (CONCAT
                        ('{'
                        , '"#": ', v.Seq, ''
                        , ',"PlayerName": "', v.ProfileDisplayName, '"'
                        , ',"Cash": ', v.Cash, ''
                        , ',"UserID": ', v.UserID, ''
                        , '}'
                        )
                    , ','
                    ) WITHIN GROUP (ORDER BY v.Seq ASC)
                )
            , ']'
            ) 
        ) AS players
    FROM #StingyBastard v
    GROUP BY v.[Name]
    FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
    )

	
--   DECLARE @MotherlodeEasyModeJson NVARCHAR(MAX) = ''
--     DECLARE @MotherlodeSkilledJson NVARCHAR(MAX) = ''
--     DECLARE @MotherlodeHardCoreJson NVARCHAR(MAX) = ''

IF OBJECT_ID('tempdb..#MotherlodeEasyMode') IS NOT NULL
	BEGIN
		DROP TABLE #MotherlodeEasyMode
	END

	CREATE TABLE #MotherlodeEasyMode
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Cash INT NOT NULL,
        Pickups INT NOT NULL,
        Total INT NOT NULL
	)
	INSERT INTO #MotherlodeEasyMode
    SELECT TOP 25 
	ROW_NUMBER() OVER(ORDER BY MAX(l.Cash + l.Pickups) DESC) AS Seq
    ,'MotherlodeEasyMode'
    ,ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Cash) AS Cash
    ,MAX(l.Pickups) AS Pickups  
    ,MAX(l.Cash + l.Pickups) AS Total
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	WHERE l.isEazyMode=1 AND l.LastCompletedLevel=@Level
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY Seq ASC

	SELECT @MotherlodeEasyModeJson = 
	(
	SELECT v.[Name]
	, 14 AS 'tournamentID'
	, 'Total' AS columnName
	, 'Tatal' AS description
	, 'MOTHERLODE (Rookie)' AS title
	, 'Most pickups and cash collected' AS subtitle
    , JSON_QUERY(@MotherlodeColumns) AS 'columns'
	, CAST(1 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{'
                        ,'"#": ', v.Seq, ''
                        , ',"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Cash": ', v.Cash, ''
                        , ',"Pickups": ', v.Pickups, ''
                        , ',"Total": ', v.Total, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Seq ASC)
				)
			, ']'
			) 
		) AS players
	FROM #MotherlodeEasyMode v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)

IF OBJECT_ID('tempdb..#MotherlodeSkilled') IS NOT NULL
	BEGIN
		DROP TABLE #MotherlodeSkilled
	END

	CREATE TABLE #MotherlodeSkilled
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Cash INT NOT NULL,
        Pickups INT NOT NULL,
        Total INT NOT NULL
	)
	INSERT INTO #MotherlodeSkilled
    SELECT TOP 25 
	ROW_NUMBER() OVER(ORDER BY MAX(l.Cash + l.Pickups) DESC) AS Seq
    ,'MotherlodeSkilled'
    ,ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Cash) AS Cash
    ,MAX(l.Pickups) AS Pickups  
    ,MAX(l.Cash + l.Pickups) AS Total
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	WHERE l.isEazyMode=0 AND l.isHardcore=0 AND l.LastCompletedLevel=@Level
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY Seq ASC

	SELECT @MotherlodeSkilledJson = 
	(
	SELECT v.[Name]
	, 15 AS 'tournamentID'
	, 'Total' AS columnName
	, 'Tatal' AS description
	, 'MOTHERLODE (Skilled)' AS title
	, 'Most pickups and cash collected' AS subtitle
    , JSON_QUERY(@MotherlodeColumns) AS 'columns'
	, CAST(1 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{'
                        ,'"#": ', v.Seq, ''
                        , ',"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Cash": ', v.Cash, ''
                        , ',"Pickups": ', v.Pickups, ''
                        , ',"Total": ', v.Total, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Seq ASC)
				)
			, ']'
			) 
		) AS players
	FROM #MotherlodeSkilled v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)

IF OBJECT_ID('tempdb..#MotherlodeHardCore') IS NOT NULL
	BEGIN
		DROP TABLE #MotherlodeHardCore
	END

	CREATE TABLE #MotherlodeHardCore
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Cash INT NOT NULL,
        Pickups INT NOT NULL,
        Total INT NOT NULL
	)
	INSERT INTO #MotherlodeHardCore
    SELECT TOP 25 
	ROW_NUMBER() OVER(ORDER BY MAX(l.Cash + l.Pickups) DESC) AS Seq
    ,'MotherlodeHardCore'
    ,ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Cash) AS Cash
    ,MAX(l.Pickups) AS Pickups  
    ,MAX(l.Cash + l.Pickups) AS Total
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	WHERE l.isHardcore=1 AND l.LastCompletedLevel=@Level
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY Seq ASC

	SELECT @MotherlodeHardCoreJson = 
	(
	SELECT v.[Name]
	, 16 AS 'tournamentID'
	, 'Total' AS columnName
	, 'Tatal' AS description
	, 'MOTHERLODE (HardCore)' AS title
	, 'Most pickups and cash collected' AS subtitle
    , JSON_QUERY(@MotherlodeColumns) AS 'columns'
	, CAST(1 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{'
                        ,'"#": ', v.Seq, ''
                        , ',"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Cash": ', v.Cash, ''
                        , ',"Pickups": ', v.Pickups, ''
                        , ',"Total": ', v.Total, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Seq ASC)
				)
			, ']'
			) 
		) AS players
	FROM #MotherlodeHardCore v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)


	-- dodgy mcdoodle-doo
	IF OBJECT_ID('tempdb..#DodgyHardCore') IS NOT NULL
		BEGIN
			DROP TABLE #DodgyHardCore
		END

		CREATE TABLE #DodgyHardCore
		(
			Seq INT NOT NULL,
			[Name] VARCHAR(50) NOT NULL,
			ProfileDisplayName VARCHAR(200) NULL,
			UserID INT NULL,
			Score INT NOT NULL
		)

		INSERT INTO #DodgyHardCore
		SELECT TOP 25 
		ROW_NUMBER() OVER(ORDER BY MAX(l.Score) DESC) AS Seq
        ,'DodgyMcDodgersonHardCore'
        ,ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
		,MAX(ISNULL(u.UserID,0)) AS UserID
		,MAX(l.Score) AS Score 
		FROM common.leaderboard l
		LEFT OUTER JOIN common.Users u ON  l.UserName = u.SteamID or l.UserID=u.UserID
		WHERE l.LastCompletedLevel=@Level AND l.isHardcore=1 AND l.hasBeenHit=0
		GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
		ORDER BY MAX(l.Score) DESC

		SELECT @DodgyHardCoreJson = 
		(
		SELECT v.[Name]
		, 18 AS 'tournamentID'
		, 'Score' AS columnName
		, 'Score' AS description
		, 'DODGY McDODGERSON (HardCore)' AS title
		, CONCAT('Highest score and never been hit on ',@Level) AS subtitle
        , JSON_QUERY(@DodgyColumns) AS 'columns'
		,  CAST(1 AS BIT) AS 'hasLevels'
		,	JSON_QUERY
			(CONCAT	
				('[',
					(STRING_AGG
						(CONCAT
							('{'
                            , '"#": ', v.Seq, ''
                            , ',"PlayerName": "', v.ProfileDisplayName, '"'
							, ',"Score": ', v.Score, ''
							, ',"UserID": ', v.UserID, ''
							, '}'
							)
						, ','
						) WITHIN GROUP (ORDER BY v.Seq ASC)
					)
				, ']'
				) 
			) AS players
		FROM #DodgyHardCore v
		GROUP BY v.[Name]
		FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
		)

	

		-- lowly - le poor leaderboard

		IF OBJECT_ID('tempdb..#TheLowlyEazyMode') IS NOT NULL
			BEGIN
			DROP TABLE #TheLowlyEazyMode
			END

			CREATE TABLE #TheLowlyEazyMode
			(
			Seq INT NOT NULL,
			[Name] VARCHAR(50) NOT NULL,
			ProfileDisplayName VARCHAR(200) NULL,
			UserID INT NULL,
			Score INT NOT NULL
			)

			INSERT INTO #TheLowlyEazyMode
			SELECT TOP 25 
			ROW_NUMBER() OVER(ORDER BY MIN(l.Score) ASC) AS Seq ,'TheLowlyEazyMode',
			ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
			,MAX(ISNULL(u.UserID,0)) AS UserID
			,MIN(l.Score) AS Score 
			FROM common.leaderboard l
			LEFT OUTER JOIN common.Users u ON  l.UserName = u.SteamID or l.UserID=u.UserID
			WHERE l.LastCompletedLevel=@Level AND isEazyMode=1
			GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
			ORDER BY MIN(l.Score) ASC

			SELECT @LowlyEazyModeJson = 
			(
			SELECT v.[Name]
			, 17 AS 'tournamentID'
			, 'Score' AS columnName
			, 'Score' AS description
			, 'THE LOWLY (Rookie)' AS title
			, CONCAT('Lowest score on ',@Level) AS subtitle
            , JSON_QUERY(@LowlyColumns) AS 'columns'
			,  CAST(1 AS BIT) AS 'hasLevels'
			,	JSON_QUERY
			(CONCAT	
				('[',
					(STRING_AGG
						(CONCAT
							('{'
                            , '"#": ', v.Seq, ''
                            , ',"PlayerName": "', v.ProfileDisplayName, '"'
							, ',"Score": ', v.Score, ''
							, ',"UserID": ', v.UserID, ''
							, '}'
							)
						, ','
						) WITHIN GROUP (ORDER BY v.Seq ASC)
					)
				, ']'
				) 
			) AS players
			FROM #TheLowlyEazyMode v
			GROUP BY v.[Name]
			FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
			)


	
    --comment out to disable any leaderboard
	SELECT
    JSON_QUERY(
    CONCAT(
    '[' 
	, ISNULL(JSON_QUERY(@HighScoreEazyModeJson),'{}')	    , ',' 
	, ISNULL(JSON_QUERY(@HighScoreSkilledModeJson),'{}')    , ',' 
	, ISNULL(JSON_QUERY(@HighScoreHardCoreJson),'{}')	    , ',' 
	, ISNULL(JSON_QUERY(@RichestEasyModeJson),'{}')		    , ',' 
	, ISNULL(JSON_QUERY(@RichestSkilledModeJson),'{}')	    , ',' 
	, ISNULL(JSON_QUERY(@RichestHardCoreJson),'{}')		    , ',' 
    , ISNULL(JSON_QUERY(@KDREasyModeJson),'{}')		        , ','
    , ISNULL(JSON_QUERY(@KDRSkilledModeJson),'{}')		    , ','
    , ISNULL(JSON_QUERY(@KDRHardCoreJson),'{}')		        , ','
    , ISNULL(JSON_QUERY(@DeathScoreEasyModeJson),'{}')		, ','
    , ISNULL(JSON_QUERY(@DeathScoreSkilledModeJson),'{}')	, ','
	, ISNULL(JSON_QUERY(@DeathlessHardCoreJson),'{}')	    , ',' 
	, ISNULL(JSON_QUERY(@StingyBastardHCJson),'{}')		    , ','
    , ISNULL(JSON_QUERY(@MotherlodeEasyModeJson),'{}')		    , ','
    , ISNULL(JSON_QUERY(@MotherlodeSkilledJson),'{}')		    , ','
    , ISNULL(JSON_QUERY(@MotherlodeHardCoreJson),'{}')		    , ','
	, ISNULL(JSON_QUERY(@DodgyHardCoreJson),'{}')	        , ',' 
	, ISNULL(JSON_QUERY(@LowlyEazyModeJson),'{}')
    ,']'))
	AS DATA
    FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
END