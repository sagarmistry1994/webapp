﻿CREATE PROCEDURE [common].[get_AllLeaderboardInfo_LEVEL]
(
	@LevelID INT = 1
,	@tournamentID INT = 18
)
AS
BEGIN
	DECLARE @DodgyHardCoreJson NVARCHAR(MAX) = ''
	DECLARE @LowlyEazyModeJson NVARCHAR(MAX) = ''
    DECLARE @MotherlodeEasyModeJson NVARCHAR(MAX) = ''
    DECLARE @MotherlodeSkilledJson NVARCHAR(MAX) = ''
    DECLARE @MotherlodeHardCoreJson NVARCHAR(MAX) = ''
	DECLARE @Level VARCHAR(100) = ''
    DECLARE @DodgyColumns NVARCHAR(MAX) = '[{"columnName":"#","description":"#"},{"columnName":"PlayerName","description":"Player"},{"columnName":"Score","description":"Score"}]' 
	DECLARE @LowlyColumns NVARCHAR(MAX) = '[{"columnName":"#","description":"#"},{"columnName":"PlayerName","description":"Player"},{"columnName":"Score","description":"Score"}]' 
    DECLARE @MotherlodeColumns NVARCHAR(MAX) = '[{"columnName":"#","description":"#"},{"columnName":"PlayerName","description":"Player"},{"columnName":"Pickups","description":"Pickups"},{"columnName":"Cash","description":"Cash"},{"columnName":"Total","description":"Total"}]'


	SELECT @Level = gl.LevelName
	FROM common.GameLevels gl
	WHERE gl.GameID=1 AND gl.LevelID=@LevelID


	IF @tournamentID=18 
		BEGIN
				IF OBJECT_ID('tempdb..#DodgyHardCore') IS NOT NULL
				BEGIN
					DROP TABLE #DodgyHardCore
				END

				CREATE TABLE #DodgyHardCore
				(
					Seq INT NOT NULL,
					[Name] VARCHAR(50) NOT NULL,
					ProfileDisplayName VARCHAR(200) NULL,
					UserID INT NULL,
					Score INT NOT NULL
				)

				INSERT INTO #DodgyHardCore
				SELECT TOP 25 
				ROW_NUMBER() OVER(ORDER BY MAX(l.Score) DESC) AS Seq ,'DodgyMcDodgersonHardCore',
				ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
				,MAX(ISNULL(u.UserID,0)) AS UserID
				,MAX(l.Score) AS Score 
				FROM common.leaderboard l
				LEFT OUTER JOIN common.Users u ON  l.UserName = u.SteamID or l.UserID=u.UserID
				WHERE l.LastCompletedLevel=@Level AND l.isHardcore=1 AND l.hasBeenHit=0
				GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
				ORDER BY MAX(l.Score) DESC

				SELECT @DodgyHardCoreJson = 
				(
				SELECT v.[Name]
				, 18 AS 'tournamentID'
				, 'Score' AS columnName
				, 'Score' AS description
				, 'DODGY McDODGERSON (HardCore)' AS title
				, CONCAT('Highest score and never been hit on ',@Level) AS subtitle
                , JSON_QUERY(@DodgyColumns) AS 'columns'
				,  CAST(1 AS BIT) AS 'hasLevels'
				,	JSON_QUERY
			            (CONCAT	
                        ('[',
                            (STRING_AGG
                                (CONCAT
                                    ('{'
                                    , '"#": ', v.Seq, ''
                                    , ',"PlayerName": "', v.ProfileDisplayName, '"'
                                    , ',"Score": ', v.Score, ''
                                    , ',"UserID": ', v.UserID, ''
                                    , '}'
                                    )
                                , ','
                                ) WITHIN GROUP (ORDER BY v.Seq ASC)
                            )
                        , ']')
                        ) AS players
				FROM #DodgyHardCore v
				GROUP BY v.[Name]
				FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
				)

				SELECT
				JSON_QUERY(
				CONCAT(
				'[' 
				 ,JSON_QUERY(@DodgyHardCoreJson)
				,']')) 
				AS DATA
				FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER

		END
		ELSE IF @tournamentID=17
			BEGIN
				IF OBJECT_ID('tempdb..#TheLowlyEazyMode') IS NOT NULL
				BEGIN
				DROP TABLE #TheLowlyEazyMode
				END

				CREATE TABLE #TheLowlyEazyMode
				(
				Seq INT NOT NULL,
				[Name] VARCHAR(50) NOT NULL,
				ProfileDisplayName VARCHAR(200) NULL,
				UserID INT NULL,
				Score INT NOT NULL
				)

				INSERT INTO #TheLowlyEazyMode
				SELECT TOP 25 
				ROW_NUMBER() OVER(ORDER BY MIN(l.Score) ASC) AS Seq ,'TheLowlyEazyMode',
				ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
				,MAX(ISNULL(u.UserID,0)) AS UserID
				,MIN(l.Score) AS Score 
				FROM common.leaderboard l
				LEFT OUTER JOIN common.Users u ON  l.UserName = u.SteamID or l.UserID=u.UserID
				WHERE l.LastCompletedLevel=@Level AND isEazyMode=1
				GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
				ORDER BY MIN(l.Score) ASC

				SELECT @LowlyEazyModeJson = 
				(
				SELECT v.[Name]
				, 17 AS 'tournamentID'
				, 'Score' AS columnName
				, 'Score' AS description
				, 'THE LOWLY (Rookie)' AS title
				, CONCAT('Lowest score on ',@Level) AS subtitle
                , JSON_QUERY(@LowlyColumns) AS 'columns'
				,  CAST(1 AS BIT) AS 'hasLevels'
				,	JSON_QUERY
                    (CONCAT	
                        ('[',
                            (STRING_AGG
                                (CONCAT
                                    ('{'
                                    , '"#": ', v.Seq, ''
                                    , ',"PlayerName": "', v.ProfileDisplayName, '"'
                                    , ',"Score": ', v.Score, ''
                                    , ',"UserID": ', v.UserID, ''
                                    , '}'
                                    )
                                , ','
                                ) WITHIN GROUP (ORDER BY v.Seq ASC)
                            )
                        , ']'
                        ) 
                    ) AS players
				FROM #TheLowlyEazyMode v
				GROUP BY v.[Name]
				FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
				)

			SELECT
			JSON_QUERY(
			CONCAT(
			'[' 
			, JSON_QUERY(@LowlyEazyModeJson)
			,']')) 
			AS DATA
			FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
		END

    ELSE IF @tournamentID=14
        BEGIN
        IF OBJECT_ID('tempdb..#MotherlodeEasyMode') IS NOT NULL
            BEGIN
                DROP TABLE #MotherlodeEasyMode
            END

            CREATE TABLE #MotherlodeEasyMode
            (
                Seq INT NOT NULL,
                [Name] VARCHAR(50) NOT NULL,
                ProfileDisplayName VARCHAR(200) NULL,
                UserID INT NULL,
                Cash INT NOT NULL,
                Pickups INT NOT NULL,
                Total INT NOT NULL
            )
            INSERT INTO #MotherlodeEasyMode
            SELECT TOP 25 
            ROW_NUMBER() OVER(ORDER BY MAX(l.Cash + l.Pickups) DESC) AS Seq
            ,'MotherlodeEasyMode'
            ,ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
            ,MAX(ISNULL(u.UserID,0)) AS UserID
            ,MAX(l.Cash) AS Cash
            ,MAX(l.Pickups) AS Pickups  
            ,MAX(l.Cash + l.Pickups) AS Total
            FROM common.leaderboard l
            LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
            WHERE l.isEazyMode=1 AND l.LastCompletedLevel=@Level
            GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
            ORDER BY Seq ASC

            SELECT @MotherlodeEasyModeJson = 
            (
            SELECT v.[Name]
            , 14 AS 'tournamentID'
            , 'Total' AS columnName
            , 'Tatal' AS description
            , 'MOTHERLODE (Rookie)' AS title
            , 'Most pickups and cash collected' AS subtitle
            , JSON_QUERY(@MotherlodeColumns) AS 'columns'
            , CAST(1 AS BIT) AS 'hasLevels'
            , JSON_QUERY
                (CONCAT	
                    ('[',
                        (STRING_AGG
                            (CONCAT
                                ('{'
                                ,'"#": ', v.Seq, ''
                                , ',"PlayerName": "', v.ProfileDisplayName, '"'
                                , ',"Cash": ', v.Cash, ''
                                , ',"Pickups": ', v.Pickups, ''
                                , ',"Total": ', v.Total, ''
                                , ',"UserID": ', v.UserID, ''
                                , '}'
                                )
                            , ','
                            ) WITHIN GROUP (ORDER BY v.Seq ASC)
                        )
                    , ']'
                    ) 
                ) AS players
            FROM #MotherlodeEasyMode v
            GROUP BY v.[Name]
            FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
            )

            SELECT
			JSON_QUERY(
			CONCAT(
			'[' 
			, JSON_QUERY(@MotherlodeEasyModeJson)
			,']')) 
			AS DATA
			FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER

    END
    ELSE IF @tournamentID=15
        BEGIN
            IF OBJECT_ID('tempdb..#MotherlodeSkilled') IS NOT NULL
        BEGIN
            DROP TABLE #MotherlodeSkilled
        END

        CREATE TABLE #MotherlodeSkilled
        (
            Seq INT NOT NULL,
            [Name] VARCHAR(50) NOT NULL,
            ProfileDisplayName VARCHAR(200) NULL,
            UserID INT NULL,
            Cash INT NOT NULL,
            Pickups INT NOT NULL,
            Total INT NOT NULL
        )
        INSERT INTO #MotherlodeSkilled
        SELECT TOP 25 
        ROW_NUMBER() OVER(ORDER BY MAX(l.Cash + l.Pickups) DESC) AS Seq
        ,'MotherlodeSkilled'
        ,ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
        ,MAX(ISNULL(u.UserID,0)) AS UserID
        ,MAX(l.Cash) AS Cash
        ,MAX(l.Pickups) AS Pickups  
        ,MAX(l.Cash + l.Pickups) AS Total
        FROM common.leaderboard l
        LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
        WHERE l.isEazyMode=0 AND l.isHardcore=0 AND l.LastCompletedLevel=@Level
        GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
        ORDER BY Seq ASC

        SELECT @MotherlodeSkilledJson = 
        (
        SELECT v.[Name]
        , 15 AS 'tournamentID'
        , 'Total' AS columnName
        , 'Tatal' AS description
        , 'MOTHERLODE (Skilled)' AS title
        , 'Most pickups and cash collected' AS subtitle
        , JSON_QUERY(@MotherlodeColumns) AS 'columns'
        , CAST(1 AS BIT) AS 'hasLevels'
        , JSON_QUERY
            (CONCAT	
                ('[',
                    (STRING_AGG
                        (CONCAT
                            ('{'
                            ,'"#": ', v.Seq, ''
                            , ',"PlayerName": "', v.ProfileDisplayName, '"'
                            , ',"Cash": ', v.Cash, ''
                            , ',"Pickups": ', v.Pickups, ''
                            , ',"Total": ', v.Total, ''
                            , ',"UserID": ', v.UserID, ''
                            , '}'
                            )
                        , ','
                        ) WITHIN GROUP (ORDER BY v.Seq ASC)
                    )
                , ']'
                ) 
            ) AS players
        FROM #MotherlodeSkilled v
        GROUP BY v.[Name]
        FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
        )

         SELECT
			JSON_QUERY(
			CONCAT(
			'[' 
			, JSON_QUERY(@MotherlodeSkilledJson)
			,']')) 
			AS DATA
			FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER

    END

    ELSE IF @tournamentID=16
     BEGIN
            IF OBJECT_ID('tempdb..#MotherlodeHardCore') IS NOT NULL
            BEGIN
                DROP TABLE #MotherlodeHardCore
            END

            CREATE TABLE #MotherlodeHardCore
            (
                Seq INT NOT NULL,
                [Name] VARCHAR(50) NOT NULL,
                ProfileDisplayName VARCHAR(200) NULL,
                UserID INT NULL,
                Cash INT NOT NULL,
                Pickups INT NOT NULL,
                Total INT NOT NULL
            )
            INSERT INTO #MotherlodeHardCore
            SELECT TOP 25 
            ROW_NUMBER() OVER(ORDER BY MAX(l.Cash + l.Pickups) DESC) AS Seq
            ,'MotherlodeHardCore'
            ,ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
            ,MAX(ISNULL(u.UserID,0)) AS UserID
            ,MAX(l.Cash) AS Cash
            ,MAX(l.Pickups) AS Pickups  
            ,MAX(l.Cash + l.Pickups) AS Total
            FROM common.leaderboard l
            LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
            WHERE l.isHardcore=1 AND l.LastCompletedLevel=@Level
            GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
            ORDER BY Seq ASC

            SELECT @MotherlodeHardCoreJson = 
            (
            SELECT v.[Name]
            , 16 AS 'tournamentID'
            , 'Total' AS columnName
            , 'Tatal' AS description
            , 'MOTHERLODE (HardCore)' AS title
            , 'Most pickups and cash collected' AS subtitle
            , JSON_QUERY(@MotherlodeColumns) AS 'columns'
            , CAST(1 AS BIT) AS 'hasLevels'
            , JSON_QUERY
                (CONCAT	
                    ('[',
                        (STRING_AGG
                            (CONCAT
                                ('{'
                                ,'"#": ', v.Seq, ''
                                , ',"PlayerName": "', v.ProfileDisplayName, '"'
                                , ',"Cash": ', v.Cash, ''
                                , ',"Pickups": ', v.Pickups, ''
                                , ',"Total": ', v.Total, ''
                                , ',"UserID": ', v.UserID, ''
                                , '}'
                                )
                            , ','
                            ) WITHIN GROUP (ORDER BY v.Seq ASC)
                        )
                    , ']'
                    ) 
                ) AS players
            FROM #MotherlodeHardCore v
            GROUP BY v.[Name]
            FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
            )

        SELECT
			JSON_QUERY(
			CONCAT(
			'[' 
			, JSON_QUERY(@MotherlodeHardCoreJson)
			,']')) 
			AS DATA
			FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER

    END
END