﻿CREATE PROCEDURE [common].[get_AllLeaderboardInfo_DYNAMIC]
AS
BEGIN

	--DECLARE @AccuracyHardCoreJson NVARCHAR(MAX) = ''
	--DECLARE @AccuracyEazyModeJson NVARCHAR(MAX) = ''
	--DECLARE @AccuracySkilledModeJson NVARCHAR(MAX) = ''
	DECLARE @RichestHardCoreJson NVARCHAR(MAX) = ''
	DECLARE @RichestEasyModeJson NVARCHAR(MAX) = ''
	DECLARE @RichestSkilledModeJson NVARCHAR(MAX) = ''
	DECLARE @HighScoreHardCoreJson NVARCHAR(MAX) = ''
	DECLARE @HighScoreEazyModeJson NVARCHAR(MAX) = ''
	DECLARE @HighScoreSkilledModeJson NVARCHAR(MAX) = ''

	DECLARE @DeathlessHardCoreJson NVARCHAR(MAX) =''
	DECLARE @DeathlessEasyModeJson NVARCHAR(MAX) =''
	DECLARE @DeathlessSkilledModeJson NVARCHAR(MAX) =''

	DECLARE @StingyBastardHCJson NVARCHAR(MAX) = ''
	DECLARE @StingyBastardEMJson NVARCHAR(MAX) = ''
	DECLARE @StingyBastardSMJson NVARCHAR(MAX) = ''

	--has level
	DECLARE @DodgyHardCoreJson NVARCHAR(MAX) = '' 
	DECLARE @DodgyEasyModeJson NVARCHAR(MAX) = ''
	DECLARE @DodgySkilledModeJson NVARCHAR(MAX) = ''

	DECLARE @LowlyEazyModeJson NVARCHAR(MAX) = ''
	DECLARE @LowlyHardModeJson NVARCHAR(MAX) = ''
	DECLARE @LowlySkilledModeJson NVARCHAR(MAX) = ''

	DECLARE @Level VARCHAR(100) = ''

	SELECT @Level = gl.LevelName
	FROM common.GameLevels gl
	WHERE gl.GameID=1 AND gl.LevelID=1

----Accuracy (HardCore)
----Accuracy (EazyMode)

--	IF OBJECT_ID('tempdb..#AccuracyHardCore') IS NOT NULL
--	BEGIN
--		DROP TABLE #AccuracyHardCore
--	END

--	CREATE TABLE #AccuracyHardCore
--	(
--		Seq INT NOT NULL,
--		[Name] VARCHAR(50) NOT NULL,
--		ProfileDisplayName VARCHAR(200) NULL,
--		UserID INT NULL,
--		BulletHitPerc INT NOT NULL
--	)
--	INSERT INTO #AccuracyHardCore
--	SELECT TOP 25 
--	1 ,'AccuracyHardCore',
--	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
--	,MAX(ISNULL(u.UserID,0)) AS UserID
--	,MAX(CAST(l.BulletHitPerc AS INT)) AS BulletHitPerc
--	FROM common.leaderboard l
--	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
--	WHERE l.isHardcore = 1
--	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
--	ORDER BY MAX(CAST(l.BulletHitPerc AS INT)) DESC

--	SELECT @AccuracyHardCoreJson = 
--	(
--	SELECT v.[Name]
--	, 3 AS 'tournamentID'
--	, 'BulletHitPerc' AS columnName
--	, 'Hit Percentage' AS description
--	, 'Accuracy (HardCore)' AS title
--	, 'Highest accuracy percentage' AS subtitle
--   -- , JSON_QUERY('[{"columnName":"BulletHitPerc","description":"Hit Percentage"}]') AS columns
--	,  CAST(0 AS BIT) AS 'hasLevels'
--	,	JSON_QUERY
--		(CONCAT	
--			('[',
--				(STRING_AGG
--					(CONCAT
--						('{', '"PlayerName": "', v.ProfileDisplayName, '"'
--						, ',"BulletHitPerc": ', v.BulletHitPerc, ''
--						, ',"UserID": ', v.UserID, ''
--						, '}'
--						)
--					, ','
--					) WITHIN GROUP (ORDER BY CAST(v.BulletHitPerc AS INT) DESC)
--				)
--			, ']'
--			) 
--		) AS players
--	FROM #AccuracyHardCore v
--	GROUP BY v.[Name]
--	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
--	)


--	IF OBJECT_ID('tempdb..#AccuracyEazyMode') IS NOT NULL
--	BEGIN
--		DROP TABLE #AccuracyEazyMode
--	END

--	CREATE TABLE #AccuracyEazyMode
--	(
--		Seq INT NOT NULL,
--		[Name] VARCHAR(50) NOT NULL,
--		ProfileDisplayName VARCHAR(200) NULL,
--		UserID INT NULL,
--		BulletHitPerc INT NOT NULL
--	)
--	INSERT INTO #AccuracyEazyMode
--	SELECT TOP 25 
--	1 ,'AccuracyEazyMode',
--	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
--	,MAX(ISNULL(u.UserID,0)) AS UserID
--	,MAX(CAST(l.BulletHitPerc AS INT)) AS BulletHitPerc
--	FROM common.leaderboard l
--	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
--	WHERE l.isEazyMode = 1
--	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
--	ORDER BY MAX(CAST(l.BulletHitPerc AS INT)) DESC

--	SELECT @AccuracyEazyModeJson = 
--	(
--	SELECT v.[Name]
--	, 4 AS 'tournamentID'
--	, 'BulletHitPerc' AS columnName
--	, 'Hit Percentage' AS description
--	, 'Accuracy (Rookie)' AS title
--	, 'Highest accuracy percentage' AS subtitle
--	,  CAST(0 AS BIT) AS 'hasLevels'
--	,	JSON_QUERY
--		(CONCAT	
--			('[',
--				(STRING_AGG
--					(CONCAT
--						('{', '"PlayerName": "', v.ProfileDisplayName, '"'
--						, ',"BulletHitPerc": ', v.BulletHitPerc, ''
--						, ',"UserID": ', v.UserID, ''
--						, '}'
--						)
--					, ','
--					) WITHIN GROUP (ORDER BY CAST(v.BulletHitPerc AS INT) DESC)
--				)
--			, ']'
--			) 
--		) AS players
--	FROM #AccuracyEazyMode v
--	GROUP BY v.[Name]
--	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
--	)

--	IF OBJECT_ID('tempdb..#AccuracySkilledMode') IS NOT NULL
--	BEGIN
--		DROP TABLE #AccuracySkilledMode
--	END

--	CREATE TABLE #AccuracySkilledMode
--	(
--		Seq INT NOT NULL,
--		[Name] VARCHAR(50) NOT NULL,
--		ProfileDisplayName VARCHAR(200) NULL,
--		UserID INT NULL,
--		BulletHitPerc INT NOT NULL
--	)
--	INSERT INTO #AccuracySkilledMode
--	SELECT TOP 25 
--	1 ,'AccuracySkilledMode',
--	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
--	,MAX(ISNULL(u.UserID,0)) AS UserID
--	,MAX(CAST(l.BulletHitPerc AS INT)) AS BulletHitPerc
--	FROM common.leaderboard l
--	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
--	WHERE l.isEazyMode=0 AND l.isHardcore=0
--	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
--	ORDER BY MAX(CAST(l.BulletHitPerc AS INT)) DESC

--	SELECT @AccuracySkilledModeJson = 
--	(
--	SELECT v.[Name]
--	, 100 AS 'tournamentID'
--	, 'BulletHitPerc' AS columnName
--	, 'Hit Percentage' AS description
--	, 'Accuracy (Skilled)' AS title
--	, 'Highest accuracy percentage' AS subtitle
--	,  CAST(0 AS BIT) AS 'hasLevels'
--	,	JSON_QUERY
--		(CONCAT	
--			('[',
--				(STRING_AGG
--					(CONCAT
--						('{', '"PlayerName": "', v.ProfileDisplayName, '"'
--						, ',"BulletHitPerc": ', v.BulletHitPerc, ''
--						, ',"UserID": ', v.UserID, ''
--						, '}'
--						)
--					, ','
--					) WITHIN GROUP (ORDER BY CAST(v.BulletHitPerc AS INT) DESC)
--				)
--			, ']'
--			) 
--		) AS players
--	FROM #AccuracySkilledMode v
--	GROUP BY v.[Name]
--	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
--	)
	
--Richest (HardCore)
--Richest (EazyMode)
--DECLARE @RichestHardCoreJson NVARCHAR(MAX) = ''
--DECLARE @RichestEasyModeJson NVARCHAR(MAX) = ''

	IF OBJECT_ID('tempdb..#RichestHardCore') IS NOT NULL
	BEGIN
		DROP TABLE #RichestHardCore
	END

	CREATE TABLE #RichestHardCore
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Cash INT NOT NULL
	)
	INSERT INTO #RichestHardCore
	SELECT TOP 25 
	1 ,'RichestHardCore',
	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Cash) AS Cash  
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	WHERE l.isHardcore=1
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY MAX(l.Cash) DESC

	SELECT @RichestHardCoreJson = 
	(
	SELECT v.[Name]
	, 4 AS 'tournamentID'
	, 'Cash' AS columnName
	, 'Cash' AS description
	, 'Richest (HardCore)' AS title
	, 'Most cash accrued' AS subtitle
	,  CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{', '"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Cash": ', v.Cash, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Cash DESC)
				)
			, ']'
			) 
		) AS players
	FROM #RichestHardCore v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)

	IF OBJECT_ID('tempdb..#RichestEazyMode') IS NOT NULL
	BEGIN
		DROP TABLE #RichestEazyMode
	END

	CREATE TABLE #RichestEazyMode
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Cash INT NOT NULL
	)
	INSERT INTO #RichestEazyMode
	SELECT TOP 25 
	1 ,'RichestEazyMode',
	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Cash) AS Cash  
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	WHERE l.isEazyMode=1
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY MAX(l.Cash) DESC

	SELECT @RichestEasyModeJson = 
	(
	SELECT v.[Name]
	, 6 AS 'tournamentID'
	, 'Cash' AS columnName
	, 'Cash' AS description
	, 'Richest (Rookie)' AS title
	, 'Most cash accrued' AS subtitle
	,  CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{', '"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Cash": ', v.Cash, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Cash DESC)
				)
			, ']'
			) 
		) AS players
	FROM #RichestEazyMode v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)


	--@RichestSkilledModeJson

	IF OBJECT_ID('tempdb..#RichestSkilledMode') IS NOT NULL
	BEGIN
		DROP TABLE #RichestSkilledMode
	END

	CREATE TABLE #RichestSkilledMode
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Cash INT NOT NULL
	)
	INSERT INTO #RichestSkilledMode
	SELECT TOP 25 
	1 ,'RichestSkilledMode',
	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Cash) AS Cash  
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	WHERE l.isEazyMode=0 AND l.isHardcore=0
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY MAX(l.Cash) DESC

	SELECT @RichestSkilledModeJson = 
	(
	SELECT v.[Name]
	, 5 AS 'tournamentID'
	, 'Cash' AS columnName
	, 'Cash' AS description
	, 'Richest (Skilled)' AS title
	, 'Most cash accrued' AS subtitle
	,  CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{', '"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Cash": ', v.Cash, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Cash DESC)
				)
			, ']'
			) 
		) AS players
	FROM #RichestSkilledMode v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)

--High Score (HardCore)
--High Score (EazyMode)
	--DECLARE @HighScoreHardCoreJson NVARCHAR(MAX) = ''
	--DECLARE @HighScoreEazyModeJson NVARCHAR(MAX) = ''
	--@HighScoreSkilledModeJson

	IF OBJECT_ID('tempdb..#HighScoreHardCore') IS NOT NULL
	BEGIN
		DROP TABLE #HighScoreHardCore
	END

	CREATE TABLE #HighScoreHardCore
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Score INT NOT NULL
	)
	INSERT INTO #HighScoreHardCore
	SELECT TOP 25 
	1 ,'HighScoreHardCore',
	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Score) AS Score  
	FROM (
    SELECT MAX(l.Score) as Score, MAX(l.UserID) as UserID, MAX(l.UserName) as UserName
    FROM common.Leaderboard l
    WHERE l.isHardcore=1
    GROUP BY l.UserName, l.LastCompletedLevel
	) l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY MAX(l.Score) DESC

	SELECT @HighScoreHardCoreJson = 
	(
	SELECT v.[Name]
	, 1 AS 'tournamentID'
	, 'Score' AS columnName
	, 'Score' AS description
	, 'High Score (HardCore)' AS title
	, 'Highest score' AS subtitle
	,  CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{', '"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Score": ', v.Score, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Score DESC)
				)
			, ']'
			) 
		) AS players
	FROM #HighScoreHardCore v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)

	IF OBJECT_ID('tempdb..#HighScoreEazyMode') IS NOT NULL
	BEGIN
		DROP TABLE #HighScoreEazyMode
	END

	CREATE TABLE #HighScoreEazyMode
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Score INT NOT NULL
	)
	INSERT INTO #HighScoreEazyMode
	SELECT TOP 25 
	1 ,'HighScoreEazyMode',
	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Score) AS Score  
	FROM (
    SELECT MAX(l.Score) as Score, MAX(l.UserID) as UserID, MAX(l.UserName) as UserName
    FROM common.Leaderboard l
    WHERE l.isEazyMode=1
    GROUP BY l.UserName, l.LastCompletedLevel
	) l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY MAX(l.Score) DESC

	SELECT @HighScoreEazyModeJson = 
	(
	SELECT v.[Name]
	, 3 AS 'tournamentID'
	, 'Score' AS columnName
	, 'Score' AS description
	, 'High Score (Rookie)' AS title
	, 'Highest score' AS subtitle
	,  CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{', '"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Score": ', v.Score, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Score DESC)
				)
			, ']'
			) 
		) AS players
	FROM #HighScoreEazyMode v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)


	IF OBJECT_ID('tempdb..#HighScoreSkilledMode') IS NOT NULL
	BEGIN
		DROP TABLE #HighScoreSkilledMode

	END

	CREATE TABLE #HighScoreSkilledMode
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Score INT NOT NULL
	)
	INSERT INTO #HighScoreSkilledMode
	SELECT TOP 25 
	1 ,'HighScoreSkilledMode',
	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Score) AS Score  
	FROM (
    SELECT MAX(l.Score) as Score, MAX(l.UserID) as UserID, MAX(l.UserName) as UserName
    FROM common.Leaderboard l
    WHERE l.isHardcore=0 AND l.isEazyMode=0
    GROUP BY l.UserName, l.LastCompletedLevel
	) l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY MAX(l.Score) DESC

	SELECT @HighScoreSkilledModeJson = 
	(
	SELECT v.[Name]
	, 2 AS 'tournamentID'
	, 'Score' AS columnName
	, 'Score' AS description
	, 'High Score (Skilled)' AS title
	, 'Highest score' AS subtitle
	,  CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{', '"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Score": ', v.Score, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Score DESC)
				)
			, ']'
			) 
		) AS players
	FROM #HighScoreSkilledMode v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)

	

	--DEATHLESS (HardCore)	Yes	HardCore	Highest score upon completing the final level without dying.	rank, player, score	isHardcore = 1 and IF((int deaths = 0) AND (string level = [last game level])) THEN RANK (int score)
	IF OBJECT_ID('tempdb..#DeathlessHardCore') IS NOT NULL
	BEGIN
		DROP TABLE #DeathlessHardCore
	END

	CREATE TABLE #DeathlessHardCore
	(
		Seq INT NOT NULL,
		[Name] VARCHAR(50) NOT NULL,
		ProfileDisplayName VARCHAR(200) NULL,
		UserID INT NULL,
		Score INT NOT NULL
	)
	INSERT INTO #DeathlessHardCore
	SELECT TOP 25 
	1 ,'DeathlessHardCore',
	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	,MAX(ISNULL(u.UserID,0)) AS UserID
	,MAX(l.Score) AS Score  
	FROM common.leaderboard l
	LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	WHERE l.isHardcore=1 AND l.Deaths=0 AND l.LastCompletedLevel='Final'
	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	ORDER BY MAX(l.Score) DESC

	SELECT @DeathlessHardCoreJson = 
	(
	SELECT v.[Name]
	, 12 AS 'tournamentID'
	, 'Score' AS columnName
	, 'Score' AS description
	, 'DEATHLESS (HardCore)' AS title
	, 'Highest score upon completing the final level without dying' AS subtitle
	,  CAST(0 AS BIT) AS 'hasLevels'
	, JSON_QUERY
		(CONCAT	
			('[',
				(STRING_AGG
					(CONCAT
						('{', '"PlayerName": "', v.ProfileDisplayName, '"'
						, ',"Score": ', v.Score, ''
						, ',"UserID": ', v.UserID, ''
						, '}'
						)
					, ','
					) WITHIN GROUP (ORDER BY v.Score DESC)
				)
			, ']'
			) 
		) AS players
	FROM #DeathlessHardCore v
	GROUP BY v.[Name]
	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	)

	----DECLARE @DeathlessEasyModeJson NVARCHAR(MAX) =''
	--IF OBJECT_ID('tempdb..#DeathlessEasyMode') IS NOT NULL
	--BEGIN
	--	DROP TABLE #DeathlessEasyMode
	--END

	--CREATE TABLE #DeathlessEasyMode
	--(
	--	Seq INT NOT NULL,
	--	[Name] VARCHAR(50) NOT NULL,
	--	ProfileDisplayName VARCHAR(200) NULL,
	--	UserID INT NULL,
	--	Score INT NOT NULL
	--)
	--INSERT INTO #DeathlessEasyMode
	--SELECT TOP 25 
	--1 ,'DeathlessEasyMode',
	--ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	--,MAX(ISNULL(u.UserID,0)) AS UserID
	--,MAX(l.Score) AS Score  
	--FROM common.leaderboard l
	--LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	--WHERE l.isEazyMode=1 AND l.Deaths=0 AND l.LastCompletedLevel='Final'
	--GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	--ORDER BY MAX(l.Score) DESC

	--SELECT @DeathlessEasyModeJson = 
	--(
	--SELECT v.[Name]
	--, 1002 AS 'tournamentID'
	--, 'Score' AS columnName
	--, 'Score' AS description
	--, 'DEATHLESS (Rookie)' AS title
	--, 'Highest score upon completing the final level without dying' AS subtitle
	--,  CAST(0 AS BIT) AS 'hasLevels'
	--, JSON_QUERY
	--	(CONCAT	
	--		('[',
	--			(STRING_AGG
	--				(CONCAT
	--					('{', '"PlayerName": "', v.ProfileDisplayName, '"'
	--					, ',"Score": ', v.Score, ''
	--					, ',"UserID": ', v.UserID, ''
	--					, '}'
	--					)
	--				, ','
	--				) WITHIN GROUP (ORDER BY v.Score DESC)
	--			)
	--		, ']'
	--		) 
	--	) AS players
	--FROM #DeathlessEasyMode v
	--GROUP BY v.[Name]
	--FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	--)
	
	----DECLARE @DeathlessSkilledModeJson NVARCHAR(MAX) =''
	--IF OBJECT_ID('tempdb..#DeathlessSkilledMode') IS NOT NULL
	--BEGIN
	--	DROP TABLE #DeathlessSkilledMode
	--END

	--CREATE TABLE #DeathlessSkilledMode
	--(
	--	Seq INT NOT NULL,
	--	[Name] VARCHAR(50) NOT NULL,
	--	ProfileDisplayName VARCHAR(200) NULL,
	--	UserID INT NULL,
	--	Score INT NOT NULL
	--)
	--INSERT INTO #DeathlessSkilledMode
	--SELECT TOP 25 
	--1 ,'DeathlessSkilledMode',
	--ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	--,MAX(ISNULL(u.UserID,0)) AS UserID
	--,MAX(l.Score) AS Score  
	--FROM common.leaderboard l
	--LEFT OUTER JOIN common.Users u ON l.UserID=u.UserID OR l.UserName = u.SteamID 
	--WHERE l.isEazyMode=1 AND l.Deaths=0 AND l.LastCompletedLevel='Final'
	--GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	--ORDER BY MAX(l.Score) DESC

	--SELECT @DeathlessSkilledModeJson = 
	--(
	--SELECT v.[Name]
	--, 1003 AS 'tournamentID'
	--, 'Score' AS columnName
	--, 'Score' AS description
	--, 'DEATHLESS (Skilled)' AS title
	--, 'Highest score upon completing the final level without dying' AS subtitle
	--,  CAST(0 AS BIT) AS 'hasLevels'
	--, JSON_QUERY
	--	(CONCAT	
	--		('[',
	--			(STRING_AGG
	--				(CONCAT
	--					('{', '"PlayerName": "', v.ProfileDisplayName, '"'
	--					, ',"Score": ', v.Score, ''
	--					, ',"UserID": ', v.UserID, ''
	--					, '}'
	--					)
	--				, ','
	--				) WITHIN GROUP (ORDER BY v.Score DESC)
	--			)
	--		, ']'
	--		) 
	--	) AS players
	--FROM #DeathlessSkilledMode v
	--GROUP BY v.[Name]
	--FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	--)


    IF OBJECT_ID('tempdb..#StingyBastard') IS NOT NULL
    BEGIN
        DROP TABLE #StingyBastard
    END
    CREATE TABLE #StingyBastard
    (
        Seq INT NOT NULL,
        [Name] VARCHAR(50) NOT NULL,
        ProfileDisplayName VARCHAR(200) NULL,
        UserID INT NULL,
        Cash INT NOT NULL
    )
    INSERT INTO #StingyBastard
    SELECT TOP 25 
    1 ,'StingyBastardHardCore',
    ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
    ,MAX(ISNULL(u.UserID,0)) AS UserID
    ,MAX(l.Cash) AS Cash 
    FROM common.leaderboard l
    LEFT OUTER JOIN common.Users u ON  l.UserName = u.SteamID or l.UserID=u.UserID
    WHERE l.isHardcore=1 AND NULLIF(l.weaponBottom,'') IS NULL AND NULLIF(l.weaponTop,'') IS NULL AND NULLIF(l.weaponLeft,'') IS NULL and NULLIF(weaponRight,'') IS NULL AND 
    l.shipClass=0 AND l.shipID='mosquito'
    GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
    ORDER BY MAX(l.Cash) DESC

    SELECT @StingyBastardHCJson = 
    (
    SELECT v.[Name]
    , 13 AS 'tournamentID'
    , 'Cash' AS columnName
    , 'Cash' AS description
    , 'STINGY BASTARD (HardCore)' AS title
	, 'Most cash accrued with no upgrades' AS subtitle
	,  CAST(0 AS BIT) AS 'hasLevels'
    ,   JSON_QUERY
        (CONCAT 
            ('[',
                (STRING_AGG
                    (CONCAT
                        ('{', '"PlayerName": "', v.ProfileDisplayName, '"'
                        , ',"Cash": ', v.Cash, ''
                        , ',"UserID": ', v.UserID, ''
                        , '}'
                        )
                    , ','
                    ) WITHIN GROUP (ORDER BY v.Cash DESC)
                )
            , ']'
            ) 
        ) AS players
    FROM #StingyBastard v
    GROUP BY v.[Name]
    FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
    )

	----DECLARE @StingyBastardEMJson NVARCHAR(MAX) = ''

	--IF OBJECT_ID('tempdb..#StingyBastardEM') IS NOT NULL
 --   BEGIN
 --       DROP TABLE #StingyBastardEM
 --   END
 --   CREATE TABLE #StingyBastardEM
 --   (
 --       Seq INT NOT NULL,
 --       [Name] VARCHAR(50) NOT NULL,
 --       ProfileDisplayName VARCHAR(200) NULL,
 --       UserID INT NULL,
 --       Cash INT NOT NULL
 --   )
 --   INSERT INTO #StingyBastardEM
 --   SELECT TOP 25 
 --   1 ,'#StingyBastardEM',
 --   ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
 --   ,MAX(ISNULL(u.UserID,0)) AS UserID
 --   ,MAX(l.Cash) AS Cash 
 --   FROM common.leaderboard l
 --   LEFT OUTER JOIN common.Users u ON  l.UserName = u.SteamID or l.UserID=u.UserID
 --   WHERE l.isEazyMode=1 AND NULLIF(l.weaponBottom,'') IS NULL AND NULLIF(l.weaponTop,'') IS NULL AND NULLIF(l.weaponLeft,'') IS NULL and NULLIF(weaponRight,'') IS NULL AND 
 --   l.shipClass=0 AND l.shipID='mosquito'
 --   GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
 --   ORDER BY MAX(l.Cash) DESC

 --   SELECT @StingyBastardEMJson = 
 --   (
 --   SELECT v.[Name]
 --   , 1102 AS 'tournamentID'
 --   , 'Cash' AS columnName
 --   , 'Cash' AS description
 --   , 'STINGY BASTARD (Rookie)' AS title
	--, 'Most cash accrued with no upgrades' AS subtitle
	--,  CAST(0 AS BIT) AS 'hasLevels'
 --   ,   JSON_QUERY
 --       (CONCAT 
 --           ('[',
 --               (STRING_AGG
 --                   (CONCAT
 --                       ('{', '"PlayerName": "', v.ProfileDisplayName, '"'
 --                       , ',"Cash": ', v.Cash, ''
 --                       , ',"UserID": ', v.UserID, ''
 --                       , '}'
 --                       )
 --                   , ','
 --                   ) WITHIN GROUP (ORDER BY v.Cash DESC)
 --               )
 --           , ']'
 --           ) 
 --       ) AS players
 --   FROM #StingyBastardEM v
 --   GROUP BY v.[Name]
 --   FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
 --   )

	----DECLARE @StingyBastardSMJson NVARCHAR(MAX) = ''

	--IF OBJECT_ID('tempdb..#StingyBastardSM') IS NOT NULL
 --   BEGIN
 --       DROP TABLE #StingyBastardSM
 --   END
 --   CREATE TABLE #StingyBastardSM
 --   (
 --       Seq INT NOT NULL,
 --       [Name] VARCHAR(50) NOT NULL,
 --       ProfileDisplayName VARCHAR(200) NULL,
 --       UserID INT NULL,
 --       Cash INT NOT NULL
 --   )
 --   INSERT INTO #StingyBastardSM
 --   SELECT TOP 25 
 --   1 ,'#StingyBastardSM',
 --   ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
 --   ,MAX(ISNULL(u.UserID,0)) AS UserID
 --   ,MAX(l.Cash) AS Cash 
 --   FROM common.leaderboard l
 --   LEFT OUTER JOIN common.Users u ON  l.UserName = u.SteamID or l.UserID=u.UserID
 --   WHERE l.isEazyMode=0 AND l.isHardCore=0 AND NULLIF(l.weaponBottom,'') IS NULL AND NULLIF(l.weaponTop,'') IS NULL AND NULLIF(l.weaponLeft,'') IS NULL and NULLIF(weaponRight,'') IS NULL AND 
 --   l.shipClass=0 AND l.shipID='mosquito'
 --   GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
 --   ORDER BY MAX(l.Cash) DESC

 --   SELECT @StingyBastardSMJson = 
 --   (
 --   SELECT v.[Name]
 --   , 1103 AS 'tournamentID'
 --   , 'Cash' AS columnName
 --   , 'Cash' AS description
 --   , 'STINGY BASTARD (Skilled)' AS title
	--, 'Most cash accrued with no upgrades' AS subtitle
	--,  CAST(0 AS BIT) AS 'hasLevels'
 --   ,   JSON_QUERY
 --       (CONCAT 
 --           ('[',
 --               (STRING_AGG
 --                   (CONCAT
 --                       ('{', '"PlayerName": "', v.ProfileDisplayName, '"'
 --                       , ',"Cash": ', v.Cash, ''
 --                       , ',"UserID": ', v.UserID, ''
 --                       , '}'
 --                       )
 --                   , ','
 --                   ) WITHIN GROUP (ORDER BY v.Cash DESC)
 --               )
 --           , ']'
 --           ) 
 --       ) AS players
 --   FROM #StingyBastardSM v
 --   GROUP BY v.[Name]
 --   FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
 --   )

	-- dodgy mcdoodooson
	IF OBJECT_ID('tempdb..#DodgyHardCore') IS NOT NULL
		BEGIN
			DROP TABLE #DodgyHardCore
		END

		CREATE TABLE #DodgyHardCore
		(
			Seq INT NOT NULL,
			[Name] VARCHAR(50) NOT NULL,
			ProfileDisplayName VARCHAR(200) NULL,
			UserID INT NULL,
			Score INT NOT NULL
		)

		INSERT INTO #DodgyHardCore
		SELECT TOP 25 
		1 ,'DodgyMcDodgersonHardCore',
		ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
		,MAX(ISNULL(u.UserID,0)) AS UserID
		,MAX(l.Score) AS Score 
		FROM common.leaderboard l
		LEFT OUTER JOIN common.Users u ON  l.UserName = u.SteamID or l.UserID=u.UserID
		WHERE l.LastCompletedLevel=@Level AND l.isHardcore=1 AND l.hasBeenHit=0
		GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
		ORDER BY MAX(l.Score) DESC

		SELECT @DodgyHardCoreJson = 
		(
		SELECT v.[Name]
		, 18 AS 'tournamentID'
		, 'Score' AS columnName
		, 'Score' AS description
		, 'DODGY McDODGERSON (HardCore)' AS title
		, CONCAT('Highest score and never been hit on ',@Level) AS subtitle
		,  CAST(1 AS BIT) AS 'hasLevels'
		,	JSON_QUERY
			(CONCAT	
				('[',
					(STRING_AGG
						(CONCAT
							('{', '"PlayerName": "', v.ProfileDisplayName, '"'
							, ',"Score": ', v.Score, ''
							, ',"UserID": ', v.UserID, ''
							, '}'
							)
						, ','
						) WITHIN GROUP (ORDER BY v.Score DESC)
					)
				, ']'
				) 
			) AS players
		FROM #DodgyHardCore v
		GROUP BY v.[Name]
		FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
		)

	----DECLARE @DodgyEasyModeJson NVARCHAR(MAX) = ''
	--IF OBJECT_ID('tempdb..#DodgyEasyMode') IS NOT NULL
	--	BEGIN
	--		DROP TABLE #DodgyEasyMode
	--	END

	--	CREATE TABLE #DodgyEasyMode
	--	(
	--		Seq INT NOT NULL,
	--		[Name] VARCHAR(50) NOT NULL,
	--		ProfileDisplayName VARCHAR(200) NULL,
	--		UserID INT NULL,
	--		Score INT NOT NULL
	--	)

	--	INSERT INTO #DodgyEasyMode
	--	SELECT TOP 25 
	--	1 ,'DodgyMcDodgersonEasyMode',
	--	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	--	,MAX(ISNULL(u.UserID,0)) AS UserID
	--	,MAX(l.Score) AS Score 
	--	FROM common.leaderboard l
	--	LEFT OUTER JOIN common.Users u ON  l.UserName = u.SteamID or l.UserID=u.UserID
	--	WHERE l.LastCompletedLevel=@Level AND l.isEazyMode=1 AND l.hasBeenHit=0
	--	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	--	ORDER BY MAX(l.Score) DESC

	--	SELECT @DodgyEasyModeJson = 
	--	(
	--	SELECT v.[Name]
	--	, 1702 AS 'tournamentID'
	--	, 'Score' AS columnName
	--	, 'Score' AS description
	--	, 'DODGY McDODGERSON (Rookie)' AS title
	--	, CONCAT('Highest score and never been hit on ',@Level) AS subtitle
	--	,  CAST(1 AS BIT) AS 'hasLevels'
	--	,	JSON_QUERY
	--		(CONCAT	
	--			('[',
	--				(STRING_AGG
	--					(CONCAT
	--						('{', '"PlayerName": "', v.ProfileDisplayName, '"'
	--						, ',"Score": ', v.Score, ''
	--						, ',"UserID": ', v.UserID, ''
	--						, '}'
	--						)
	--					, ','
	--					) WITHIN GROUP (ORDER BY v.Score DESC)
	--				)
	--			, ']'
	--			) 
	--		) AS players
	--	FROM #DodgyHardCore v
	--	GROUP BY v.[Name]
	--	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	--	)



	----DECLARE @DodgySkilledModeJson NVARCHAR(MAX) = ''
	--IF OBJECT_ID('tempdb..#DodgySkilledMode') IS NOT NULL
	--	BEGIN
	--		DROP TABLE #DodgySkilledMode
	--	END

	--	CREATE TABLE #DodgySkilledMode
	--	(
	--		Seq INT NOT NULL,
	--		[Name] VARCHAR(50) NOT NULL,
	--		ProfileDisplayName VARCHAR(200) NULL,
	--		UserID INT NULL,
	--		Score INT NOT NULL
	--	)

	--	INSERT INTO #DodgyEasyMode
	--	SELECT TOP 25 
	--	1 ,'DodgyMcDodgersonSkilledMode',
	--	ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	--	,MAX(ISNULL(u.UserID,0)) AS UserID
	--	,MAX(l.Score) AS Score 
	--	FROM common.leaderboard l
	--	LEFT OUTER JOIN common.Users u ON  l.UserName = u.SteamID or l.UserID=u.UserID
	--	WHERE l.LastCompletedLevel=@Level AND l.isEazyMode=0 AND l.isHardcore=0 AND l.hasBeenHit=0
	--	GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	--	ORDER BY MAX(l.Score) DESC

	--	SELECT @DodgySkilledModeJson = 
	--	(
	--	SELECT v.[Name]
	--	, 1703 AS 'tournamentID'
	--	, 'Score' AS columnName
	--	, 'Score' AS description
	--	, 'DODGY McDODGERSON (Skilled)' AS title
	--	, CONCAT('Highest score and never been hit on ',@Level) AS subtitle
	--	,  CAST(1 AS BIT) AS 'hasLevels'
	--	,	JSON_QUERY
	--		(CONCAT	
	--			('[',
	--				(STRING_AGG
	--					(CONCAT
	--						('{', '"PlayerName": "', v.ProfileDisplayName, '"'
	--						, ',"Score": ', v.Score, ''
	--						, ',"UserID": ', v.UserID, ''
	--						, '}'
	--						)
	--					, ','
	--					) WITHIN GROUP (ORDER BY v.Score DESC)
	--				)
	--			, ']'
	--			) 
	--		) AS players
	--	FROM #DodgyHardCore v
	--	GROUP BY v.[Name]
	--	FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	--	)

		-- lowly modes

		IF OBJECT_ID('tempdb..#TheLowlyEazyMode') IS NOT NULL
			BEGIN
			DROP TABLE #TheLowlyEazyMode
			END

			CREATE TABLE #TheLowlyEazyMode
			(
			Seq INT NOT NULL,
			[Name] VARCHAR(50) NOT NULL,
			ProfileDisplayName VARCHAR(200) NULL,
			UserID INT NULL,
			Score INT NOT NULL
			)

			INSERT INTO #TheLowlyEazyMode
			SELECT TOP 25 
			1 ,'TheLowlyEazyMode',
			ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
			,MAX(ISNULL(u.UserID,0)) AS UserID
			,MIN(l.Score) AS Score 
			FROM common.leaderboard l
			LEFT OUTER JOIN common.Users u ON  l.UserName = u.SteamID or l.UserID=u.UserID
			WHERE l.LastCompletedLevel=@Level AND isEazyMode=1
			GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
			ORDER BY MIN(l.Score) ASC

			SELECT @LowlyEazyModeJson = 
			(
			SELECT v.[Name]
			, 17 AS 'tournamentID'
			, 'Score' AS columnName
			, 'Score' AS description
			, 'THE LOWLY (Rookie)' AS title
			, CONCAT('Lowest score on ',@Level) AS subtitle
			,  CAST(1 AS BIT) AS 'hasLevels'
			,	JSON_QUERY
			(CONCAT	
				('[',
					(STRING_AGG
						(CONCAT
							('{', '"PlayerName": "', v.ProfileDisplayName, '"'
							, ',"Score": ', v.Score, ''
							, ',"UserID": ', v.UserID, ''
							, '}'
							)
						, ','
						) WITHIN GROUP (ORDER BY v.Score ASC)
					)
				, ']'
				) 
			) AS players
			FROM #TheLowlyEazyMode v
			GROUP BY v.[Name]
			FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
			)

	----DECLARE @LowlyHardModeJson NVARCHAR(MAX) = ''
	--IF OBJECT_ID('tempdb..#LowlyHardMode') IS NOT NULL
	--		BEGIN
	--		DROP TABLE #LowlyHardMode
	--		END

	--		CREATE TABLE #LowlyHardMode
	--		(
	--		Seq INT NOT NULL,
	--		[Name] VARCHAR(50) NOT NULL,
	--		ProfileDisplayName VARCHAR(200) NULL,
	--		UserID INT NULL,
	--		Score INT NOT NULL
	--		)

	--		INSERT INTO #LowlyHardMode
	--		SELECT TOP 25 
	--		1 ,'TheLowlyEazyMode',
	--		ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	--		,MAX(ISNULL(u.UserID,0)) AS UserID
	--		,MIN(l.Score) AS Score 
	--		FROM common.leaderboard l
	--		LEFT OUTER JOIN common.Users u ON  l.UserName = u.SteamID or l.UserID=u.UserID
	--		WHERE l.LastCompletedLevel=@Level AND l.isHardcore=1
	--		GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	--		ORDER BY MIN(l.Score) ASC

	--		SELECT @LowlyHardModeJson = 
	--		(
	--		SELECT v.[Name]
	--		, 1402 AS 'tournamentID'
	--		, 'Score' AS columnName
	--		, 'Score' AS description
	--		, 'THE LOWLY (HardCore)' AS title
	--		, CONCAT('Lowest score on ',@Level) AS subtitle
	--		,  CAST(1 AS BIT) AS 'hasLevels'
	--		,	JSON_QUERY
	--		(CONCAT	
	--			('[',
	--				(STRING_AGG
	--					(CONCAT
	--						('{', '"PlayerName": "', v.ProfileDisplayName, '"'
	--						, ',"Score": ', v.Score, ''
	--						, ',"UserID": ', v.UserID, ''
	--						, '}'
	--						)
	--					, ','
	--					) WITHIN GROUP (ORDER BY v.Score ASC)
	--				)
	--			, ']'
	--			) 
	--		) AS players
	--		FROM #LowlyHardMode v
	--		GROUP BY v.[Name]
	--		FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	--		)



	----DECLARE @LowlySkilledModeJson NVARCHAR(MAX) = ''
	--IF OBJECT_ID('tempdb..#LowlySkilledMode') IS NOT NULL
	--		BEGIN
	--		DROP TABLE #LowlySkilledMode
	--		END

	--		CREATE TABLE #LowlySkilledMode
	--		(
	--		Seq INT NOT NULL,
	--		[Name] VARCHAR(50) NOT NULL,
	--		ProfileDisplayName VARCHAR(200) NULL,
	--		UserID INT NULL,
	--		Score INT NOT NULL
	--		)

	--		INSERT INTO #LowlySkilledMode
	--		SELECT TOP 25 
	--		1 ,'LowlySkilledMode',
	--		ISNULL(STRING_ESCAPE(NULLIF(MAX(u.ProfileDisplayName),''),'json'),'UserNameHidden')
	--		,MAX(ISNULL(u.UserID,0)) AS UserID
	--		,MIN(l.Score) AS Score 
	--		FROM common.leaderboard l
	--		LEFT OUTER JOIN common.Users u ON  l.UserName = u.SteamID or l.UserID=u.UserID
	--		WHERE l.LastCompletedLevel=@Level AND l.isHardcore=0 AND l.isEazyMode=0
	--		GROUP BY CASE WHEN NULLIF(u.ProfileDisplayName,'') IS NULL THEN CAST(l.UserName AS VARCHAR) ELSE u.ProfileDisplayName END
	--		ORDER BY MIN(l.Score) ASC

	--		SELECT @LowlySkilledModeJson = 
	--		(
	--		SELECT v.[Name]
	--		, 1403 AS 'tournamentID'
	--		, 'Score' AS columnName
	--		, 'Score' AS description
	--		, 'THE LOWLY (Skilled)' AS title
	--		, CONCAT('Lowest score on ',@Level) AS subtitle
	--		,  CAST(1 AS BIT) AS 'hasLevels'
	--		,	JSON_QUERY
	--		(CONCAT	
	--			('[',
	--				(STRING_AGG
	--					(CONCAT
	--						('{', '"PlayerName": "', v.ProfileDisplayName, '"'
	--						, ',"Score": ', v.Score, ''
	--						, ',"UserID": ', v.UserID, ''
	--						, '}'
	--						)
	--					, ','
	--					) WITHIN GROUP (ORDER BY v.Score ASC)
	--				)
	--			, ']'
	--			) 
	--		) AS players
	--		FROM #LowlySkilledMode v
	--		GROUP BY v.[Name]
	--		FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
	--		)

			--commented out for testing

	SELECT
    JSON_QUERY(
    CONCAT(
    '[' 
	, ISNULL(JSON_QUERY(@HighScoreEazyModeJson),'{}')	, ',' 
	, ISNULL(JSON_QUERY(@HighScoreSkilledModeJson),'{}'), ',' 
	, ISNULL(JSON_QUERY(@HighScoreHardCoreJson),'{}')	, ',' 
	, ISNULL(JSON_QUERY(@RichestEasyModeJson),'{}')		, ',' 
	, ISNULL(JSON_QUERY(@RichestSkilledModeJson),'{}')	, ',' 
	, ISNULL(JSON_QUERY(@RichestHardCoreJson),'{}')		, ',' 
	, ISNULL(JSON_QUERY(@DeathlessHardCoreJson),'{}')	, ',' 
	, ISNULL(JSON_QUERY(@StingyBastardHCJson),'{}')		, ','
	,ISNULL(JSON_QUERY(@DodgyHardCoreJson),'{}')	, ',' 
	,ISNULL(JSON_QUERY(@LowlyEazyModeJson),'{}')  
    ,']'))
	AS DATA
    FOR JSON PATH, INCLUDE_NULL_VALUES, WITHOUT_ARRAY_WRAPPER
END