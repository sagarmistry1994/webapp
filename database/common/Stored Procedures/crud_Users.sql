﻿CREATE PROCEDURE [common].[crud_Users]
(
	@EmailID VARCHAR(256),
	@Pwd VARCHAR(2000),
	@ProfileDisplayName VARCHAR(200)
)
AS
BEGIN
SET NOCOUNT ON

DECLARE @usr_ins_cnt INT = 0
DECLARE @usr_upd_cnt INT = 0
DECLARE @UserIDNew INT =NULL
DECLARE @ErrMsg VARCHAR(MAX)
DECLARE @MaxUserID INT
DECLARE @IsHiddenEmailID BIT=0
DECLARE @Gender CHAR(1)=''
DECLARE	@CountryID SMALLINT=0
DECLARE	@GooglePlusProfile VARCHAR(500)=''
DECLARE	@WebsiteUrl VARCHAR(2048)=''
DECLARE	@IsEmailVerified BIT=0
DECLARE	@StateID INT=0

BEGIN TRY
	BEGIN -- Validation Starts

		IF (@EmailID='' AND @EmailID IS NULL)
		BEGIN
			SELECT 'EmailID should not be blank!' AS msg, 1 AS msgCode
			RETURN
		END
		IF (@Pwd='' AND @Pwd IS NULL)
		BEGIN
			SELECT 'Password should not be blank!' AS msg, 1 AS msgCode
			RETURN
		END

		IF EXISTS (SELECT 1 FROM common.Users WHERE ProfileDisplayName=@ProfileDisplayName) -- COLLATE SQL_Latin1_General_CP1_CS_AS)
		BEGIN
			SELECT 'Already username in the system!' AS msg, 1 AS msgCode
			RETURN
		END

		IF EXISTS (SELECT 1 FROM common.Users WHERE EmailID=LOWER(@EmailID))
		BEGIN
			SELECT 'Already email id present in the system!' AS msg, 1 AS msgCode
			RETURN
		END
	END ----Validation Ends
	BEGIN
	BEGIN TRANSACTION
		BEGIN	
			INSERT INTO common.Users 
			(
				EmailID
				,Pwd
				,ProfileDisplayName
			) 
			VALUES 
			(
				@EmailID
				,@Pwd
				,@ProfileDisplayName
			)
			SELECT @UserIDNew = @@IDENTITY

			IF ISNULL(@UserIDNew, 0) = 0
			BEGIN
				SELECT @ErrMsg = CONCAT('Something''s wrong while inserting record. New UserID is ', ISNULL(@UserIDNew, 0), '!');
				THROW 51000, @ErrMsg, 1;
			END

			SELECT 'Record inserted successfully!' AS msg, 0 AS msgCode, @UserIDNew AS UserID, @EmailID AS EmailID, @ProfileDisplayName AS ProfileDisplayName
		END
		COMMIT TRANSACTION
	END
END TRY
BEGIN CATCH
	IF @@TRANCOUNT > 0
	ROLLBACK TRANSACTION

	DECLARE @CRUDErrorsID INT = 0
	INSERT INTO dblog.CRUDErrors(CallerUserID, CalledSpName, SpParameter_String, SpErrorMsg, SpCalledOn) 
	SELECT 1,'common.crud_Users', CONCAT( 
	 '@EmailID', ' = ''', @EmailID,''', '
	, '@Pwd', ' = ''', @Pwd,''', '
	, '@ProfileDisplayName', ' = ''', @ProfileDisplayName,''', '
	)
	, ERROR_MESSAGE(), GETDATE()
	SELECT @CRUDErrorsID = SCOPE_IDENTITY()

	SELECT CONCAT('Awww snap.. something went wrong. Please contact administrator or try again later. Error reference ID - ', @CRUDErrorsID, '!') AS msg
	, 2 AS msgCode, @UserIDNew AS UserID
END CATCH
END