﻿CREATE TABLE [common].[Games] (
    [GameID]              SMALLINT       IDENTITY (1, 1) NOT NULL,
    [GameName]            VARCHAR (500)  NOT NULL,
    [GameShortDesc]       VARCHAR (200)  NULL,
    [GameDesc]            VARCHAR (2000) NULL,
    [GameTypeID]          SMALLINT       NOT NULL,
    [PlatformID]          TINYINT        NULL,
    [HomeBannerImageLink] VARCHAR (4000) CONSTRAINT [DF_Games_HomeBannerImageLink] DEFAULT ('') NOT NULL,
    [HomeBannerText]      VARCHAR (4000) CONSTRAINT [DF_Games_HomeBannerText] DEFAULT ('') NOT NULL,
    [IsEnabled]           BIT            CONSTRAINT [DF_Game_IsEnabled] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Game] PRIMARY KEY CLUSTERED ([GameID] ASC),
    CONSTRAINT [FK_Games_ToGamePlatforms] FOREIGN KEY ([PlatformID]) REFERENCES [common].[GamePlatforms] ([PlatformID]),
    CONSTRAINT [FK_Games_ToGameTypes] FOREIGN KEY ([GameTypeID]) REFERENCES [common].[GameTypes] ([GameTypeID])
);

