﻿CREATE TABLE [common].[GamePlatforms] (
    [PlatformID]   TINYINT        IDENTITY (1, 1) NOT NULL,
    [PlatformName] VARCHAR (100)  NULL,
    [PlatformDesc] VARCHAR (2000) NULL,
    CONSTRAINT [PK_GamePlatforms] PRIMARY KEY CLUSTERED ([PlatformID] ASC)
);

