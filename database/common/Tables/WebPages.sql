﻿CREATE TABLE [common].[WebPages] (
    [WebPageID]   SMALLINT      IDENTITY (1, 1) NOT NULL,
    [WebPageName] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_WebPages] PRIMARY KEY CLUSTERED ([WebPageID] ASC)
);

