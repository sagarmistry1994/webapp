﻿CREATE TABLE [common].[UserSocialLinks] (
    [UserSocialLinkID] INT            IDENTITY (1, 1) NOT NULL,
    [UserID]           INT            NOT NULL,
    [SocialAccountID]  TINYINT        NULL,
    [SocialLinkUrl]    VARCHAR (4000) NULL,
    [CreatedByUserID]  INT            NOT NULL,
    [CreatedOn]        DATETIME       NULL,
    [ModifiedByUserID] INT            NULL,
    [ModifiedOn]       DATETIME       NULL,
    [SocialSpecialID]  NUMERIC (30)   NULL,
    CONSTRAINT [PK_UserSocialLinks] PRIMARY KEY CLUSTERED ([UserSocialLinkID] ASC),
    CONSTRAINT [UK_UserSocialLinks_UserIDSocialAccountID] UNIQUE NONCLUSTERED ([UserID] ASC, [SocialAccountID] ASC)
);

