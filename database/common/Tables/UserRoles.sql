﻿CREATE TABLE [common].[UserRoles] (
    [RoleID]   TINYINT      IDENTITY (1, 1) NOT NULL,
    [RoleName] VARCHAR (50) NOT NULL,
    CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED ([RoleID] ASC)
);

