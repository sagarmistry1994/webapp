﻿CREATE TABLE [common].[InGameUserDetails] (
    [InGameUserDetailsID] INT            IDENTITY (0, 1) NOT NULL,
    [UserID]              INT            NOT NULL,
    [GameID]              SMALLINT       DEFAULT ((1)) NOT NULL,
    [InGameUserID]        NUMERIC (18)   NULL,
    [InGameUserName]      NVARCHAR (300) NULL,
    [CreatedOn]           DATETIME       DEFAULT (getdate()) NULL,
    [ModifiedOn]          DATETIME       NULL,
    CONSTRAINT [InGameUserName_PK] PRIMARY KEY CLUSTERED ([InGameUserDetailsID] ASC),
    CONSTRAINT [InGameUserName_FK] FOREIGN KEY ([GameID]) REFERENCES [common].[Games] ([GameID]),
    CONSTRAINT [InGameUserName_FK_1] FOREIGN KEY ([UserID]) REFERENCES [common].[Users] ([UserID])
);

