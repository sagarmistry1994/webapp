﻿CREATE TABLE [common].[Images] (
    [ImageID]   INT            IDENTITY (1, 1) NOT NULL,
    [ImageName] VARCHAR (200)  NOT NULL,
    [ImageUrl]  VARCHAR (4000) CONSTRAINT [DF_Images_ImageUrl] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_Images] PRIMARY KEY CLUSTERED ([ImageID] ASC)
);

