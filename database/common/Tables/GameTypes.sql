﻿CREATE TABLE [common].[GameTypes] (
    [GameTypeID]  SMALLINT      IDENTITY (1, 1) NOT NULL,
    [Description] VARCHAR (100) NOT NULL,
    CONSTRAINT [PK_GameTypes] PRIMARY KEY CLUSTERED ([GameTypeID] ASC)
);

