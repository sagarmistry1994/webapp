﻿CREATE TABLE [common].[Country] (
    [CountryID]        TINYINT       IDENTITY (1, 1) NOT NULL,
    [CountryName]      VARCHAR (200) NOT NULL,
    [CountryShortName] VARCHAR (200) NOT NULL,
    [IsActive]         BIT           CONSTRAINT [DF_Country_IsActive] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED ([CountryID] ASC)
);

