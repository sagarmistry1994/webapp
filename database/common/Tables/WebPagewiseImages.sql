﻿CREATE TABLE [common].[WebPagewiseImages] (
    [WebPageID] SMALLINT NOT NULL,
    [ImageID]   INT      NOT NULL,
    [GameID]    SMALLINT NULL,
    CONSTRAINT [PK_WebPagewiseImages] PRIMARY KEY CLUSTERED ([WebPageID] ASC, [ImageID] ASC)
);

