﻿CREATE TABLE [common].[UserTeams] (
    [TeamID]           INT             IDENTITY (1, 1) NOT NULL,
    [TeamName]         NVARCHAR (2000) NOT NULL,
    [TeamIcon_Link]    VARCHAR (2000)  NOT NULL,
    [TeamBanner_Link]  VARCHAR (2000)  NOT NULL,
    [TeamMember_Count] SMALLINT        NOT NULL,
    [IsActive]         INT             CONSTRAINT [DF_UserTeams_IsActive] DEFAULT ((0)) NOT NULL,
    [GamesPlayed]      SMALLINT        CONSTRAINT [DF_UserTeams_TournamentJoined] DEFAULT ((0)) NOT NULL,
    [GamesWon]         SMALLINT        CONSTRAINT [DF_UserTeams_TournamentWon] DEFAULT ((0)) NOT NULL,
    [CreatedByUserID]  INT             NOT NULL,
    [CreatedOn]        DATETIME        CONSTRAINT [DF__dtl_UserT__Creat__3F115E1A] DEFAULT (getdate()) NULL,
    [ModifiedBy]       NCHAR (10)      NULL,
    [ModifiedOn]       DATETIME        NULL,
    CONSTRAINT [PK_UserTeams] PRIMARY KEY CLUSTERED ([TeamID] ASC),
    CONSTRAINT [UK_UserTeams_TeamName] UNIQUE NONCLUSTERED ([TeamName] ASC, [IsActive] ASC)
);

