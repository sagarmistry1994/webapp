﻿CREATE TABLE [common].[SocialAccounts] (
    [SocialAccountID]   TINYINT       IDENTITY (1, 1) NOT NULL,
    [SocialAccountName] VARCHAR (100) NULL,
    [IsForSignup]       BIT           CONSTRAINT [DF_SocialAccounts_IsForSignup] DEFAULT ((0)) NOT NULL,
    [IsForSocialLink]   BIT           CONSTRAINT [DF_SocialAccounts_IsForSocialLink] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_SocialAccounts] PRIMARY KEY CLUSTERED ([SocialAccountID] ASC),
    CONSTRAINT [UK_SocialAccounts_SocialAccountName] UNIQUE NONCLUSTERED ([SocialAccountName] ASC)
);

