﻿CREATE TABLE [common].[ETLTempLeaderboard] (
    [id]            INT             IDENTITY (1, 1) NOT NULL,
    [userName]      NVARCHAR (4000) NOT NULL,
    [lastLevel]     VARCHAR (500)   NOT NULL,
    [bulletsTotal]  INT             NOT NULL,
    [bulletsHit]    INT             NOT NULL,
    [pickups]       INT             NOT NULL,
    [cash]          INT             NOT NULL,
    [score]         INT             NOT NULL,
    [deaths]        INT             NOT NULL,
    [bulletHitPerc] INT             NOT NULL,
    [createdOn]     DATETIME        NULL,
    [modifiedOn]    DATETIME        NULL,
    CONSTRAINT [pk_ETLTempLeaderboard] PRIMARY KEY CLUSTERED ([id] ASC),
    CONSTRAINT [uk_ETLTempLeaderboard] UNIQUE NONCLUSTERED ([userName] ASC)
);

