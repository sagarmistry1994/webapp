﻿CREATE TABLE [common].[States] (
    [StateID]        INT           NULL,
    [StateShortCode] NVARCHAR (3)  NULL,
    [StateDesc]      VARCHAR (100) NULL
);

