import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import featuredTournamentsReducer from './reducers/featuredTournamentsReducer';
import leaderBoardReducer from './reducers/leaderBoardReducer';
import allGamesReducer from './reducers/allGamesReducer';
import userReducer from './reducers/userReducer';
import miscReducer from './reducers/miscReducer';

const rootReducer = combineReducers({
  routing: routerReducer,
  featuredTournaments: featuredTournamentsReducer,
  leaderBoard: leaderBoardReducer,
  allGames: allGamesReducer,
  user: userReducer, 
  misc: miscReducer,
})

export type IAppState = ReturnType <typeof rootReducer>

export default rootReducer



