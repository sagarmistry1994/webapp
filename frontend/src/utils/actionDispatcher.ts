import { Actions } from "./actions";
import {
  IFeaturedTournament,
  ILeaderBoardRow,
  IUserProfileNewDetails,
  ILevelBasedLb,
} from "./interfacePools";

const makeAction = <T extends Actions, P>(type: T) => (payload: P) => {
  return {
    type,
    payload,
  };
};

export const getFeaturedTournaments = makeAction<
  Actions.GET_FEATURED_TOURNAMENTS,
  IFeaturedTournament[]
>(Actions.GET_FEATURED_TOURNAMENTS);

export const getLeaderBoardDetails = makeAction<
  Actions.GET_LEADERBOARD,
  ILeaderBoardRow
>(Actions.GET_LEADERBOARD);

export const getAllGames = makeAction<
  Actions.GET_ALL_GAMES,
  IFeaturedTournament[]
>(Actions.GET_ALL_GAMES);

export const getUserInfo = makeAction<
  Actions.GET_USER_INFO,
  IUserProfileNewDetails
>(Actions.GET_USER_INFO);

export const userLogOut = makeAction<Actions.USER_LOGOUT, "">(
  Actions.USER_LOGOUT
);

export const zIndex = makeAction<Actions.NEGATIVE_Z_INDEX, "">(
  Actions.NEGATIVE_Z_INDEX
);

export const getLevelBasedLb = makeAction<
  Actions.GET_LEVEL_BASED_LEADERBOARD,
  ILevelBasedLb
>(Actions.GET_LEVEL_BASED_LEADERBOARD);

interface IStringMap<T> {
  [key: string]: T;
}
type IAnyFunction = (...args: any[]) => any;
type IActionUnion<A extends IStringMap<IAnyFunction>> = ReturnType<A[keyof A]>;

const actions = {
  getFeaturedTournaments,
  getLeaderBoardDetails,
  getAllGames,
  getUserInfo,
  userLogOut,
  zIndex,
  getLevelBasedLb,
};

export type IAction = IActionUnion<typeof actions>;
