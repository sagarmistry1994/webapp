const Constants = {
  UserFields: {
    userName: "userName",
    userEmail: "userEmail",
    userPassword: "userPassword",
    userConfirmPassword: "userConfirmPassword",
    isEmailHiddenFromPublic: "isEmailHiddenFromPublic",
    userProfileName: "userProfileName",
    userGender: "userGender",
    userFb: "userFb",
    userTwitter: "userTwitter",
    userGooglePlus: "userGooglePlus",
    userWebsite: "userWebsite",
  },
  userToken: "userToken",
  userProfile: "userProfile",
  rememberMe: "rememberMe",
  UserID: "UserID",
};

export const ErrorMsg = {
  userNameEmpty: "Please enter username / email",
  userNameInvalid: "Invalid username / email!",
  otpEmpty: "Please enter OTP",
  otpInvalid: "Invalid OTP!",
  userPasswordEmpty: "Password cannot be blank",
  userPasswordInvalid: "Your password must be atleast 8 character long",
  userConfirmPassword: "Passwords dont match",
  userNameVerified: "Your email is already verified!",
};

export const errorMsg = {
  forbidden: "Cannot register with this email. Contact admin",
  registered: "Email already registered",
  server: "Server error, try again!",
  inCorrect: "OTP incorrect",
  invalid: "Invalid credentials!",
  userExists: "user already exists",
  noNetwork: "Network not connected. Please try later!",
  notFound: "Please enter correct email",
  optNotVerified: "Invalid OTP, please enter the OTP we've mailed",
  generic: "Error encountered, please try again!",
  userName: "Username already exists!",
  already:' already taken'
};

export const gameList = [
  {
    code: 1,
    column: "BulletsHit",
    dis: "Hit",
    title: "Bullets Hit",
    detailCol: "BulletsHit",
  },
  {
    code: 2,
    column: "BulletHitPerc",
    dis: "Hits %",
    title: "Bullets Hits %",
    detailCol: "BulletHitPerc",
  },
  {
    code: 3,
    column: "Pickups",
    dis: "Pickups",
    title: "Pickups",
    detailCol: "Pickups",
  },
  {
    code: 4,
    column: "Deaths",
    dis: "Deaths",
    title: "Deaths",
    detailCol: "Deaths",
  },
  { code: 5, column: "Cash", dis: "Cash", title: "Cash", detailCol: "Cash" },
];

export const userEdit = {
  UserID: "UserID",
  EmailID: "EmailID",
  UserName: "UserName",
  ProfileDisplayName: "ProfileDisplayName",
  ProfilePictureUrl: "ProfilePictureUrl",
  FacebookProfile: "FacebookProfile",
  TwitterProfile: "TwitterProfile",
  InstagramProfile: "InstagramProfile",
  SteamProfile: "SteamProfile",
  DiscordProfile: "DiscordProfile",
  XboxProfile: "XboxProfile",
  PsProfile: "PsProfile",
  StateShortCode: "StateShortCode",
  CountryName: "CountryName",
  About: "About",
  Followers: "Followers",
  InGameUserID: "InGameUserID",
};

export default Constants;
