import axios from "axios";
import { getLocalStorage } from "./commonUtils";
import Constants from "./Constants";

const baseUrl = "https://backend.indieesportsleague.com/api/v1/";
// const baseUrl = "http://localhost:3002/api/v1/";

//https://backend.indieesportsleague.com/api/v1/leaderboard/lb/new/1/14
//https://backend.indieesportsleague.com/api/v1/leaderboard/lb/14/10/1

// WEB SERVICES FOR AXIOS
export const featuredTournamentUrl = `${baseUrl}leaderboard/featuredtournament`;

export const allGamesUrl = `${baseUrl}games/all`;

export const leaderBoardBasedOnGameUrl = `${baseUrl}leaderboard/tournament/`;

export const sortGamesUrl = `${baseUrl}leaderboard/gamewise/`;

export const chkUserNameUrl = `${baseUrl}users/username/`;

export const chkUserEmailUrl = `${baseUrl}users/email/`;

export const imageUploadUrl = `${baseUrl}users/profile/upload`;

export const getAllCountriesUrl = `${baseUrl}users/country`;

export const userRegistrationUrl = `${baseUrl}users/registration`;

export const userLoginUrl = `${baseUrl}users/login`;

export const verifyEmailUrl = `${baseUrl}users/otp`;

export const verifyEmailOtpUrl = `${baseUrl}users/verifyemail`;

export const verifyOtpUrl = `${baseUrl}users/verifyotp`;

export const changePasswordUrl = `${baseUrl}users/changepasword`;

export const userUrl = `${baseUrl}users/userProfile/`;

export const allLeaderUrl = `${baseUrl}leaderboard/lb`;

export const getProfileImages = `${baseUrl}users/profile`;

export const getHeaderImages = `${baseUrl}users/header`;

export const userProfileget = `${baseUrl}users/userProfileget`;

export const userProfilegetPvt = `${baseUrl}users/userProfilegetPvt`;

export const userNameUpdate = `${baseUrl}users/userNameUpdate`;

export const userLocation = `${baseUrl}users/stateAndcountry`;

export const userImageUpdate = `${baseUrl}users/headerimageUpdate`;

export const levelBasedLb = `${baseUrl}leaderboard/lb/new/`;

export const gameStringUpdate = `${baseUrl}users/gameString`;

//HEADER FOR AXIOS
export const commonHeader = { "Content-Type": "application/json" };
export const imgHeader = { "Content-Type": "multipart/form-data" };
export const imgHeader1 = { "Content-Type": "image/png" };
export const tokenHeader = {
  "Content-Type": "application/json",
  Authorization: `Bearer ${getLocalStorage(Constants.userToken)}`,
};

export const tokenImgHeader = {
  "Content-Type": "image/png",
  Authorization: `Bearer ${getLocalStorage(Constants.userToken)}`,
};

export const getMethod = async (
  url: string,
  postLogin?: boolean
): Promise<any> => {
  try {
    const head = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getLocalStorage(Constants.userToken)}`,
    };
    return await axios
      .get(url, { headers: postLogin && postLogin ? head : commonHeader })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        console.log(err);
        return err;
      });
  } catch {
    console.log("netwok error");
  }
};

export const postMethod = async (
  url: string,
  data: Object,
  postLogin?: boolean
): Promise<any> => {
  try {
    const head = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getLocalStorage(Constants.userToken)}`,
    };
    return await axios
      .post(url, data, {
        headers: postLogin && postLogin ? head : commonHeader,
      })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        console.log(err);
        return err;
      });
  } catch {
    console.log("netwok error");
  }
};

export const putMethod = async (
  url: string,
  data: Object,
  postLogin?: boolean
): Promise<any> => {
  try {
    const head = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${getLocalStorage(Constants.userToken)}`,
    };
    return await axios
      .put(url, data, {
        headers: postLogin && postLogin ? head : commonHeader,
      })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        console.log(err);
        return err;
      });
  } catch {
    console.log("netwok error");
  }
};

export const uploadMethod = async (
  url: string,
  data: Object,
  postLogin?: boolean
): Promise<any> => {
  try {
    const head = {
      "Content-Type": "image/png",
      Authorization: `Bearer ${getLocalStorage(Constants.userToken)}`,
    };
    return await axios
      .post(url, data, {
        headers: postLogin && postLogin ? head : imgHeader,
      })
      .then((res) => {
        console.log(res);
        return res;
      })
      .catch((err) => {
        console.log(err);
        return err;
      });
  } catch {
    console.log("netwok error");
  }
};
