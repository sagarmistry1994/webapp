import React from "react";
// import LoadingData from "./common/LoadingData";
import { capitaliseWord } from "../utils/commonUtils";
import { Link } from "react-router-dom";

interface IListProps {
  loading?: boolean;
  codeId: number;
  searchTerm: string;
  data: any | {};
  [key: string]: any;
  columns: any;
}

function LeaderBoardList(props: IListProps) {
  const { searchTerm, data, userId, columns } = props;
  return (
    <>
      {/* <LoadingData isLoading={loading} /> */}
      {/* <div className="format-leaderboard "> */}
      <table className="leaderboard-rankings ">
        <thead>
          <tr>
            {columns && columns.length > 0
              ? columns.map((item: any, key: number) => {
                  return <th key={key}>{item.description}</th>;
                })
              : null}
          </tr>
        </thead>
        <tbody>
          {data &&
            data.players &&
            data.players.map((list: any, key: number) => {
              return (
                <tr
                  key={key}
                  className={`${
                    (list.PlayerName &&
                      list.PlayerName.toLowerCase()
                        .trim()
                        .indexOf(
                          searchTerm && searchTerm.toLowerCase().trim()
                        ) !== -1) ||
                    userId === list.UserID
                      ? "highlighted"
                      : "false"
                  }`}
                >
                  {/* <tr key={key}> */}
                  {/* <td>{key + 1}</td>
                  {list.UserID === userId ? (
                    <td>
                      <Link to={{pathname: `/user`}}>
                        {capitaliseWord(list.PlayerName)}
                      </Link>
                    </td>
                  ) : (
                    <td>
                      <Link to={{pathname: `/user/${list.UserID}`}}>
                        {capitaliseWord(list.PlayerName)}
                      </Link>
                    </td>
                  )}
                  <td>{list[list.columnName]}</td> */}
                  {data.columns.map((item: any, key: number) => {
                    return (
                      <td key={key} data-id={list.UserID}>
                        {item.columnName === "PlayerName" ? (
                          list.UserID === userId ? (
                            <Link to={{ pathname: `/user` }}>
                              {capitaliseWord(list.PlayerName)}
                            </Link>
                          ) : (
                            <Link to={{ pathname: `/user/${list.UserID}` }}>
                              {capitaliseWord(list.PlayerName)}
                            </Link>
                          )
                        ) : (
                          list[item.columnName]
                        )}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
        </tbody>
      </table>
      {/* </div> */}
    </>
  );
}

export default LeaderBoardList;
