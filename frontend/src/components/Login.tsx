import React from "react";
import FormTemplate from "./FormTemplate";
import TextInput from "./formFields/TextInput";
import axios from "axios";
import { userLoginUrl, commonHeader } from "../utils/apis";
import { ILoginState } from "../utils/interfacePools";
import { setLocalStorage, removeFromLocalStorage } from "../utils/commonUtils";
import Constants from "../utils/Constants";
import store from "../store";
import { userLogOut } from "../utils/actionDispatcher";

type ILoginProps = any;

class Login extends React.Component<ILoginProps, ILoginState> {
  constructor(props: ILoginProps) {
    super(props);

    this.state = {
      userName: "",
      userPassword: "",
      isInvalidCredentials: false,
      isFormInvalid: false,
      showSuccessMsg: false,
    };
  }

  componentDidMount() {
    store.dispatch(userLogOut(""));
    removeFromLocalStorage(Constants.userToken);
    removeFromLocalStorage(Constants.userProfile);
  }

  handleUserFields = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { name, value },
    } = e;
    this.setState({ [name]: value });
  };

  handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    setTimeout(() => {
      const { userName, userPassword } = this.state;
      if (userName && userPassword) {
        this.setState({ isFormInvalid: false });
        const form = {
          userName: userName.toLowerCase(),
          password: userPassword,
        };
        this.sendFormFields(JSON.stringify(form));
      } else {
        this.setState({ isFormInvalid: true });
      }
    }, 1000);
  };

  async sendFormFields(param: Object) {
    const url = userLoginUrl;
    axios
      .post(url, param, { headers: commonHeader })
      .then((res) => {
        if (res.data.statuscode === 200) {
          this.setState({ isInvalidCredentials: false });
          const response = res.data.data[0];
          const token = res.data.token;
          setLocalStorage(Constants.userToken, token);
          // setLocalStorage(Constants.userProfile, JSON.stringify(response));
          // store.dispatch(getUserInfo(response));
          this.postRegistration(response.userProfile.UserID);
          return;
        }
      })
      .catch((err) => {
        console.log(err);
        if (err.response.status === 401) {
          this.setState({ isInvalidCredentials: true });
        }
        return;
      });
  }

  postRegistration(id?: any) {
    this.setState({ showSuccessMsg: true });
    setTimeout(() => this.props.history.push(`/editProfile/`), 1000);
  }

  render() {
    const { isFormInvalid, showSuccessMsg, isInvalidCredentials } = this.state;
    return (
      <>
        {showSuccessMsg ? (
          <div className="registration-success">
            <span className="loading">Login successful</span>
          </div>
        ) : null}
        <FormTemplate
          customStyle={true}
          title={"Login"}
          subTitle1={"Forgot Password?"}
          subTitle2={"Resend verification Email?"}
          subLink1={"/forgot-password"}
          subLink2={"/verify-email"}
          submitBtn1={"Login"}
          submitBtn2={"Create an account"}
          onSubmit={this.handleSubmit}
          trivialUrl={"/register"}
        >
          <div className="form-group">
            <label className="control-label">
              Username<span className="required-red">*</span>
            </label>
            <div className="fields-holder">
              <TextInput
                name={"userName"}
                type={"text"}
                required={"true"}
                autoComplete={"off"}
                onChange={this.handleUserFields}
                minLength={8}
              />
            </div>
          </div>
          <div className="form-group">
            <label className="control-label">
              Password<span className="required-red">*</span>
            </label>
            <div className="fields-holder">
              <TextInput
                name={"userPassword"}
                type={"password"}
                required={"true"}
                autoComplete={"off"}
                onChange={this.handleUserFields}
              />
            </div>
          </div>
          <p></p>
          {isFormInvalid ? (
            <span className="reg-error">Please fill in both fields</span>
          ) : null}
          {isInvalidCredentials ? (
            <span className="reg-error">Invalid Credentials. Try again</span>
          ) : null}
        </FormTemplate>
      </>
    );
  }
}

export default Login;
