import React from "react";
import { RouteComponentProps } from "react-router-dom";
import { connect } from "react-redux";
import {
  ILeaderBoardRow,
  IUserProfileNewDetails,
} from "../utils/interfacePools";
import { IAppState } from "./../reducer";

import { getLeaderBoardDetails } from "../utils/actionDispatcher";
import { getMethod, allLeaderUrl } from "../utils/apis";
import LeaderBoardList from "./LeaderBoardList";
import store from "../store";
import { Link } from "react-router-dom";
import {
  IsUserProfileLocalStorage,
  getLocalStorage,
} from "../utils/commonUtils";
import Constants from "../utils/Constants";
import Header from "./Header";
import Footer from "./Footer";
interface IProps {
  afas?: string;
}

interface IGameLeaderBoardProps extends RouteComponentProps<{ id: string }> {}

type Props = IProps & IGameLeaderBoardProps & LinkStateProps;

interface LinkStateProps {
  leaderBoard: ILeaderBoardRow;
  userDetails?: IUserProfileNewDetails;
}

interface IState {
  leaderBoard: ILeaderBoardRow;
  gameBg: string;
  featuredPlayerBg: string;
  showLoading?: boolean;
  noData?: boolean;
  searchTerm?: string;
  sendTerm?: string;
  error?: string;
  count?: number;
  showrankstatus?: boolean;
  userId?: number;
  statussubtitle?: string;
  changelevel?: string;
  columns?: any;
  currentpage?:any,
  nolevel?:any
}

const mapStateToProps = (
  state: IAppState,
  ownProps: IProps
): LinkStateProps => {
  return {
    leaderBoard: state.leaderBoard.leaderboard,
    userDetails: state.user,
  };
};

class GameLeaderBoard extends React.Component<Props, IState> {
  constructor(props: Props) {
    super(props);
    this.state = {
      leaderBoard: props.leaderBoard ? props.leaderBoard : [],
      gameBg: "",
      featuredPlayerBg: "",
      sendTerm: "",
      searchTerm: "",
      showrankstatus: false,
      statussubtitle: "",
      changelevel: "",
      nolevel:1,
      columns: props.leaderBoard ? props.leaderBoard.columns : [],
      currentpage:1
    };

    
  }
  componentDidMount() {
    this.getLeaderBoardMethod();
    const { userDetails } = this.props;
    const a = userDetails && userDetails.UserID;
    const b = IsUserProfileLocalStorage();
    const c = getLocalStorage(Constants.userToken);
    if ((a || b) && c) {
      this.setState({ userId: a || b });
    } else {
      // console.log('user not logged')
    }
    window.scrollTo(0, 0);
  }

  private handleChangeLevel = (e: any) => {
    const startvalue = this.props.leaderBoard["subtitle"];
    // console.log(startvalue?.split('on'))
    var dataset = e.target.options[e.target.selectedIndex].dataset;
    const level = e.target.value;
    this.setState(
      {
        nolevel:level,
        changelevel: startvalue?.split("on")[0] + " on " + dataset.valuename,
      },
      () => {
        this.getLeaderBoardMethod(level,1);
      }
    );
  };

  getLeaderBoardMethod = async (level: any = "1",pagenumber:any = 1): Promise<void> => {
    this.setState({ showLoading: true, noData: false, leaderBoard: [] });
    const currentUrl = this.props.match.params.id;
    const {changelevel} = this.state
    this.setState({
      currentpage:pagenumber
    });

    const url = `${allLeaderUrl}/${currentUrl}/20/${level}/1/${pagenumber}`;
    console.log(url)
    let res = await getMethod(url);
    if (res && res.response && res.response.status === 404) {
      const a = { ...this.props.leaderBoard, players: [], subtitle: changelevel ? changelevel: this.props.leaderBoard.subtitle };
      store.dispatch(getLeaderBoardDetails(a));
      this.setState({
        leaderBoard: this.props.leaderBoard,
        noData: true,
        showLoading: false,
        statussubtitle: "",
        changelevel: changelevel ? changelevel: this.props.leaderBoard.subtitle,
        // currentpage:pagenumber
      });
    }
     else if (res && res.response && res.response.status === 500) {
      const a = { ...this.props.leaderBoard, players: [], subtitle: changelevel ? changelevel: this.props.leaderBoard.subtitle };
      store.dispatch(getLeaderBoardDetails(a));
      this.setState({ leaderBoard: a, showLoading: false, noData: true });
    } else if (res && res.data && res.data.statuscode === 200) {
      const data = res.data.data[0];
      const response = { ...this.props.leaderBoard, subtitle: changelevel ? changelevel: this.props.leaderBoard.subtitle, ...data,  };

      store.dispatch(getLeaderBoardDetails(response));
      this.setState({
        leaderBoard: this.props.leaderBoard,
        noData: false,
        showLoading: false,
        statussubtitle: this.props.leaderBoard["subtitle"],
        columns: this.props.leaderBoard.columns,
        // currentpage:pagenumber
      });
    } else if (res && !res.status) {
      const a = { ...this.props.leaderBoard, players: [], subtitle: changelevel ? changelevel: this.props.leaderBoard.subtitle  };
      store.dispatch(getLeaderBoardDetails(a));
      this.setState({
        leaderBoard: this.props.leaderBoard,
        noData: true,
        showLoading: false,

      });
    }
  };

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ searchTerm: e.target.value.trim() });
  };
  handleChange1 = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ currentpage: e.target.value });
    this.getLeaderBoardMethod(1,this.state.currentpage);
    window.scrollTo(0, 0);
  };
  handlenext = (e:number)=>{
    this.getLeaderBoardMethod(this.state.nolevel,e + 1 );
    window.scrollTo(0, 0);
  }
  handleprevious = (e:number)=>{
    this.getLeaderBoardMethod(this.state.nolevel,e - 1 );
    window.scrollTo(0, 0);
  }
  handlefirst = (e:number)=>{
    this.getLeaderBoardMethod(this.state.nolevel,e)
    window.scrollTo(0, 0);
  }
  handlelast = (e:number)=>{
    this.getLeaderBoardMethod(this.state.nolevel,e)
    window.scrollTo(0, 0);
  }

  scrollToNode=(node:any)=>{
		node.scrollIntoView({ behavior: 'smooth' });
	}
  handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (this.state.searchTerm) {
      this.setState({
        sendTerm: this.state.searchTerm,
        error: "",
      });
      const { leaderBoard, searchTerm } = this.state;
      let data = leaderBoard.players.filter(
        (item: any) =>
          item.PlayerName &&
          item.PlayerName.toLowerCase()
            .trim()
            .includes(searchTerm && searchTerm.toLowerCase().trim())
      );
      if (data && data.length > 0) {
        this.setState({ count: data.length, });
      } else {
        this.setState({ error: "Player not found" });
      }
    } else {
      this.setState({
        // error: "search is empty",
        error: "Please enter player name",
        sendTerm: this.state.searchTerm,
      });
    }
  };

  // clearSearch = () => {
  //   this.setState({ searchTerm: "", sendTerm: "", error: "" });
  // };
  showrank = () => {
    this.setState({
      showrankstatus: !this.state.showrankstatus,
    });
  };
  render() {
    const { leaderBoard } = this.props;
    const { userId, changelevel, columns,currentpage } = this.state;
    return (
      <>
      <Header/>
        <div id="content">
          <div className="component component-game-masthead">
            {/* <div className="game-bg-holder all-boards"> */}
            <div className="component-inner">
              <div className="intro">
                <h1>1993 SPACE MACHINE</h1>
                <div className="buy">
                  <div className="cta">
                    GET GAME <span></span>
                  </div>
                  {/* <label className="getgamelabel">GET GAME <span></span></label> */}
                  <ul className="providers">
                    <li>
                      <a
                        href="https://store.steampowered.com/app/373480/1993_Space_Machine/"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        STEAM
                      </a>
                    </li>
                    {/* <li>
                    <Link to={{ pathname: "" }}>Xbox</Link>
                    </li>
                    <li>
                    <Link to={{ pathname: "" }}>Playstation</Link>
                    </li>
                    <li>
                    <Link to={{ pathname: "" }}>EPIC GAME STORE</Link>
                    </li>
                    <li>
                    <Link to={{ pathname: "" }}>STADIA</Link>
                    </li>
                    <li>
                      <Link to={{ pathname: "" }}>GOG</Link>
                    </li> */}
                  </ul>
                </div>
              </div>
            </div>
            {/* <div className="component-inner carousel-container">
              <div
                className="carousel"
                data-flickity='{ "cellAlign": "left", "contain": true, "freeScroll": false, "wrapAround": true, "imagesLoaded": true }'
              >
                <div className="carousel-item video">
                  <img
                    src={require("../media/screen1.jpg")}
                    alt="Game carousels 1"
                  />
                  <div className="overlay video-play"></div>
                </div>

                <div className="carousel-item image">
                  <img
                    src={require("../media/screen2.jpg")}
                    alt="Game carousels 2"
                  />
                </div>
              </div>
            </div> */}
            <div className="component-inner game-meta">
              <div className="publisher">
                <h3 className="publisher-name">LIMIT BREAK STUDIOS</h3>
                <p className="publisher-type">ARCADE SPACE SHOOTER</p>
              </div>

              {/* <ul className="platforms">
                <li className="platform-item">
                  <div className="format-icon steam"></div>
                </li>
                <li className="platform-item">
                  <div className="format-icon xbox"></div>
                </li>
                <li className="platform-item">
                  <div className="format-icon windows"></div>
                </li>
              </ul> */}
            </div>
            {/* </div> */}
          </div>
          <div
            className="component component-leaderboard-single"
            style={{ paddingTop: 20 }}
          >
            <div className="component-inner">
              {/* <div className="row"> */}
              {/* <div className="col-sm-12 col-xs-12"> */}
              {/* <div className="intro">
              <a className="left format-button secondary" href='/leagues'>Back</a>
                <span className="right format-button primary">FIND YOUR RANK</span>
                <h2 className="format-section-title">{gameList[parseInt(this.props.match.params.id) - 1].title} {" "} LEADERBOARD</h2>
              </div> */}
              <div className="intro">
                {leaderBoard && leaderBoard.hasLevels ? (
                  <>
                    <div className="wrapingheaderdetail">
                      <h2 className="format-section-title">
                        {leaderBoard && leaderBoard.title}
                      </h2>

                      <div className="selectwrap">
                        <select onClick={(e) => this.handleChangeLevel(e)}>
                          {leaderBoard.levels &&
                            leaderBoard.levels.map((level: any, key) => {
                              return (
                                <option
                                  key={key}
                                  data-id={level.levelId}
                                  value={level.levelId}
                                  data-valuename={level.levelName}
                                >
                                  {level.levelName}
                                </option>
                              );
                            })}
                        </select>
                      </div>
                    </div>
                  </>
                ) : (
                  <h2 className="format-section-title">
                    {leaderBoard && leaderBoard.title}
                  </h2>
                )}

                <div className="actions">
                  {this.state.error && (
                    <div className="format-messages">
                      <ul className="msg-error">
                        <li>{this.state.error}</li>
                      </ul>
                    </div>
                  )}
                  <form onSubmit={this.handleSubmit}>
                    <div className="combined-input-button">
                      <input
                        type="text"
                        name="game-title"
                        placeholder="Search users"
                        onChange={this.handleChange}
                        value={this.state.searchTerm && this.state.searchTerm}
                      />
                      {/* <TextInput
                          onChange={this.handleChange}
                          // error={this.state.error}
                          placeholder="Search users"
                          value={this.state.searchTerm && this.state.searchTerm}
                        /> */}
                      <button>Submit</button>
                    </div>
                  </form>

                  <span className="find-rank format-button primary">
                    FIND YOUR RANK
                  </span>
                </div>
              </div>
              <div className="intro">
                <p className="format-section-desc">
                  {changelevel ? changelevel : leaderBoard.subtitle}
                  {/* {leaderBoard && leaderBoard.subtitle
                    ? leaderBoard.subtitle
                    : `${changelevel}`} */}
                </p>
                <p className="format-section-desc">
                No of Players: {!this.state.noData && !this.state.showLoading ?leaderBoard.noOfPlayers:'0'}
                </p>
              </div>
              {/* </div> */}
              <div className="format-leaderboard  animation fade-in animation-queued" ref={(node) => node}>
                {/* <div className="game-card-leaderboard-wrapper "> */}
                {/* {
                    this.state.showrankstatus?
                    <div className="id-search-wrap">
                    <div>
                      <div className="form-head">What's your rank?</div>
                      <form onSubmit={this.handleSubmit}>
                        <TextInput
                          onChange={this.handleChange}
                          // error={this.state.error}
                          placeholder="Enter user ID"
                          value={this.state.searchTerm && this.state.searchTerm}
                        />
                        <button>Ok</button>
                      </form>
                    </div>
                  </div>:null
                  } */}

                {/* ( */}
                <LeaderBoardList
                  data={this.state.leaderBoard}
                  columns={columns && columns}
                  userId={userId && userId}
                  codeId={parseInt(this.props.match.params.id) - 1}
                  searchTerm={
                    this.state.sendTerm ? this.state.sendTerm : "null"
                  }
                />
                {/* )} */}
                {this.state.noData ? (
                  <p style={{ marginTop: "2rem", textAlign: "center" }}>
                    No data found
                  </p>
                ) : null}
                {this.state.showLoading ? (
                  <p style={{ marginTop: "2rem", textAlign: "center" }}>
                    Loading...
                  </p>
                ) : null}
                <div className="format-pagination">
                <form>
                    <ul className="pages-links">
                    <li className={"page-link " + (currentpage === 1 ?'disabled':'')} onClick={()=>this.handlefirst(1)} >
                      <Link to={{ pathname: "" }}>«</Link>
                    </li>
                    <li className={"page-link " + (currentpage === 1 ?'disabled':'')} onClick={()=>this.handleprevious(currentpage && currentpage)}>
                      <Link to={{ pathname: "" }}>‹</Link>
                    </li>
                    <li className="current-page">
                      <input type="number" name="page" onChange={this.handleChange1}
                        value={currentpage && currentpage} disabled={true}/>{" "}
                <span>of {leaderBoard && leaderBoard.pageTotal ? leaderBoard.pageTotal: 1}</span>
                    </li>
                    <li className={"page-link " + (currentpage === leaderBoard.pageTotal?'disabled':'')} onClick={()=>this.handlenext(currentpage && currentpage)}>
                      <Link to={{ pathname: "" }} >›</Link>
                    </li>
                    <li className={"page-link " + (currentpage === leaderBoard.pageTotal ?'disabled':'')} onClick={()=>this.handlelast(leaderBoard && leaderBoard.pageTotal)}>
                      <Link to={{ pathname: "" }}>»</Link>
                    </li>
                    </ul>
                  </form>
                </div>
                {/* </div> */}
              </div>
              {/* </div> */}
              <p className="cta">
                <Link
                  className="format-link-arrow"
                  to={{ pathname: `/leagues/101` }}
                >
                  SEE ALL LEADERBOARDS
                </Link>
                {/* <a className="format-link-arrow" href="/leagues">
                    SEE ALL LEADERBOARDS
                  </a> */}
              </p>
            </div>
          </div>
        </div>
        <Footer />
      </>
    );
  }
}

export default connect(mapStateToProps, {})(GameLeaderBoard);
