import React from "react";
import { connect } from "react-redux";
import { IUserProfileNewDetails } from "./../utils/interfacePools";
import { IAppState } from "../reducer";
import { RouteComponentProps } from "react-router-dom";
import store from "../store";
import { userLogOut, getUserInfo, zIndex } from "../utils/actionDispatcher";
// import { handleUserName } from "../components/Form/FormValdiations";
import axios from "axios";
import {
  removeFromLocalStorage,
  // isUserLogged,
  getLocalStorage,
  setLocalStorage,
  IsUserProfileLocalStorage,
  haveErrors,
} from "../utils/commonUtils";
import Constants, { errorMsg } from "../utils/Constants";
import NotLoggedInWB from "./common/NotLoggedInWB";
import {
  userUrl,
  putMethod,
  postMethod,
  getMethod,
  getProfileImages,
  getHeaderImages,
  userNameUpdate,
  userLocation,
  userProfilegetPvt,
  userImageUpdate,
} from "../utils/apis";
import HeaderWB from "./HeaderWB";
import FooterWB from "./FooterWB";
// import ProfileGameDetails from "./ProfileGameDetails";
import UpdateImageModal from "./UploadImageModal";

type IUserProfileProps = any;

interface LinkProps {
  userProfile: IUserProfileNewDetails;
}

export interface IErrors {
  [key: string]: string;
}

interface IUserProfProps extends RouteComponentProps<{ id: string }> {}

type Props = IUserProfileProps & LinkProps & IUserProfProps;

const mapStateToProps = (
  state: IAppState,
  ownProps: IUserProfileProps
): LinkProps => {
  return {
    userProfile: state.user,
  };
};

interface IUserState {
  isUserLoggedIn?: boolean;
  profileData?: any;
  showLoading?: boolean;
  showEditScreen?: boolean;
  errors: IErrors;
  [key: string]: any;
  errorFromApi?: string;
  btnLoader?: boolean;
  usrname?: string;
  submitted?: boolean;
  steamId?: string;
  noUserDetail?: boolean;
  showSteamSuccess?: boolean;
  isShowModal?: boolean;
  profileImagesList?: any;
  headerImagesList?: any;
  selectedProfileImage?: number;
  selectedHeaderImage?: number;
}

let defaultheader =
  "https://iel-webapp-profile-images.s3.amazonaws.com/images/profile/WP-Profile-bkg-1.jpg";
let defaultprofile =
  "https://iel-webapp-profile-images.s3.amazonaws.com/images/profile/WB-PROFILE-1.jpg";
  

class editProfileWB extends React.Component<Props, IUserState> {
  constructor(props: Props) {
    super(props);
    const errors: IErrors = {};
    this.state = {
      errors,
      profileData: props.userProfile ? props.userProfile : {},
      UserName: "",
      ProfileDisplayName: props.userProfile
        ? props.userProfile.ProfileDisplayName
        : "",
      ProfilePictureUrl: "",
      FacebookProfile: "",
      TwitterProfile: "",
      InstagramProfile: "",
      SteamProfile: "",
      DiscordProfile: "",
      XboxProfile: "",
      PsProfile: "",
      StateShortCode: "",
      CountryName: "",
      About: "",
      Followers: "",
      InGameUserID: null,
      steamId: "",
      usrname: "afgsd",
      showSteamSuccess: false,
      isShowModal: false,
      changeusername: false,
      inputValue: "",
      inputlocationValue: "",
      btnLoader: false,
      btnLoader1: false,
    };
  }

  componentDidMount() {
    const a = this.props.userProfile && this.props.userProfile.UserID;
    const b = IsUserProfileLocalStorage();
    const c = getLocalStorage(Constants.userToken);
    if ((a || b) && c) {
      // console.log("have id and token");
      this.setState({ isUserLoggedIn: true }, () => this.getUser());
    } else {
      this.setState({ isUserLoggedIn: false });
    }
    window.scrollTo(0, 0);
  }

  handleSteamLogin = () => {
    console.log("handle steam");
    const popupWindow = window.open(
      process.env.REACT_APP_API_URL + "/auth/steam",
      "_blank",
      "width=800, height=600"
    );
    window.focus && popupWindow && popupWindow.focus();
    this.steamCallack();
  };

  private steamCallack = () => {
    console.log("steam callback");
    window.addEventListener("message", (event) => {
      if (event.origin !== process.env.REACT_APP_API_URL) return;

      const { token, ok, steamid, personaname, realname } = event.data;
      if (ok) {
        console.log(token);
        console.log(steamid);
        console.log(personaname);
        console.log(realname);
        steamid &&
          this.setState(
            { steamId: steamid, personaName: personaname ? personaname : '', realName: realname ? realname : '' },
            () => {
              this.sendSteamData();
            }
          );
      }
    });
  };

  private sendSteamData = async (): Promise<void> => {
    console.log("send steam data");
    const { profileData, steamId, personaName, realName } = this.state;
    let data = {
      UserID: profileData.UserID.toString(),
      SteamID: steamId,
      personaName,
      realName,
    };
    console.log(data);
    let res = await postMethod(userUrl, data, true);
    if (res && res.response && res.response.status === 404) {
      console.log(res.response);
    } else if (res && res.data && res.data.statuscode === 200) {
      console.log(res.data);
      this.getUser();
      this.setState({ showSteamSuccess: true }, () => {
        setTimeout(() => {
          this.setState({ showSteamSuccess: false });
        }, 1000);
      });
    } else {
      console.log(res);
    }
  };

  private handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { name, value },
    } = e;
    this.setState({ [name]: value });
  };
  private haveErrors(errors: IErrors) {
    let haveError: boolean = false;
    Object.keys(errors).forEach((key: string) => {
      if (errors[key].length > 0) {
        haveError = true;
      }
    });
    return haveError;
  }
  handleSubmit = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
    e.preventDefault();
    const {
      profileData,
      UserName,
      ProfileDisplayName,
      ProfilePictureUrl,
      FacebookProfile,
      TwitterProfile,
      InstagramProfile,
      SteamProfile,
      DiscordProfile,
      XboxProfile,
      PsProfile,
      About,
      InGameUserID,
    } = this.state;
    this.setState(
      {
        errorFromApi: "",
        errors: {
          ...this.state.errors,
        },
      },
      () => {
        if (!haveErrors(this.state.errors)) {
          const form = {
            UserID: profileData.UserID.toString(),
            UserName,
            ProfileDisplayName,
            ProfilePictureUrl,
            FacebookProfile,
            TwitterProfile,
            InstagramProfile,
            SteamProfile,
            DiscordProfile,
            XboxProfile,
            PsProfile,
            About,
            InGameUserID,
          };
          // console.log("good to go");
          this.submitForm(form);
          // let res = await putMethod(userUrl,form,true)
          // console.log(form);
        } else {
          console.log("have errors");
        }
      }
    );
  };

  submitForm = async (form: any): Promise<void> => {
    this.setState({ btnLoader: true });
    let res = await putMethod(userUrl, form, true);
    if (res && res.response && res.response.status === 404) {
      // console.log("user not found");
      this.setState({ btnLoader: false });
    } else if (res && res.response && res.response.status === 500) {
      this.setState({ btnLoader: false });
    } else if (res && res.data && res.data.statuscode === 200) {
      // successfully updated
      this.setState({ btnLoader: false, submitted: true }, () => {
        this.getUserDetails(form.UserID);
        setTimeout(() => {
          this.setState({ submitted: false, showEditScreen: false });
          this.props.history.push("/userwb");
        }, 1000);
      });
    }
  };

  getUser = async (): Promise<void> => {
    if (this.props.userProfile && this.props.userProfile.UserID) {
      const userId = this.props.userProfile.UserID;
      this.getUserDetails(userId);
    } else {
      let userId = IsUserProfileLocalStorage();
      userId && this.getUserDetails(userId);
    }
  };

  getUserDetails = async (id: number): Promise<void> => {
    const token = getLocalStorage(Constants.userToken);
    const head = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    this.setState({ showLoading: true });
    const url = `${userProfilegetPvt}/${id}`;
    let res = await axios
      .get(url, { headers: head })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        console.log(err);
        return err;
      });
    if (res && res.response && res.response.status === 404) {
      // console.log("data not found");
      // store.dispatch(userLogOut(''))
      this.setState({ showLoading: false, noUserDetail: true });
    } else if (res && res.response && res.response.status === 401) {
      // store.dispatch(userLogOut(''))
      this.setState({ showLoading: false, noUserDetail: true });
    } else if (res && res.response && res.response.status === 500) {
      // store.dispatch(userLogOut(''))
      this.setState({ showLoading: false, noUserDetail: true });
      // console.log("server error");
    } else if (res && res.data && res.data.statuscode === 200) {
      const response = res.data.data[0];
      setLocalStorage(Constants.userProfile, JSON.stringify(response));
      store.dispatch(getUserInfo(response));
      this.setState(
        {
          profileData: this.props.userProfile,
          UserName: this.props.userProfile.UserName,
          ProfileDisplayName: this.props.userProfile.ProfileDisplayName,
          ProfilePictureUrl: this.props.userProfile.ProfilePictureUrl,
          FacebookProfile: this.props.userProfile.FacebookProfile,
          TwitterProfile: this.props.userProfile.TwitterProfile,
          InstagramProfile: this.props.userProfile.InstagramProfile,
          SteamProfile: this.props.userProfile.SteamProfile,
          DiscordProfile: this.props.userProfile.DiscordProfile,
          XboxProfile: this.props.userProfile.XboxProfile,
          PsProfile: this.props.userProfile.PsProfile,
          About: this.props.userProfile.About,
          InGameUserID: this.props.userProfile.InGameUserID,
          getPvtUser: response,
          noData: false,
          isOffline: false,
          inputValue:
            response && response.ProfileDisplayName
              ? response.ProfileDisplayName
              : `GamerID`,
          inputlocationValue:
            response && response.CountryNState
              ? response.CountryNState
              : "CA,USA",
          selectedProfileImage:
            response.ProfileImageID && response.ProfileImageID,
          selectedHeaderImage: response.HeaderImageID && response.HeaderImageID,
        },
        () => {
          this.getProfileImagesList();
          this.getHeaderImagesList();
          this.setState({ showLoading: false, noUserDetail: false });
        }
      );
      // console.log(response);
    }
  };

  private getProfileImagesList = async (): Promise<void> => {
    let res = await getMethod(getProfileImages, true);
    if (res && res.response && res.response.status === 404) {
      // this.setState({  })
    } else if (res && res.response && res.response.status === 500) {
      //
    } else if (res && res.data && res.data.statuscode === 200) {
      this.setState({ profileImagesList: res.data.data }, () => {
        this.setProfileImage();
      });
    } else if (res && !res.status) {
      // no network
    }
  };

  private getHeaderImagesList = async (): Promise<void> => {
    let res = await getMethod(getHeaderImages, true);
    if (res && res.response && res.response.status === 404) {
      // this.setState({  })
    } else if (res && res.response && res.response.status === 500) {
      //
    } else if (res && res.data && res.data.statuscode === 200) {
      this.setState({ headerImagesList: res.data.data }, () => {
        this.setHeaderImage();
      });
    } else if (res && !res.status) {
      // no network
    }
  };

  onLogout = () => {
    store.dispatch(userLogOut(""));
    removeFromLocalStorage(Constants.userToken);
    removeFromLocalStorage(Constants.userProfile);
    this.props.history.push("/");
  };

  showEditScreen = () => {
    this.setState({ showEditScreen: true });
  };

  closeEditScreen = () => {
    this.setState({ showEditScreen: false });
  };

  private setProfileImage = () => {
    const { selectedProfileImage, profileImagesList } = this.state;
    if (selectedProfileImage && selectedProfileImage) {
      let list = profileImagesList;
      let profileImage =
        list &&
        list.filter(
          (index: any) => index.ProfileImageID === selectedProfileImage
        )[0].ProfileImageURL;
      this.setState({ profileImage: profileImage });
      // console.log(profileImage)
    }
  };

  private setHeaderImage = () => {
    const { selectedHeaderImage, headerImagesList } = this.state;
    if (selectedHeaderImage && selectedHeaderImage) {
      let list = headerImagesList;
      let headerImage =
        list &&
        list.filter(
          (index: any) => index.HeaderImageID === selectedHeaderImage
        )[0].HeaderImageURL;
      this.setState({ headerImage: headerImage });
    }
  };

  toggleImageModal = () => {
    const { selectedHeaderImage, selectedProfileImage } = this.state;
    const { HeaderImageID, ProfileImageID } = this.props.userProfile;
    store.dispatch(zIndex(""));
    this.setState({ isShowModal: !this.state.isShowModal }, () => {
      if (
        !this.state.isShowModal &&
        (selectedHeaderImage !== HeaderImageID ||
          selectedProfileImage !== ProfileImageID)
      ) {
        this.postUserImages();
      }
    });
  };

  private postUserImages = async (): Promise<void> => {
    const { selectedHeaderImage, selectedProfileImage } = this.state;
    const data = {
      ProfileImageID: selectedProfileImage?.toString(),
      HeaderImageID: selectedHeaderImage?.toString(),
    };
    let res = await postMethod(userImageUpdate, data, true);
    if (res && res.response && res.response.status === 404) {
      //no data found
    } else if (res && res.data && res.data.statuscode === 200) {
      //submitted
      this.getHeaderImagesList();
      this.getProfileImagesList();
    } else if (res && !res.status) {
      //probable network issue
    }
  };

  handleProfileImageClick = (id: any) => {
    this.setState({ selectedProfileImage: id });
  };

  handleHeaderImageClick = (id: any) => {
    this.setState({ selectedHeaderImage: id });
  };
  changestxt = (id: any) => {
    // alert('hello')

    this.setState({
      // inputValue:
      // this.props.userProfile && this.state.getPvtUser.ProfileDisplayName
      //     ? this.state.getPvtUser.ProfileDisplayName
      //     : `GamerID`,
      changeusername: true,
    });
  };
  updateInputValue = (evt: any) => {
    evt.preventDefault();
    this.setState({
      errorusername: "",
      inputValue: evt.target.value.replace(/^\s+|\s+$/g, ""),
      errorFromApi: "",
    });
  };
  updateusername = (evt: any) => {
    evt.preventDefault();

    if (
      this.state.inputValue === undefined ||
      this.state.inputValue === null ||
      this.state.inputValue === ""
    ) {
      this.setState({
        errorusername: `Please enter Username`,
      });
    } else if (
      this.state.inputValue.length <= 2 ||
      this.state.inputValue.length >= 21
    ) {
      this.setState({
        errorusername: `Username should be 3 - 20 characters in length`,
      });
    } else if (this.state.inputValue.match("^[a-zA-Z0-9 ]*$") != null) {
      // if (evt.key === "Enter") {
      //   this.setState({
      //     inputValue: this.state.inputValue,
      //     // changeusername: false,
      //   });
      //   const form = { ProfileDisplayName: this.state.inputValue };
      //   this.submitCall(form);
      // }
      this.setState(
        {
          errorusername: "",
          inputValue: this.state.inputValue,
          changeusername: false,
        },
        () => {
          if (
            this.state.inputValue !== this.props.userProfile.ProfileDisplayName
          ) {
            const form = { ProfileDisplayName: this.state.inputValue };
            this.submitCall(form);
          }
        }
      );
    } else {
      this.setState({
        errorusername: `Username cannot contain special characters`,
      });
    }
  };
  changestxt1 = (id: any) => {
    // alert('hello')

    this.setState({
      // inputlocationValue:
      //   this.state.getPvtUser && this.state.getPvtUser.CountryNState
      //     ? this.state.getPvtUser.CountryNState
      //     : `CA ,USA`,
      changelocation: true,
    });
  };
  updateInputlocationValue = (evt: any) => {
    evt.preventDefault();
    this.setState({
      errorlocation: "",
      inputlocationValue: evt.target.value,
      // changeusername:false
    });
  };
  updatelocation = (evt: any) => {
    evt.preventDefault();

    if (
      this.state.inputlocationValue === undefined ||
      this.state.inputlocationValue === null ||
      this.state.inputlocationValue === ""
    ) {
      this.setState({
        errorlocation: `Please enter Location`,
      });
    } else if (
      this.state.inputlocationValue.length <= 2 ||
      this.state.inputlocationValue.length >= 36
    ) {
      this.setState({
        errorlocation: `location can have 3-35 characters`,
      });
    } else {
      // if (evt.key === "Enter") {
      //   this.setState({
      //     inputlocationValue: this.state.inputlocationValue,
      //     // changelocation: false,
      //   });
      //   const form = { CountryNState: this.state.inputlocationValue };
      //   this.submitCall1(form);
      // }
      this.setState(
        {
          errorlocation: "",
          inputlocationValue: this.state.inputlocationValue,
          changelocation: false,
        },
        () => {
          if (
            this.state.inputlocationValue !==
            this.props.userProfile.CountryNState
          ) {
            const form = { CountryNState: this.state.inputlocationValue };
            this.submitCall1(form);
          }
        }
      );
    }
  };

  submitCall = async (param: object): Promise<void> => {
    this.setState({ btnLoader: true });
    let res = await postMethod(userNameUpdate, param, true);
    console.log(res);
    if (
      res &&
      res.response &&
      (res.response.status === 404 || res.response.status === 401)
    ) {
      this.setState({
        isOffline: false,
        btnLoader: false,
        errorusername: errorMsg.invalid,
        changeusername: true,
      });
    } else if (res && res.response && res.response.status === 409) {
      this.setState({
        isOffline: false,
        btnLoader: false,
        errorusername: this.state.inputValue + "" + errorMsg.already,
        changeusername: true,
      });
    } else if (res && res.response && res.response.status === 500) {
      this.setState({
        isOffline: false,
        btnLoader: false,
        errorusername: errorMsg.server,
        changeusername: false,
      });
    } else if (res && res.data && res.data.statuscode === 200) {
      // inputValue:res.data.data[0].ProfileDisplayName
      // console.log(res.data.data[0].ProfileDisplayName)
      this.setState(
        {
          isOffline: false,
          changeusername: false,
          btnLoader: false,
          submitted: true,
        },
        () => {
          // console.log(this.state.inputValue)
          this.getUser();
        }
      );
    } else if (!res.status) {
      // console.log("no response");
      this.setState({
        btnLoader: false,
        isOffline: true,
        changeusername: false,
      });
    }
  };
  submitCall1 = async (param: object): Promise<void> => {
    this.setState({ btnLoader1: true });

    let res = await postMethod(userLocation, param, true);
    console.log(res);
    if (
      res &&
      res.response &&
      (res.response.status === 404 || res.response.status === 401)
    ) {
      this.setState({
        isOffline: false,
        btnLoader1: false,
        errorlocation: errorMsg.invalid,
        changelocation: true,
      });
    } else if (res && res.response && res.response.status === 500) {
      this.setState({
        isOffline: false,
        btnLoader1: false,
        errorlocation: errorMsg.server,
        changelocation: true,
      });
    } else if (res && res.data && res.data.statuscode === 200) {
      this.setState({ isOffline: false, changelocation: false });
      // this.setState({ btnLoader: false, submitted: true }, () => {
      //   setTimeout(() => {
      //     this.props.history.push("/user");
      //   }, 500);
      // });
      // console.log(res.data.data[0].CountryNState)
      // inputlocationValue:res.data.data[0].CountryNState
      this.setState(
        {
          isOffline: false,
          changelocation: false,
          btnLoader1: false,
          submitted: true,
        },
        () => {
          // console.log(this.state.inputlocationValue)
          this.getUser();
        }
      );
    } else if (!res.status) {
      // console.log("no response");
      this.setState({
        btnLoader1: false,
        isOffline: true,
        changelocation: false,
      });
    }
  };

  render() {
    const { SteamID } = this.props.userProfile;
    const {
      steamId,
      isShowModal,
      headerImagesList,
      profileImagesList,
      selectedHeaderImage,
      selectedProfileImage,
      profileImage,
      headerImage,
    } = this.state;
    return (
      <>
        <HeaderWB />
        {/* {!this.state.isUserLoggedIn ? (
          <NotLoggedIn />
        ) : this.state.noUserDetail ? (
          <>
            <div className="notlogged-in">
              <p>
                Error Encountered..Plz{" "}
                <Link to={{ pathname: "/login" }}>Login</Link> again
              </p>
            </div>
          </>
        ) : this.state.profileData && this.state.profileData ? ( */}
        {!this.state.isUserLoggedIn ? (
          <NotLoggedInWB />
        ) : (
          <>
            <div id="content" className='wavebreak'>
              <div
                className="component component-masthead userprofile "
                style={{
                  backgroundImage: `url(${
                    headerImage ? headerImage : defaultheader
                  })`,
                }}
              >
                <div className="component-inner"></div>
              </div>
              <div className="component component-user-profile">
                <div className="component-inner">
                  {/* <div className="profile-bio">
                    <div className='inlineblock' id='usernameedit'>
                      <h2 className="username">{this.state.profileData && this.state.profileData.UserName ? this.state.profileData.UserName : `GamerID`} </h2>
                      <span className="format-icon edit"></span>
                    </div>
                    <div className='inlineblock' id='locationedit'>
                      <p className="location">CA, USA</p>
                      <span className="format-icon edit"></span>
                    </div>

                  </div> */}
                  <div className="profile-bio">
                    <div className="inlineblock" id="usernameedit">
                      {!this.state.changeusername ? (
                        <h2 className="username" onClick={this.changestxt}>
                          {this.state.getPvtUser &&
                          this.state.getPvtUser.ProfileDisplayName
                            ? this.state.inputValue
                            : "GameID"}{" "}
                          <span className="format-icon edit"></span>
                        </h2>
                      ) : null}

                      {this.state.changeusername ? (
                        <div className="inputusername">
                          <input
                            type="text"
                            className="form-control"
                            name="text"
                            autoFocus
                            onChange={this.updateInputValue}
                            value={
                              this.state.inputValue && this.state.inputValue
                            }
                          />
                          {this.state.btnLoader ? (
                            <span className="loader-btn"></span>
                          ) : (
                            <span
                              className="format-icon check"
                              onClick={this.updateusername}
                            ></span>
                          )}
                        </div>
                      ) : null}
                      <div className="format-messages">
                        <ul className="msg-error">
                          {this.state.errorusername && (
                            <li>{this.state.errorusername}</li>
                          )}
                        </ul>
                      </div>
                    </div>
                    <div className="inlineblock" id="locationedit">
                      {!this.state.changelocation ? (
                        <p className="location" onClick={this.changestxt1}>
                          {this.state.getPvtUser &&
                          this.state.getPvtUser.ProfileDisplayName
                            ? this.state.inputlocationValue
                            : `CA ,USA`}{" "}
                          <span className="format-icon edit"></span>
                        </p>
                      ) : null}

                      {this.state.changelocation ? (
                        <div className="inputlocation">
                          <input
                            type="text"
                            className="form-control"
                            name="text"
                            onChange={this.updateInputlocationValue}
                            // onKeyPress={this.updateInputlocationValue}
                            value={
                              this.state.inputlocationValue &&
                              this.state.inputlocationValue
                            }
                          />
                          {this.state.btnLoader1 ? (
                            <span className="loader-btn"></span>
                          ) : (
                            <span
                              className="format-icon check"
                              onClick={this.updatelocation}
                            ></span>
                          )}
                        </div>
                      ) : null}
                      <div className="format-messages">
                        <ul className="msg-error">
                          {this.state.errorlocation && (
                            <li>{this.state.errorlocation}</li>
                          )}
                          {/* {this.state.errorFromApi && (
                          <li>{this.state.errorFromApi}</li>
                        )} */}
                        </ul>
                      </div>
                    </div>
                    {/* <div className='inlineblock' id='locationedit'>
                              <p className="location">CA, USA</p>
                              <span className="format-icon edit"></span>
                            </div> */}
                  </div>
                  <div className="profile-details">
                    <div
                      className="photo editable"
                      onClick={this.toggleImageModal}
                    >
                      {/* {profileImage && profileImage ? ( */}
                        <img
                          src={profileImage ? profileImage : defaultprofile}
                          // src={profileImage}
                          alt="avatar"
                        />
                      {/* // ) : null} */}

                      <span className="format-icon edit"></span>
                    </div>
                    {this.state.isUserLoggedIn ? (
                      <div className="platform-ids">
                        <h3>CONNECT YOUR GAMER IDs</h3>
                        <ul className="platforms">
                          {/* {showSteamSuccess ? (
                                  <li className="platform-item steam connected" role="alert">
                                    <div className="format-icon steam white"></div>
                                      <div className="name">STEAM</div>
                                  </li>
                                ) : null} */}
                          <li
                            className={
                              SteamID || steamId
                                ? "platform-item steam connected disabled"
                                : "platform-item steam"
                            }
                            onClick={this.handleSteamLogin}
                          >
                            <div className="format-icon steam white"></div>
                            <div className="name">STEAM</div>
                          </li>
                          {/* <li className="platform-item playstation">
                            <div className="format-icon playstation white"></div>
                            <div className="name">PLAYSTATION</div>
                          </li>
                          <li className="platform-item switch">
                            <div className="format-icon switch white"></div>
                            <div className="name">SWITCH</div>
                          </li>
                          <li className="platform-item xbox">
                            <div className="format-icon xbox white"></div>
                            <div className="name">XBOX</div>
                          </li>
                          <li className="platform-item discord">
                            <div className="format-icon discord white"></div>
                            <div className="name">DISCORD</div>
                          </li>
                          <li className="platform-item epic">
                            <div className="format-icon epic white"></div>
                            <div className="name">EPIC</div>
                          </li>
                          <li className="platform-item gog">
                            <div className="format-icon gog white"></div>
                            <div className="name">GOG</div>
                          </li> */}
                        </ul>
                      </div>
                    ) : null}
                  </div>
                  {isShowModal && (
                    <UpdateImageModal
                      close={this.toggleImageModal}
                      handleProfileClick={this.handleProfileImageClick}
                      handleHeaderClick={this.handleHeaderImageClick}
                      profileImagesList={profileImagesList && profileImagesList}
                      headerImagesList={headerImagesList && headerImagesList}
                      selectedProfileImage={
                        selectedProfileImage && selectedProfileImage
                      }
                      selectedHeaderImage={
                        selectedHeaderImage && selectedHeaderImage
                      }
                    />
                  )}
                  <div className="profile-rankings">
                    {/* {this.state.isUserLoggedIn ? (
                      <h2>Your Leaderboards</h2>
                    ) : (
                      <h2>
                        {this.state.profileData &&
                        this.state.profileData.UserName
                          ? this.state.profileData.UserName
                          : `GamerID's`}{" "}
                        Leaderboards
                      </h2>
                    )} */}
                    {/* <ProfileGameDetails details={this.state.profileData} /> */}
                  </div>
                </div>
              </div>
            </div>
          </>
        )}

        {/* ) : (
                <LoadingData isLoading={this.state.showLoading} />
              )} */}
        <FooterWB />
      </>
    );
  }
}

export default connect(mapStateToProps, {})(editProfileWB);
