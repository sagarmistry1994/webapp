import React from "react";
import Header from "./Header";
import Footer from "./Footer";
const NotFound = () => (
  <>
  <Header />
  <div className="register-wrapper">
    <div className="container-fluid register-container">
      <div className="col-sm-12 col-xs-12">
        <div className="register-section-wrapper component component-account-form">
          <div className='notfound' style={{marginTop:'1rem'}}>
          <h1>404</h1>
            <h3>Page Not Found</h3>
            <p>We are sorry but the page you are looking for does not exist.</p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <Footer />
  </>
);

export default NotFound;
