import React from "react";
import { capitaliseWord } from "../utils/commonUtils";
import { Link } from "react-router-dom";
// import { levelBasedLb, getMethod } from "../utils/apis";

const DetailCard = (props: any) => {
  const link = props.TournamentID && props.TournamentID;
  const {
    searchTerm,
    keys,
    // desc,
    title,
    players,
    // columnName,
    userId,
    subTitle,
    columns,
  } = props;
  return (
    <>
      <div
        className="leaderboard-item format-leaderboard  animation fade-in animation-queued"
        key={keys}
      >
        <div className="wrapingheader">
          <h3>{title && title}</h3>
          <p>{subTitle && subTitle}</p>
          <div className="selectwrap"></div>
        </div>
        <table className="leaderboard-rankings">
          <thead>
            <tr>
              {/* <th>#</th>
              <th>Player</th>
              <th>{desc && desc}</th> */}
              {columns.map((item: any, key: number) => {
                return <th key={key}>{item.description}</th>;
              })}
            </tr>
          </thead>
          <tbody>
            {players && players
              ? players.map((index: any, key: any) => {
                  return (
                    <tr
                      className={`${
                        (index.PlayerName &&
                          index.PlayerName.toLowerCase()
                            .trim()
                            .indexOf(
                              searchTerm && searchTerm.toLowerCase().trim()
                            ) !== -1) ||
                        userId === index.UserID
                          ? "highlighted"
                          : "false"
                      }`}
                      key={key}
                    >
                      {/* <td>{key + 1}</td>
                      {index.UserID === userId ? (
                        <td>
                          <Link to={{ pathname: `/user` }}>
                            {capitaliseWord(index.PlayerName)}
                          </Link>
                        </td>
                      ) : (
                        <td>
                          <Link to={{ pathname: `/user/${index.UserID}` }}>
                            {capitaliseWord(index.PlayerName)}
                          </Link>
                        </td>
                      )}
                      <td>{index[columnName]}</td> */}
                       {/* <td>{index.}</td> */}
                      {columns.map((item: any, key: number) => {
                        return (
                          <td key={key} data-id={index.UserID}>
                            {item.columnName === "PlayerName" ? (
                              index.UserID === userId ? (
                                <Link to={{ pathname: `/user` }}>
                                  {capitaliseWord(index.PlayerName)}
                                </Link>
                              ) : (
                                <Link
                                  to={{ pathname: `/user/${index.UserID}` }}
                                >
                                  {capitaliseWord(index.PlayerName)}
                                </Link>
                              )
                            ) : (
                              index[item.columnName]
                            )}
                          </td>
                        );
                      })}
                    </tr>
                  );
                })
              : null}
          </tbody>
        </table>
        <div className="cta">
          <Link
            className="format-button secondary"
            to={{ pathname: `/leagues/${link}/leaderboard/102` }}
          >
            See full leaderboard
          </Link>
        </div>
      </div>
    </>
  );
};

export default DetailCard;
