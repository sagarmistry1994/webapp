import React from "react";
import { connect } from "react-redux";
import { IUserProfileNewDetails } from "./../utils/interfacePools";
import { IAppState } from "../reducer";
import { RouteComponentProps, Link } from "react-router-dom";
import store from "../store";
import { userLogOut, getUserInfo, zIndex } from "../utils/actionDispatcher";

import axios from "axios";
import {
  removeFromLocalStorage,
  // isUserLogged,
  getLocalStorage,
  setLocalStorage,
  IsUserProfileLocalStorage,
  haveErrors,
} from "../utils/commonUtils";
import Constants from "../utils/Constants";
import NotLoggedIn from "./common/NotLoggedIn";
import { userUrl, putMethod, postMethod } from "../utils/apis";
import LoadingData from "./common/LoadingData";

import dp from "./../assets/gen-dp.jpg";

import ProfileDetails from "./ProfileDetails";
import ProfileGameDetails from "./ProfileGameDetails";
import UpdateImageModal from "./UploadImageModal";
// import ScrollToTop from "./common/ScrollToTop";

type IUserProfileProps = any;

interface LinkProps {
  userProfile: IUserProfileNewDetails;
}

export interface IErrors {
  [key: string]: string;
}

interface IUserProfProps extends RouteComponentProps<{ id: string }> {}

type Props = IUserProfileProps & LinkProps & IUserProfProps;

const mapStateToProps = (
  state: IAppState,
  ownProps: IUserProfileProps
): LinkProps => {
  return {
    userProfile: state.user,
  };
};

interface IUserState {
  isUserLoggedIn?: boolean;
  profileData?: any;
  showLoading?: boolean;
  showEditScreen?: boolean;
  errors: IErrors;
  [key: string]: any;
  errorFromApi?: string;
  btnLoader?: boolean;
  usrname?: string;
  submitted?: boolean;
  steamId?: string;
  noUserDetail?: boolean;
  showSteamSuccess?: boolean;
  isShowModal?: boolean;
}

class UserProfile extends React.Component<Props, IUserState> {
  constructor(props: Props) {
    super(props);
    const errors: IErrors = {};
    this.state = {
      errors,
      profileData: props.userProfile ? props.userProfile : {},
      UserName: "",
      ProfileDisplayName: props.userProfile
        ? props.userProfile.ProfileDisplayName
        : "",
      ProfilePictureUrl: "",
      FacebookProfile: "",
      TwitterProfile: "",
      InstagramProfile: "",
      SteamProfile: "",
      DiscordProfile: "",
      XboxProfile: "",
      PsProfile: "",
      StateShortCode: "",
      CountryName: "",
      About: "",
      Followers: "",
      InGameUserID: null,
      steamId: "",
      usrname: "afgsd",
      showSteamSuccess: false,
      isShowModal: false,
    };
  }

  componentDidMount() {
    const a = this.props.userProfile && this.props.userProfile.UserID;
    const b = IsUserProfileLocalStorage();
    const c = getLocalStorage(Constants.userToken);
    if ((a || b) && c) {
      // console.log("have id and token");
      this.setState({ isUserLoggedIn: true }, () => this.getUser());
    } else {
      this.setState({ isUserLoggedIn: false });
    }
    window.scrollTo(0, 0);
  }

  handleSteamLogin = () => {
    const popupWindow = window.open(
      process.env.REACT_APP_API_URL + "/auth/steam",
      "_blank",
      "width=800, height=600"
    );
    window.focus && popupWindow && popupWindow.focus();
    this.steamCallack();
  };




  private steamCallack = () => {
    window.addEventListener("message", (event) => {
      if (event.origin !== process.env.REACT_APP_API_URL) return;

      const { token, ok, steamid, personaname, realname } = event.data;
      if (ok) {
        console.log(token);
        console.log(steamid);
        console.log(personaname);
        console.log(realname)
        steamid && personaname && realname &&
          this.setState({ steamId: steamid, personaName: personaname, realName: realname }, () => {
            this.sendSteamData();
          });
      }
    });
  };

  private sendSteamData = async (): Promise<void> => {
    const { profileData, steamId, personaName, realName } = this.state;
    let data = {
      UserID: profileData.UserID.toString(),
      SteamID: steamId,
      personaName,
      realName
    };
    console.log(data);
    let res = await postMethod(userUrl, data, true);
    if (res && res.response && res.response.status === 404) {
      console.log(res.response);
    } else if (res && res.data && res.data.statuscode === 200) {
      console.log(res.data);
      this.setState({ showSteamSuccess: true }, () => {
        setTimeout(() => {
          this.setState({ showSteamSuccess: false });
        }, 1000);
      });
    } else {
      console.log(res);
    }
  };

  private handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { name, value },
    } = e;
    this.setState({ [name]: value });
  };

  handleSubmit = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
    e.preventDefault();
    const {
      profileData,
      UserName,
      ProfileDisplayName,
      ProfilePictureUrl,
      FacebookProfile,
      TwitterProfile,
      InstagramProfile,
      SteamProfile,
      DiscordProfile,
      XboxProfile,
      PsProfile,
      About,
      InGameUserID,
    } = this.state;
    this.setState(
      {
        errorFromApi: "",
        errors: {
          ...this.state.errors,
        },
      },
      () => {
        if (!haveErrors(this.state.errors)) {
          const form = {
            UserID: profileData.UserID.toString(),
            UserName,
            ProfileDisplayName,
            ProfilePictureUrl,
            FacebookProfile,
            TwitterProfile,
            InstagramProfile,
            SteamProfile,
            DiscordProfile,
            XboxProfile,
            PsProfile,
            About,
            InGameUserID,
          };
          // console.log("good to go");
          this.submitForm(form);
          // let res = await putMethod(userUrl,form,true)
          // console.log(form);
        } else {
          console.log("have errors");
        }
      }
    );
  };

  submitForm = async (form: any): Promise<void> => {
    this.setState({ btnLoader: true });
    let res = await putMethod(userUrl, form, true);
    if (res && res.response && res.response.status === 404) {
      // console.log("user not found");
      this.setState({ btnLoader: false });
    } else if (res && res.response && res.response.status === 500) {
      this.setState({ btnLoader: false });
    } else if (res && res.data && res.data.statuscode === 200) {
      // successfully updated
      this.setState({ btnLoader: false, submitted: true }, () => {
        this.getUserDetails(form.UserID);
        setTimeout(() => {
          this.setState({ submitted: false, showEditScreen: false });
        }, 1000);
      });
    }
  };

  getUser = async (): Promise<void> => {
    if (this.props.userProfile && this.props.userProfile.UserID) {
      const userId = this.props.userProfile.UserID;
      this.getUserDetails(userId);
    } else {
      let userId = IsUserProfileLocalStorage();
      userId && this.getUserDetails(userId);
    }
  };

  getUserDetails = async (id: number): Promise<void> => {
    const token = getLocalStorage(Constants.userToken);
    const head = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    this.setState({ showLoading: true });
    const url = `${userUrl}${id}`;
    let res = await axios
      .get(url, { headers: head })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        console.log(err);
        return err;
      });
    if (res && res.response && res.response.status === 404) {
      // console.log("data not found");
      // store.dispatch(userLogOut(''))
      this.setState({ showLoading: false, noUserDetail: true });
    } else if (res && res.response && res.response.status === 401) {
      // store.dispatch(userLogOut(''))
      this.setState({ showLoading: false, noUserDetail: true });
    } else if (res && res.response && res.response.status === 500) {
      // store.dispatch(userLogOut(''))
      this.setState({ showLoading: false, noUserDetail: true });
      // console.log("server error");
    } else if (res && res.data && res.data.statuscode === 200) {
      const response = res.data.data[0];
      setLocalStorage(Constants.userProfile, JSON.stringify(response));
      store.dispatch(getUserInfo(response));
      this.setState(
        {
          profileData: this.props.userProfile,
          UserName: this.props.userProfile.UserName,
          ProfileDisplayName: this.props.userProfile.ProfileDisplayName,
          ProfilePictureUrl: this.props.userProfile.ProfilePictureUrl,
          FacebookProfile: this.props.userProfile.FacebookProfile,
          TwitterProfile: this.props.userProfile.TwitterProfile,
          InstagramProfile: this.props.userProfile.InstagramProfile,
          SteamProfile: this.props.userProfile.SteamProfile,
          DiscordProfile: this.props.userProfile.DiscordProfile,
          XboxProfile: this.props.userProfile.XboxProfile,
          PsProfile: this.props.userProfile.PsProfile,
          About: this.props.userProfile.About,
          InGameUserID: this.props.userProfile.InGameUserID,
        },
        () => this.setState({ showLoading: false, noUserDetail: false })
      );
      // console.log(response);
    }
  };

  onLogout = () => {
    store.dispatch(userLogOut(""));
    removeFromLocalStorage(Constants.userToken);
    removeFromLocalStorage(Constants.userProfile);
    this.props.history.push("/");
  };

  showEditScreen = () => {
    this.setState({ showEditScreen: true });
  };

  closeEditScreen = () => {
    this.setState({ showEditScreen: false });
  };

  toggleImageModal = () => {
    store.dispatch(zIndex(""));
    this.setState({ isShowModal: !this.state.isShowModal });
  };

  handleProfileImageClick = (id: any) => {
    console.log(id);
    // this.setState({ selec });
  };

  handleHeaderImageClick = (id: any) => {
    console.log(id);
  };

  render() {
    const { isShowModal } = this.state;
    return (
      <>
        {!this.state.isUserLoggedIn ? (
          <NotLoggedIn />
        ) : this.state.noUserDetail ? (
          <>
            <div className="notlogged-in">
              <p>
                Error Encountered..Plz{" "}
                <Link to={{ pathname: "/login" }}>Login</Link> again
              </p>
            </div>
          </>
        ) : this.state.profileData && this.state.profileData ? (
          <>
            <div className="game-bg-wrapperprofile">
              <div className="component component-masthead userprofile ">
                <div className="component-inner"></div>
              </div>

              <div className="component component-user-profile">
                {/* {UserID && UserID ? (
                    <div className="logout-btn" onClick={this.onLogout}>
                      LOGOUT
                    </div>
                  ) : null} */}
                <div className="component-inner">
                  <>
                    <div className="game-bg-holder">
                      {/* <div className="user-dp-container">
              <div className="container">
                <div className="col-sm-12 col-xs-12">
                  <div className="user-dp">
                    <img src={dp} alt="avatar" className="dp" />
                  </div>
                </div>
              </div>
            </div> */}
                      {!this.state.showEditScreen ? (
                        <div className="profile-details">
                          <div className="photo">
                            <img src={dp} alt="avatar" />
                            {/* <Link to={{ pathname: "/editProfile" }}><span className="format-icon edit"></span></Link> */}
                            <span
                              className="format-icon edit"
                              onClick={this.toggleImageModal}
                            ></span>
                          </div>

                          <ProfileDetails details={this.state.profileData} />
                        </div>
                      ) : null}
                    </div>
                  </>
                  {/* <ScrollToTop /> */}
                  {this.state.submitted ? (
                    <div className="form-success">
                      <p className="loading">Updated successfully</p>
                    </div>
                  ) : null}
                  <div className="user-profile-wrapper">
                    {!this.state.showEditScreen ? (
                      <>
                        <ProfileGameDetails details={this.state.profileData} />
                      </>
                    ) : null}
                    {isShowModal && (
                      <UpdateImageModal
                        close={this.toggleImageModal}
                        handleProfileClick={this.handleProfileImageClick}
                        handleHeaderClick={this.handleHeaderImageClick}
                      />
                    )}
                  </div>
                </div>
              </div>
            </div>
          </>
        ) : (
          <LoadingData isLoading={this.state.showLoading} />
        )}
      </>
    );
  }
}

export default connect(mapStateToProps, {})(UserProfile);
