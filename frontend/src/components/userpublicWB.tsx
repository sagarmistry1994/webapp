import React from "react";
import { connect } from "react-redux";
import { IUserProfileNewDetails } from "./../utils/interfacePools";
import { IAppState } from "../reducer";
import { RouteComponentProps } from "react-router-dom";
// import store from "../store";
// import { userLogOut, getUserInfo, zIndex } from "../utils/actionDispatcher";
import axios from "axios";
import {
  // removeFromLocalStorage,
  // setLocalStorage,
} from "../utils/commonUtils";
// import Constants from "../utils/Constants";
import {
  userProfileget,
} from "../utils/apis";
import HeaderWB from "./HeaderWB";
import FooterWB from "./FooterWB";
// import ProfileGameDetails from "./ProfileGameDetails";

type IUserProfileProps = any;

interface LinkProps {
  userProfile: IUserProfileNewDetails;
}

export interface IErrors {
  [key: string]: string;
}

interface IUserProfProps extends RouteComponentProps<{ id: string }> {}

type Props = IUserProfileProps & LinkProps & IUserProfProps;

const mapStateToProps = (
  state: IAppState,
  ownProps: IUserProfileProps
): LinkProps => {
  return {
    userProfile: state.user,
  };
};

interface IUserState {
  isUserLoggedIn?: boolean;
  profileData?: any;
  showLoading?: boolean;
  showEditScreen?: boolean;
  errors: IErrors;
  [key: string]: any;
  errorFromApi?: string;
  btnLoader?: boolean;
  usrname?: string;
  submitted?: boolean;
  steamId?: string;
  noUserDetail?: boolean;
  showSteamSuccess?: boolean;
  isShowModal?: boolean;
  ProfileDisplayName?: string;
}
let defaultheader =
  "https://iel-webapp-profile-images.s3.amazonaws.com/images/profile/WP-Profile-bkg-1.jpg";
let defaultprofile =
  "https://iel-webapp-profile-images.s3.amazonaws.com/images/profile/WB-PROFILE-1.jpg";

class userPublicwb extends React.Component<Props, IUserState> {
  constructor(props: Props) {
    super(props);
    const errors: IErrors = {};
    this.state = {
      errors,
      // profileData: props.userProfile ? props.userProfile : {},
      // UserName: "",
      // ProfileDisplayName: props.userProfile
      //   ? props.userProfile.ProfileDisplayName
      //   : "",
      ProfilePictureUrl: "",
      FacebookProfile: "",
      TwitterProfile: "",
      InstagramProfile: "",
      SteamProfile: "",
      DiscordProfile: "",
      XboxProfile: "",
      PsProfile: "",
      StateShortCode: "",
      CountryName: "",
      About: "",
      Followers: "",
      InGameUserID: null,
      steamId: "",
      usrname: "afgsd",
      showSteamSuccess: false,
      isShowModal: false,
    };
  }

  componentDidMount() {
    this.getUserDetails()
    window.scrollTo(0, 0);
  }

  getUserDetails = async (id?: number): Promise<void> => {
    // const token = getLocalStorage(Constants.userToken);
    const head = {
      "Content-Type": "application/json",
      // Authorization: `Bearer ${token}`,
    };
    const currentUrl = this.props.match.params.id;
    this.setState({ showLoading: true });
    const url = `${userProfileget}/${currentUrl}`;
    let res = await axios
      .get(url, { headers: head })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        console.log(err);
        return err;
      });
    if (res && res.response && res.response.status === 404) {
      // console.log("data not found");
      // store.dispatch(userLogOut(''))
      this.setState({ showLoading: false, noUserDetail: true });
    } else if (res && res.response && res.response.status === 401) {
      // store.dispatch(userLogOut(''))
      this.setState({ showLoading: false, noUserDetail: true });
    } else if (res && res.response && res.response.status === 500) {
      // store.dispatch(userLogOut(''))
      this.setState({ showLoading: false, noUserDetail: true });
      // console.log("server error");
    } else if (res && res.data && res.data.statuscode === 200) {
      const response = res.data.data[0];
      // setLocalStorage(Constants.userProfile, JSON.stringify(response));
      // store.dispatch(getUserInfo(response));
      this.setState(
        {
          getPublicUser: response,
        },
        () => this.setState({ showLoading: false, noUserDetail: false })
      );
      // console.log(response);
    }
  };

  render() {
    return (
      <>
       <HeaderWB/>
        <>
          <div id="content">
            <div
              className="component component-masthead userprofile "
              style={{
                backgroundImage: `url(${
                  this.state.getPublicUser &&
                  this.state.getPublicUser.HeaderImageURL
                    ? this.state.getPublicUser.HeaderImageURL
                    : defaultheader
                })`,
              }}
            >
              <div className="component-inner"></div>
            </div>
            <div className="component component-user-profile">
              <div className="component-inner">
                {/* <div className="profile-bio">
                    <div className='inlineblock' id='usernameedit'>
                      <h2 className="username">{this.state.profileData && this.state.profileData.UserName ? this.state.profileData.UserName : `GamerID`} </h2>
                      <span className="format-icon edit"></span>
                    </div>
                    <div className='inlineblock' id='locationedit'>
                      <p className="location">CA, USA</p>
                      <span className="format-icon edit"></span>
                    </div>

                  </div> */}
                <div className="profile-bio">
                  <h2 className="username">
                    {this.state.getPublicUser &&
                    this.state.getPublicUser.ProfileDisplayName
                      ? this.state.getPublicUser.ProfileDisplayName
                      : `GamerID`}{" "}
                  </h2>
                  <p className="location">
                  {this.state.getPublicUser &&
                    this.state.getPublicUser.CountryNState
                      ? this.state.getPublicUser.CountryNState
                      : `CA, USA`}{" "}
                    </p>
                </div>
                <div className="profile-details">
                  <div className="photo editable">
                    <img
                      src={
                        this.state.getPublicUser &&
                        this.state.getPublicUser.ProfileImageURL
                          ? this.state.getPublicUser.ProfileImageURL
                          : defaultprofile
                      }
                      alt="avatar"
                    />
                  </div>
                </div>
                <div className="profile-rankings">
                  {/* <h2>
                    {this.state.getPublicUser &&
                    this.state.getPublicUser.ProfileDisplayName
                      ? this.state.getPublicUser.ProfileDisplayName
                      : `GamerID`}
                    {"'s "}
                    Leaderboards
                  </h2> */}
                  {/* <ProfileGameDetails details={this.state.getPublicUser} /> */}
                </div>
              </div>
            </div>
          </div>
        </>

        {/* ) : (
                <LoadingData isLoading={this.state.showLoading} />
              )} */}
        <FooterWB/>
      </>
    );
  }
}

export default connect(mapStateToProps, {})(userPublicwb);
