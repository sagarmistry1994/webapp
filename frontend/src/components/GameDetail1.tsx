import React from "react";
import DetailCardWB from "./CardOnDetailWB";
import CardOnDetailLevelsWB from "./CardOnDetailWithLevelsWB";
import { IAppState } from "./../reducer";
import { RouteComponentProps } from "react-router-dom";
import {
  IFeaturedTournament,
  IPlayers,
  IUserProfileNewDetails,
} from "../utils/interfacePools";
import { connect } from "react-redux";

import store from "../store";
import { allLeaderUrl, getMethod } from "../utils/apis";
import { getAllGames } from "../utils/actionDispatcher";
// import OfflineContent from "./common/OfflineContent";
import HeaderWB from "./HeaderWB";
import FooterWB from "./FooterWB";
import {
  IsUserProfileLocalStorage,
  getLocalStorage,
} from "../utils/commonUtils";
import Constants from "../utils/Constants";
interface IProps {
  afas?: string;
}

type IUserProfileProps = any;

interface LinkStateProps {
  allGames: IFeaturedTournament[];
  userDetails?: IUserProfileNewDetails;
}
interface IUserProfProps extends RouteComponentProps<{ id: string }> {}
export interface IErrors {
  [key: string]: string;
}

type Props = IProps & LinkStateProps & IUserProfProps & IUserProfileProps;

const mapStateToProps = (
  state: IAppState,
  ownProps: IProps
): LinkStateProps => {
  return {
    allGames: state.allGames,
    userDetails: state.user,
  };
};

interface IState {
  isUserLoggedIn?: boolean;
  profileData?: any;
  allGames: IFeaturedTournament[];
  sortText: string;
  bgImage: string;
  noData?: boolean;
  isOffline?: boolean;
  searchTerm?: string;
  sendTerm?: string;
  error?: string;
  count?: number;
  errors: IErrors;
  [key: string]: any;
  errorFromApi?: string;
}

class GameDetailWB extends React.Component<Props, IState> {
  constructor(props: Props) {
    super(props);
    const errors: IErrors = {};
    this.state = {
      errors,
      profileData: props.userProfile ? props.userProfile : {},
      UserName: "",
      ProfileDisplayName: props.userProfile
        ? props.userProfile.ProfileDisplayName
        : "",
      allGames: props.allGames ? props.allGames : [],

      sortText: "This Year",
      bgImage: "",
    };
  }

  componentDidMount() {
    this.getAllGamesMethod();
    const { userDetails } = this.props;
    const a = userDetails && userDetails.UserID;
    const b = IsUserProfileLocalStorage();
    const c = getLocalStorage(Constants.userToken);
    if ((a || b) && c) {
      this.setState({ userId: a || b });
    } else {
      // console.log('user not logged')
    }
    window.scrollTo(0, 0);
  }

  getAllGamesMethod = async (): Promise<void> => {
    const url =`${allLeaderUrl}/2`;
    // const header = commonHeader;
    let res = await getMethod(url);

    if (res && res.response && res.status === 404) {
      this.setState({ noData: true, isOffline: false });
    } else if (res && res.response && res.status === 500) {
      this.setState({ noData: true, isOffline: false });
    } else if (res && res.data && res.data.statuscode === 200) {
      const response = res.data.data;
      store.dispatch(getAllGames(response));
      this.setState({ allGames: response, noData: false, isOffline: false });
    } else if (res && res.data && res.data.statuscode === 500) {
      // console.log('server error')
    } else if (res && !res.status) {
      // console.log(res);
      this.setState({ isOffline: true });
    }
  };

  handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ searchTerm: e.target.value.trim() });
  };

  handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (this.state.searchTerm) {
      this.setState({
        sendTerm: this.state.searchTerm,
        error: "",
      });
      const { allGames, searchTerm } = this.state;
      let data = Object.entries(allGames).map((item) =>
        item[1][0].players.filter((index: IPlayers) =>
          index.PlayerName.toLowerCase()
            .trim()
            .includes(searchTerm && searchTerm.toLowerCase().trim())
        )
      );
      var count = data.filter((ind) => ind.length > 0);
      if (count && count.length) {
        this.setState({ count: count.length });
      } else {
        this.setState({ error: "Player not found" });
      }
    } else {
      this.setState({
        // error: "search is empty",
        error: "Please enter player name",
        sendTerm: this.state.searchTerm,
      });
    }
  };
  doNothing = () => {};
  render() {
    const { allGames, userId } = this.state;
    return (
      <>
      <HeaderWB />
        <div id="content" className='wavebreak'>
          <div className="component component-game-masthead">
            {/* <div className="game-bg-holder all-boards"> */}
            <div className="component-inner">
              <div className="intro">
                <h1>WAVE BREAK</h1>
                <div className="buy">
                  <div className="cta">
                    GET GAME <span></span>
                  </div>
                  {/* <label className="getgamelabel">GET GAME <span></span></label> */}
                  <ul className="providers">
                    <li>
                      <a
                        href="#"
                        target="_blank"
                        rel="noreferrer noopener"
                      >
                        STEAM
                      </a>
                    </li>
                    {/* <li>
                    <Link to={{ pathname: "" }}>Xbox</Link>
                    </li>
                    <li>
                    <Link to={{ pathname: "" }}>Playstation</Link>
                    </li>
                    <li>
                    <Link to={{ pathname: "" }}>EPIC GAME STORE</Link>
                    </li>
                    <li>
                    <Link to={{ pathname: "" }}>STADIA</Link>
                    </li>
                    <li>
                      <Link to={{ pathname: "" }}>GOG</Link>
                    </li> */}
                  </ul>
                </div>
              </div>
            </div>
            <div className="component-inner carousel-container">
              <div
                className="carousel"
                data-flickity='{ "cellAlign": "left", "contain": true, "freeScroll": false, "wrapAround": true, "imagesLoaded": true }'
              >
                <div className="carousel-item image">
                  <img
                    src={require("../media/WB-main-slider-header-1920x1080.jpg")}
                    alt="Game carousels 1"
                  />
                </div>
                {/* <div className="carousel-item video">
                  <img
                    src={require("../media/WB-main-slider-header-1920x1080.jpg")}
                    alt="Game carousels 2"
                  />
                  <div className="overlay video-play"></div>
                </div>

                <div className="carousel-item image">
                  <img
                    src={require("../media/WB-main-slider-header-1920x1080.jpg")}
                    alt="Game carousels 3"
                  />
                </div> */}
              </div>
            </div>
            <div className="component-inner game-meta">
              <div className="publisher">
                <h3 className="publisher-name">LIMIT BREAK STUDIOS</h3>
                <p className="publisher-type">ARCADE</p>
              </div>

              {/* <ul className="platforms">
                <li className="platform-item">
                  <div className="format-icon steam"></div>
                </li>
                <li className="platform-item">
                  <div className="format-icon xbox"></div>
                </li>
                <li className="platform-item">
                  <div className="format-icon windows"></div>
                </li>
              </ul> */}
            </div>
            {/* </div> */}
          </div>
          {/* <ScrollToTop /> */}
          <div className="component game-leader-bg component-leaderboards">
            <div className="component-inner">
              {/* {isOffline ? <OfflineContent /> : null} */}
              <h2 className="format-section-title">LEADERBOARDS</h2>
              {/* <div className="id-search-wrap"> */}
              {/* <div>
                <div className="form-head">What's your rank?</div>
                <form onSubmit={this.handleSubmit}>
                  <TextInput
                    onChange={this.handleChange}
                    error={this.state.error}
                    placeholder="Enter user ID"
                    value={this.state.searchTerm && this.state.searchTerm}
                  />
                  <button>Ok</button>
                </form>
              </div> */}
              {/* </div> */}
              {/* <div className="platform-selection">
                <ul className="tabs">
                  <li className="tab-item active">
                    <span>PC</span>
                  </li>
                  <li className="tab-item ">
                  <Link to={{ pathname: "" }}>Playstation</Link>
                  </li>
                  <li className="tab-item">
                    <Link to={{ pathname: "" }}>Xbox</Link>
                  </li>
                  <li className="tab-item">
                    <Link to={{ pathname: "" }}>Switch</Link>
                  </li>
                  <li className="tab-item">
                    <Link to={{ pathname: "" }}>iOS</Link>
                  </li>
                  <li className="tab-item">
                    <Link to={{ pathname: "" }}>Android</Link>
                  </li>
                </ul>
                <form className="dropdown">
                  <select name="platform">
                    <optgroup label="Platforms">
                      <option>PC</option>
                      <option>Playstation</option>
                      <option>Xbox</option>
                      <option>Switch</option>
                      <option>iOS</option>
                      <option>Android</option>
                    </optgroup>
                  </select>
                </form>
              </div> */}
              <div className="leaderboards">
                {/* {Object.entries(allGames).map((index: any, key: any) => {
                  return (
                    // <div
                    //   className="col-lg-4 col-md-6 col-sm-12 col-xs-12 leaderboard-item"
                    //   key={key}
                    // >
                    <DetailCardWB
                      // TournamentName={index[1][0].Name}
                      userstatus={this.state.isUserLoggedIn}
                      key={key}
                      players={index[1][0].players}
                      category={gameList[key]}
                      TournamentID={key + 1}
                      searchTerm={
                        this.state.sendTerm ? this.state.sendTerm : "null"
                      }
                    />
                    // </div>
                  );
                })} */}
                {allGames &&
                  allGames
                    .filter((index: any) => Object.keys(index).length !== 0)
                    .map((index: any, key: number) => {
                      return index.hasLevels ? (
                        <CardOnDetailLevelsWB
                          key={key}
                          name={index.Name}
                          // columnName={index.columnName}
                          // desc={index.description}
                          subTitle ={index.subtitle}
                          title={index.title}
                          TournamentID={index.tournamentID}
                          players={index.players}
                          userId={userId && userId}
                          hasLevels={index.hasLevels}
                          levels={index.levels}
                          columns={index.columns}
                        />
                      ) : (
                        <DetailCardWB
                          key={key}
                          name={index.Name}
                          // columnName={index.columnName}
                          // desc={index.description}
                          title={index.title}
                          subTitle ={index.subtitle}
                          TournamentID={index.tournamentID}
                          players={index.players}
                          userId={userId && userId}
                          hasLevels={index.hasLevels}
                          levels={index.levels}
                          columns={index.columns}
                        />
                      );
                    })}
              </div>
              <div className='staticdescription'>
                  <p>{'All scores are based on a single level except Maximum Money'}</p>
              </div>
              
              {/* <div className="format-pagination">
                <form>
                  <ul className="pages-links">
                    <li className="page-link">
                      <Link to={{ pathname: "" }}>«</Link>
                    </li>
                    <li className="page-link">
                      <Link to={{ pathname: "" }}>‹</Link>
                    </li>
                    <li className="current-page">
                      <input type="text" name="page" value="1" onChange={this.doNothing} disabled={true}/>{" "}
                      <span>of 1</span>
                    </li>
                    <li className="page-link">
                      <Link to={{ pathname: "" }}>›</Link>
                    </li>
                    <li className="page-link">
                      <Link to={{ pathname: "" }}>»</Link>
                    </li>
                  </ul>
                </form>
              </div> */}
            </div>
          </div>
        </div>
        <FooterWB />
      </>
    );
  }
}

export default connect(mapStateToProps, {})(GameDetailWB);
