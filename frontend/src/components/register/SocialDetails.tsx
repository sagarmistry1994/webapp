import React from "react";

import Constants from "../../utils/Constants";

const UserFields = Constants.UserFields;

const SocialDetails = (props: any) => {
  return (
    <>
      <div className="form-group">
        <label className="control-label">Facebook Url</label>
        <div className="fields-holder">
          <input
            className="form-control"
            type="text"
            name="facebookurl"
            autoComplete="off"
            onChange={e => props.handleFb(e, UserFields.userFb)}
          />
          {/* <p>
            {userNameReqError ? (
              <span className="reg-error">Please enter username</span>
            ) : null}
            {userNameExistsError ? (
              <span className="reg-error">
                Username already exists, try other
              </span>
            ) : null}
          </p> */}
        </div>
      </div>
      <div className="form-group">
        <label className="control-label">Twitter Url</label>
        <div className="fields-holder">
          <input
            className="form-control"
            type="text"
            name="twitterurl"
            autoComplete="off"
            onChange={e => props.handleTwitter(e, UserFields.userTwitter)}
          />
        </div>
      </div>
      <div className="form-group">
        <label className="control-label">Google+ Url</label>
        <div className="fields-holder">
          <input
            className="form-control"
            type="text"
            name="googleplusurl"
            autoComplete="off"
            onChange={e => props.handleGooglePlus(e, UserFields.userGooglePlus)}
          />
        </div>
      </div>
      <div className="form-group">
        <label className="control-label">Website Url</label>
        <div className="fields-holder">
          <input
            className="form-control"
            type="text"
            name="websiteurl"
            autoComplete="off"
            onChange={e => props.handleWebsite(e, UserFields.userWebsite)}
          />
        </div>
      </div>
    </>
  );
};

export default SocialDetails;
