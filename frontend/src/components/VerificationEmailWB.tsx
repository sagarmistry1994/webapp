import React from "react";
import FormTemplate from "./FormTemplate";
import TextInput from "./formFields/TextInput";
import { verifyEmailUrl, commonHeader, verifyEmailOtpUrl } from "../utils/apis";
import axios from "axios";
import { IVerificationEmail } from "../utils/interfacePools";
import { ErrorMsg } from "../utils/Constants";

type IVerifyEmailProps = any;

class VerificationEmailWB extends React.Component<
  IVerifyEmailProps,
  IVerificationEmail
> {
  constructor(props: any) {
    super(props);
    this.state = {
      userName: "",
      otp: null,
      isScreen1: true,
      otpErrorMsg: "",
      userNameErroMsg: "",
      showSuccessMsg: false,
    };
  }

  handleUserFields = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { name, value },
    } = e;
    this.setState({ [name]: value });
  };

  handleUser = (e: React.ChangeEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (this.state.userName) {
      //call api with username redirect to OTP if successfull & display username error msg when not
      this.submitUserName();
    } else {
      this.setState({
        userNameErroMsg: ErrorMsg.userNameEmpty,
        isScreen1: true,
      });
    }
  };

  async submitUserName() {
    //flag 1 for API - recognizing the username comes from verification email form
    const data = { userName: this.state.userName.toLowerCase(), flag: 1 };
    console.log(data);
    let response = await axios
      .post(verifyEmailUrl, data, { headers: commonHeader })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        console.log("error");
        return err;
      });
    console.log(response);
    if (response.response && response.response.status === 404) {
      this.setState({ userNameErroMsg: ErrorMsg.userNameInvalid });
    } else if (response.response && response.response.status === 409) {
      this.setState({ userNameErroMsg: ErrorMsg.userNameVerified });
    } else if (
      response.data &&
      (response.data.success === 1 || response.data.statuscode === 200)
    ) {
      this.setState({ isScreen1: false });
    }
  }

  handleOtp = (e: React.ChangeEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (this.state.otp) {
      //call api with otp and username, redirect to LOGIN if successful & display OTP error msg when not
      this.submitUserOtp();
    } else {
      // console.log("remain here");
      this.setState({ otpErrorMsg: ErrorMsg.otpEmpty });
    }
  };

  async submitUserOtp() {
    const data = {
      userName: this.state.userName.toLowerCase(),
      otp: this.state.otp,
    };
    let response = await axios
      .post(verifyEmailOtpUrl, JSON.stringify(data), { headers: commonHeader })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        console.log("error");
        return err;
      });
    if (response.response && response.response.status === 403) {
      this.setState({ otpErrorMsg: ErrorMsg.otpInvalid });
    } else if (
      response.data &&
      (response.data.success === 1 || response.data.statuscode === 200)
    ) {
      this.setState({ showSuccessMsg: true });
      setTimeout(() => this.props.history.push("/login"), 1000);
    }
  }

  render() {
    const {
      isScreen1,
      otpErrorMsg,
      userNameErroMsg,
      showSuccessMsg,
    } = this.state;
    return (
      <>
        {showSuccessMsg ? (
          <div className="registration-success">
            <span className="loading">Email verification successful</span>
          </div>
        ) : null}
        <div>
          <FormTemplate
            customStyle={true}
            title={isScreen1 ? "Resend Email" : "One Time Password (OTP)"}
            subTitle1={"Back to Login"}
            subLink1={"/login"}
            submitBtn1={isScreen1 ? "Resend verification email" : "Submit OTP"}
            onSubmit={isScreen1 ? this.handleUser : this.handleOtp}
          >
            {isScreen1 ? (
              <div className="form-group">
                <label className="control-label">
                  Username or Email<span className="required-red">*</span>
                </label>
                <div className="fields-holder">
                  <TextInput
                    name={"userName"}
                    type={"text"}
                    required={"true"}
                    autoComplete={"off"}
                    onChange={this.handleUserFields}
                  />

                  <p className="reg-error-parent">
                    <span className="reg-error">{userNameErroMsg}</span>
                  </p>
                </div>
              </div>
            ) : (
              <>
                <p>
                  We have emailed you a One Time Password(OTP), please use the
                  same OTP to verify your account
                </p>
                <div className="form-group">
                  <label className="control-label">
                    Enter OTP<span className="required-red">*</span>
                  </label>
                  <div className="fields-holder">
                    <TextInput
                      name={"otp"}
                      type={"number"}
                      required={"true"}
                      autoComplete={"off"}
                      onChange={this.handleUserFields}
                    />
                    <p className="reg-error-parent">
                      <span className="reg-error">{otpErrorMsg}</span>
                    </p>
                  </div>
                </div>
              </>
            )}
          </FormTemplate>
        </div>
      </>
    );
  }
}

export default VerificationEmailWB;
