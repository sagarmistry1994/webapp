import React from "react";
import { Link } from "react-router-dom";

const FormTemplate = (props: any) => {
  return (
    <>
      <div
        className={`register-wrapper ${
          props.customStyle ? "login-margin" : null
        }`}
      >
        <div className="container-fluid register-container">
          <div className="col-sm-12 col-xs-12">
            <div className="component component-page-title">
              <div className="component-inner">
                <>
                  <h1>{props.title}</h1>
                  <span className="sub-section-head">
                    <span className="form-sub-link">
                      <Link to={{ pathname: props.subLink1 }}>
                        {props.subTitle1}
                      </Link>
                    </span>
                    {props.subTitle2 ? (
                      <span className="form-sub-link">
                        <Link to={{ pathname: props.subLink2 }}>
                          {props.subTitle2}
                        </Link>
                      </span>
                    ) : null}
                  </span>
                </>
              </div>
            </div>
            <div className="register-section-wrapper">
              <form className="form-horizontal">
                {props.children}
                <div className="form-group">
                  <button
                    className="register-btn full-width"
                    onClick={(e) => props.onSubmit(e)}
                  >
                    {props.submitBtn1}
                  </button>
                  {props.submitBtn2 ? (
                    <Link
                      className="register-btn full-width"
                      to={{ pathname: props.trivialUrl }}
                    >
                      {props.submitBtn2}
                    </Link>
                  ) : null}
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default FormTemplate;
