import React from "react";
// import { IRegisterState } from "../utils/interfacePools";
import Constants, { errorMsg } from "./../utils/Constants";
import { userRegistrationUrl, postMethod } from "../utils/apis";
import TextInput from "../components/Form/TextInput";
import {
  handleEmail,
  handlePassword,
  handleConfirmPassword,
} from "../components/Form/FormValdiations";
import { removeFromLocalStorage } from "../utils/commonUtils";
import store from "../store";
import { userLogOut } from "../utils/actionDispatcher";

type IContactProps = any;

// const UserFields = Constants.UserFields;

export interface IErrors {
  [key: string]: string;
}
export interface IContactState {
  errors: IErrors;
  submitted?: boolean;
  email: string;
  password: string;
  confirmpassword: string;
  errorFromApi?: string;
  [key: string]: any;
  btnLoader?: boolean;
}

class Contact extends React.Component<IContactProps, IContactState> {
  constructor(props: IContactProps) {
    super(props);
    const errors: IErrors = {};
    this.state = {
      errors,
      email: "",
      password: "",
      confirmpassword: "",
      errorFromApi: "",
    };
  }

  componentDidMount() {
    store.dispatch(userLogOut(""));
    removeFromLocalStorage(Constants.userToken);
    removeFromLocalStorage(Constants.userProfile);
  }

  private handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { name, value },
    } = e;
    this.setState({ [name]: value });
  };

  private haveErrors(errors: IErrors) {
    let haveError: boolean = false;
    Object.keys(errors).forEach((key: string) => {
      if (errors[key].length > 0) {
        haveError = true;
      }
    });
    return haveError;
  }

  handleSubmit = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
    e.preventDefault();
    const { email, password, confirmpassword } = this.state;

    this.setState(
      {
        errorFromApi: "",
        errors: {
          ...this.state.errors,
          email: handleEmail(email),
          password: handlePassword(password),
          confirmpassword: handleConfirmPassword(confirmpassword, password),
        },
      },
      () => {
        if (!this.haveErrors(this.state.errors)) {
          const form = { emailId: email.toLowerCase(), password };
          this.submitCall(form);
        } else {
          console.log("have errors");
        }
      }
    );
  };

  submitCall = async (param: object) => {
    this.setState({ btnLoader: true });
    let res = await postMethod(userRegistrationUrl, param);

    if (res && res.response && res.response.status === 404) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.forbidden });
    } else if (res && res.response && res.response.status === 409) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.registered });
    } else if (res && res.response && res.response.status === 500) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.server });
    } else if (res && res.data && res.data.statuscode === 200) {
      this.postContact();
    }
  };

  postContact = () => {
    this.setState({ btnLoader: false, submitted: true }, () => {
      setTimeout(() => {
        this.props.history.push("/");
      }, 1000);
    });
  };

  render() {
    return (
      <>
        <div className="register-wrapper">
          <div className="container-fluid register-container">
            <div className="col-sm-12 col-xs-12">
              <div className="register-section-wrapper">
                <form className="form-horizontal" onSubmit={this.handleSubmit}>
                  <div className="component component-page-title">
                    <div className="component-inner">
                      <h1>CONTACT</h1>
                    </div>
                  </div>
                  <div className="format-messages">
                    <ul className="msg-error">
                      {/* {
                         this.state.errors["yourname"] && <li>{ this.state.errors["yourname"]}</li>
                       } 
                       {
                         this.state.errors["password"] && <li>{this.state.errors["password"]}</li>
                       } */}
                      {this.state.errorFromApi && (
                        <li>{this.state.errorFromApi}</li>
                      )}
                    </ul>
                  </div>
                  <TextInput
                    placeholder="First Last"
                    type="text"
                    label="Your Name"
                    onChange={this.handleChange}
                    name={"yourname"}
                    autoComplete="off"
                    required={"false"}
                  />
                  <TextInput
                    placeholder="Your username"
                    type="text"
                    label="Username"
                    onChange={this.handleChange}
                    name={"yourusername"}
                    autoComplete="off"
                    required={"false"}
                  />

                  <TextInput
                    placeholder="user@example.com"
                    type="email"
                    label="Email"
                    onChange={this.handleChange}
                    name={"youremail"}
                    autoComplete="off"
                    required={"false"}
                  />
                  <TextInput
                    placeholder="Your phone number"
                    type="text"
                    label="Password"
                    onChange={this.handleChange}
                    name={"yourpassword"}
                    autoComplete="off"
                    required={"false"}
                  />
                  <select name="subject">
                    <option>WHAT WOULD YOU LIKE TO TALK ABOUT?</option>

                    <option>Business Enquiry</option>
                    <option>Report A Bug</option>
                    <option>Other</option>
                  </select>
                  <textarea name="message"></textarea>
                  <div className="form-group">
                    <button className="register-btn full-width">
                      {" "}
                      {this.state.btnLoader ? (
                        <span className="loader-btn"></span>
                      ) : (
                        `SEND`
                      )}
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Contact;
