import React, { Component } from "react";
import TextInput from "./Form/TextInput";
// import logo from "./../assets/logo-base.svg";
import { Link } from "react-router-dom";
import { handleEmail, isRequired } from "../components/Form/FormValdiations";
import { userLoginUrl, postMethod } from "../utils/apis";
import { getUserInfo } from "../utils/actionDispatcher";
import store from "../store";
import {
  setLocalStorage,
  getLocalStorage,
  removeFromLocalStorage,
} from "../utils/commonUtils";
import Constants, { errorMsg } from "../utils/Constants";
import { userLogOut } from "../utils/actionDispatcher";
// import OfflineContent from "./common/OfflineContent";
import Header from "./Header";
import Footer from "./Footer";

export interface IErrors {
  [key: string]: string;
}
export interface IRegisterState {
  errors: IErrors;
  submitted?: boolean;
  email: string;
  password: string;
  isRemember: boolean;
  btnLoader?: boolean;
  [key: string]: any;
  isOffline?: boolean;
}

class newLogin extends Component<any, IRegisterState> {
  constructor(props: any) {
    super(props);

    const errors: IErrors = {};
    this.state = {
      errors,
      email: "",
      password: "",
      isRemember: false,
    };
  }

  componentDidMount() {
    const user = getLocalStorage(Constants.rememberMe);
    const isUser = JSON.parse(JSON.stringify(user));
    const oldUser = JSON.parse(isUser);
    if (oldUser) {
      this.setState({
        email: oldUser.user,
        password: oldUser.password,
        isRemember: true,
      });
    }
    store.dispatch(userLogOut(""));
    removeFromLocalStorage(Constants.userToken);
    removeFromLocalStorage(Constants.userProfile);
  }

  private handleCheckBox = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      isRemember: e.target.checked,
    });
  };

  private handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { name, value },
    } = e;
    this.setState({ [name]: value });
  };

  private haveErrors(errors: IErrors) {
    let haveError: boolean = false;
    Object.keys(errors).forEach((key: string) => {
      if (errors[key].length > 0) {
        haveError = true;
      }
    });
    return haveError;
  }

  handleSubmit = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
    e.preventDefault();
    const { email, password } = this.state;

    this.setState(
      {
        errorFromApi: "",
        errors: {
          ...this.state.errors,
          email: handleEmail(email),
          password: isRequired(password, "password"),
        },
      },
      () => {
        if (!this.haveErrors(this.state.errors)) {
          const form = { emailId: email.toLowerCase(), password };
          this.submitCall(form);
        } else {
          console.log("have errors");
        }
      }
    );
  };

  submitCall = async (param: object): Promise<void> => {
    this.setState({ btnLoader: true });
    let res = await postMethod(userLoginUrl, param);
    if (
      res &&
      res.response &&
      (res.response.status === 404 || res.response.status === 401)
    ) {
      this.setState({
        isOffline: false,
        btnLoader: false,
        errorFromApi: errorMsg.invalid,
      });
    } else if (res && res.response && res.response.status === 500) {
      this.setState({
        isOffline: false,
        btnLoader: false,
        errorFromApi: errorMsg.server,
      });
    } else if (res && res.data && res.data.statuscode === 200) {
      this.setState({ isOffline: false });
      const token = res.data.token;
      const adminData = res.data.data[0];
      setLocalStorage(Constants.userToken, token);
      store.dispatch(getUserInfo(adminData));
      if (this.state.isRemember) {
        setLocalStorage(
          Constants.rememberMe,
          JSON.stringify({
            user: this.state.email,
            password: this.state.password,
          })
        );
      } else {
        const user = getLocalStorage(Constants.rememberMe);
        if (user) {
          removeFromLocalStorage(Constants.rememberMe);
        }
      }
      this.postLogin();
    } else if (!res.status) {
      // console.log("no response");
      this.setState({ btnLoader: false, isOffline: true });
    }
  };

  postLogin = () => {
    this.setState({ btnLoader: false, submitted: true }, () => {
      setTimeout(() => {
        this.props.history.push("/user");
      }, 500);
    });
  };

  render() {
    // const { isOffline } = this.state;
    return (
      <>
       <Header/>
      <div className="register-wrapper">
        <div className="container-fluid register-container">
          <div className="col-sm-12 col-xs-12">
            <div className="register-section-wrapper component component-account-form">
              <form className="form-horizontal" onSubmit={this.handleSubmit}>
                {/* {isOffline ? <OfflineContent /> : null} */}
                <div className="component component-page-title">
                  <div className="component-inner">
                    <h1>LOGIN</h1>
                  </div>
                </div>
                <div className="format-messages">
                  {/* {
                    this.state.errors["email"] || this.state.errors["password"] ? */}
                  <ul className="msg-error">
                    {this.state.errors["email"] && (
                      <li>{this.state.errors["email"]}</li>
                    )}
                    {this.state.errors["password"] && (
                      <li>{this.state.errors["password"]}</li>
                    )}
                    {this.state.errorFromApi && (
                      <li>{this.state.errorFromApi}</li>
                    )}
                  </ul>
                </div>

                {/* {this.state.submitted ? (
                  <div className="form-success">
                    <span className="loading"> Login successful </span>
                  </div>
                ) : null} */}
                <TextInput
                  placeholder="user@example.com"
                  type="email"
                  label={"Email"}
                  onChange={this.handleChange}
                  name="email"
                  // error={this.state.errors["email"]}
                  autoComplete="off"
                  value={this.state.email && this.state.email}
                  autofocus={true}
                />
                <TextInput
                  placeholder="your password"
                  type="password"
                  label="Password"
                  onChange={this.handleChange}
                  // error={this.state.errors["password"]}
                  name="password"
                  autoComplete="off"
                  autofocus={false}
                  value={this.state.password && this.state.password}
                />
                {/* <div className="check-wrap">
                  <label className="custom-checkbox">
                    Remember password
                    <input
                      type="checkbox"
                      onChange={(e) => this.handleCheckBox(e)}
                      checked={this.state.isRemember && this.state.isRemember}
                    />
                    <span className="checkmark"></span>
                  </label>
                </div> */}
                <div className="options fieldset">
                  <Link
                    className="align-right"
                    to={{ pathname: "/forgot-password" }}
                  >
                    Forgot Password
                  </Link>
                  <div>
                    <label>
                    REMEMBER ME{" "}
                      <input
                        id="field-remember"
                        type="checkbox"
                        name="remember"
                        onChange={(e) => this.handleCheckBox(e)}
                        checked={this.state.isRemember && this.state.isRemember}
                      />
                    </label>
                  </div>
                </div>
                {/* <div
                  className="reg-error-parent"
                  style={{ marginBottom: "10px" }}
                >
                  <p className="reg-error">{this.state.errorFromApi}</p>
                </div> */}
                <div className="form-group">
                  <button className="register-btn full-width">
                    {" "}
                    {this.state.btnLoader ? (
                      <span className="loader-btn"></span>
                    ) : (
                      `LOGIN TO YOUR ACCOUNT`
                    )}
                  </button>
                </div>
                {/* <div className="body-text extralink">
                Don't have an account?
                <Link className="link bold" to={{ pathname: "/register" }}>
                Register
                </Link>
              </div> */}
                {/* <div className="actions">

                  <p className="help">OR LOGIN WITH YOUR SOCIAL ACCOUNT</p>

                  <ul className="sso">
                    <li className="sso-item steam">
                      <div className="format-icon steam"></div>
                      <div className="label">STEAM LOGIN</div>
                    </li>
                    <li className="sso-item google">
                      <div className="format-icon google"></div>
                      <div className="label">GOOGLE LOGIN</div>
                    </li>
                    <li className="sso-item discord">
                      <div className="format-icon discord"></div>
                      <div className="label">DISCORD LOGIN</div>
                    </li>
                  </ul>
                </div> */}
              </form>
            </div>
          </div>
        </div>
      </div>
      <Footer/>
    </>
    );
  }
}

export default newLogin;
