import React from "react";
interface ITextInputProps {
  name?: string;
  type?: string;
  required?: string;
  onChange?: any;
  onBlur?: any;
  autoComplete?: string;
  placeholder?: string;
  label?: string;
  error?: string;
  value?: string;
  customClass?: string;
  autofocus?:boolean;
}
const TextInput = (props: ITextInputProps) => {
  const {
    name,
    type,
    onChange,
    autoComplete,
    required,
    onBlur,
    placeholder,
    label,
    error,
    value,
    customClass,
    autofocus
  } = props;
  const onBlurDefault = (e: React.ChangeEvent<HTMLInputElement>) => {};
  const onChangeDefault = (e: React.FormEvent<HTMLInputElement>) => {};
  return (
    <>
      {/* <div className="form-group"> */}
       
      <div className="sbmfield">
        
          <input
            id={`field-${label && label}`}
            className={`form-control ${customClass && customClass}`}
            type={type}
            required={required === "true" ? true : false}
            placeholder={placeholder}
            name={name}
            autoComplete={autoComplete}
            onChange={onChange ? (e) => onChange(e) : onChangeDefault}
            onBlur={onBlur ? (e) => onBlur(e) : onBlurDefault}
            value={value}
            autoFocus={autofocus}
          />
        {label && label && <label className='format-sliding-label' data-for={`field-${label && label}`}>{label}</label>}
        </div>
        <div className="reg-error-parent" style={{display:'none'}}>
          <p className="reg-error">{error}</p>
        </div>
      {/* </div> */}
    </>
  );
};

export default TextInput;
