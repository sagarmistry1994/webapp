// import { IValues } from "./Form";
import { dateValidation, validatePhoneNumber, capitaliseWord } from "../../utils/commonUtils";
const regex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

// const passwordRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
const passwordRegex = /^(?!.* )(?=.*\d)(?=.*[A-Z]).{6,20}$/;

export const handleEmail = (value: string): string =>
  value === undefined || value === null || value === ""
    ? `Please enter email`
    : value.search(regex)
    ? `Invalid email`
    : "";
export const validateOtp = (value: string): string =>
  value === undefined || value === null || value === ""
    ? "Please enter OTP"
    : value.length < 4 || value.length > 4
    ? "OTP should be 4 digits"
    : "";
export const handlePassword = (value: string): string =>
  value === undefined || value === null || value === ""
    ? `Please enter password`
    : // : value && (value.length < 6 || value.length > 15)
    value.search(passwordRegex)
    ? `Must contain at least 1 digit, 1 uppercase letter & range from 6 - 20 characters without spaces`
    : "";

export const handleConfirmPassword = (
  value: string,
  password: string
): string => (value !== password ? `passwords don't match` : "");

export const isRequired = (value: string, field?: string): string =>
  value === undefined || value === null || value === ""
    ? `Please enter ${field && field ? field : `field`}`
    : "";

export const handleRequired = (value: string, field?: string): string =>
  value === undefined || value === null || value === ""
    ? `Please enter ${field && field ? field : `value`}`
    : "";

export const handleName = (value: string, field?: string): string =>
  value === undefined || value === null || value === ""
    ? `Please enter ${field && field ? field : `value`}`
    : value.length <= 2 || value.length >= 51
    ? `${field} should contain 3 - 50 characters`
    : "";

export const handleUserName = (value: string, field?: string): string =>
  value === undefined || value === null || value === ""
    ? `Please enter ${field && field ? field : `value`}`
    : value.length <= 2 || value.length >= 21
    ? `${field && capitaliseWord(field)} should be 3 - 20 characters in length`
    : value.match("^[a-zA-Z0-9 ]*$") != null
    ? ""
    : `${field && capitaliseWord(field)} cannot contain special characters`;

export const dob = (value: string): string =>
  value && value
    ? dateValidation(value) === true
      ? "Future date cannot be used"
      : ""
    : "Please enter Date of birth";

export const handlePhoneNumber = (value: string): string =>
  value && value
    ? validatePhoneNumber(value) === true
      ? ""
      : "Must contain 10 digits"
    : "Please enter phone number";
