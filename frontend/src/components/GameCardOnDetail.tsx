import React from "react";
import { IFeaturedTournament } from "../utils/interfacePools";
import cup from "./../assets/cup.png";
import gameImage from "./../assets/game-detail.svg";
import { Link } from "react-router-dom";

const GameCard = (props: IFeaturedTournament) => {
  const link = props.TournamentID && props.TournamentID;
  return (
    <>
      <div className="game-card-wrapper wrapper-on-detail">
        <div className="game-image">
          <img
            src={gameImage}
            className=""
            alt="game"
            style={{ opacity: "0.2" }}
          />
        </div>
        <div className="game-title player-column-on-detail">
          <span>NAME</span>
          <span>SCORE</span>
          <div className="game-img-data">
            <p className="txt-child">{props.TournamentName}</p>
            <p className="img-child">
              <img src={cup} alt="" className="" />
            </p>
          </div>
        </div>
        <div className="game-player-table">
          {props.players && props.players
            ? props.players.map((index, key) => {
                return (
                  <div className="player-column" key={key}>
                    <div className="player-name">{index.PlayerName}</div>
                    <div className="player-points"> {index.Score}</div>
                  </div>
                );
              })
            : null}
        </div>
        <div className="game-card-footer">
          <Link
            className="game-see-more"
            to={{
              pathname: `/game/${props.TournamentID}/leaderboard/1`,
              state: { link },
            }}
          >
            See more
          </Link>
        </div>
      </div>
    </>
  );
};

export default GameCard;
