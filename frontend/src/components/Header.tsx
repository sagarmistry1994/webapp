import React from "react";
import { IUserProfileNewDetails, IMiscPool } from "./../utils/interfacePools";
import { RouteComponentProps, Link } from "react-router-dom";
import store from "../store";
import { userLogOut } from "../utils/actionDispatcher";
import {
  removeFromLocalStorage,
  // isUserLogged,
  getLocalStorage,
  IsUserProfileLocalStorage,
} from "../utils/commonUtils";
import Constants from "../utils/Constants";
import logo from "../images/global-header-logo.png";
import { IAppState } from "../reducer";
import { connect } from "react-redux";

type IUserProfileProps = any;

interface LinkProps {
  userProfile: IUserProfileNewDetails;
  misc?: IMiscPool;
}

export interface IErrors {
  [key: string]: string;
}

const mapStateToProps = (
  state: IAppState,
  ownProps: IUserProfileProps
): LinkProps => {
  return {
    userProfile: state.user,
    misc: state.misc,
  };
};

interface IUserProfProps extends RouteComponentProps<{ id: string }> {}

type Props = IUserProfileProps & LinkProps & IUserProfProps;

interface IUserState {
  isUserLoggedIn?: boolean;
  profileData?: any;
  showLoading?: boolean;
  showEditScreen?: boolean;
  errors: IErrors;
  [key: string]: any;
  errorFromApi?: string;
  btnLoader?: boolean;
  usrname?: string;
  submitted?: boolean;
  steamId?: string;
  noUserDetail?: boolean;
  showSteamSuccess?: boolean;
}
let intervalID = 0;
class Header extends React.Component<Props, IUserState> {
  constructor(props: Props) {
    super(props);
    const errors: IErrors = {};
    this.state = {
      errors,
      profileData: props.userProfile ? props.userProfile : {},
      UserName: "",
      ProfileDisplayName: props.userProfile
        ? props.userProfile.ProfileDisplayName
        : "",
      ProfilePictureUrl: "",
      FacebookProfile: "",
      TwitterProfile: "",
      InstagramProfile: "",
      SteamProfile: "",
      DiscordProfile: "",
      XboxProfile: "",
      PsProfile: "",
      StateShortCode: "",
      CountryName: "",
      About: "",
      Followers: "",
      InGameUserID: null,
      steamId: "",
      usrname: "afgsd",
      showSteamSuccess: false,
    };
  }
  componentWillUnmount() {
    clearInterval(intervalID);
  }
  componentDidMount() {
    setInterval(() => {
      const a = this.props.userProfile && this.props.userProfile.UserID;
      const b = IsUserProfileLocalStorage();
      const c = getLocalStorage(Constants.userToken);
      if ((a || b) && c) {
        // console.log("have id and token");
        this.setState({ isUserLoggedIn: true });
      } else {
        this.setState({ isUserLoggedIn: false });
      }
    }, 100);
    // window.scrollTo(0, 0);
    // window.location.reload(1);
  }

  private handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { name, value },
    } = e;
    this.setState({ [name]: value });
  };

  onLogout = () => {
    store.dispatch(userLogOut(""));
    removeFromLocalStorage(Constants.userToken);
    removeFromLocalStorage(Constants.userProfile);
    // this.props.history.push("/");
    window.location.href = "/";
  };

  render() {
    // const { UserID } =this.props.userProfile.UserID;
    const { zIndex } = this.props.misc;
    return (
      <div id="header" className={`${zIndex ? "negative-zindex" : ""}`}>
        <div className="header-inner">
          <a className="header-logo" href="https://indieesportsleague.com/">
            <img
              aria-hidden="true"
              src={logo}
              alt="images/global-header-logo.png"
            />
            <span className="accessible">Indie Esports League</span>
          </a>

          <div className="header-nav">
            <ul className="header-nav-primary" role="navigation">
              {/* <li className="nav-item">
						<a href="/leagues">leagues</a>
					</li> */}
              <li className="nav-item">
                <a href="https://indieesportsleague.com/games/">Games</a>
              </li>
              <li className="nav-item">
                <a href="https://indieesportsleague.com/about/">About</a>
              </li>
              <li className="nav-item mobile-only">
                <a href="https://indieesportsleague.com/contact">Contact</a>
              </li>
              {/* <li className="nav-item account">
						<span>
							<a href="/login">Log in</a> /
							<a href="/register">Sign Up</a>
						</span>
						<svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50">
							<path id="global-header-account"
								  d="M0,50V43.751c0-6.876,11.25-12.5,25-12.5s25,5.625,25,12.5V50ZM12.5,12.5A12.5,12.5,0,1,1,25,25,12.5,12.5,0,0,1,12.5,12.5Z"
								  fill="#fff"/>
						</svg>
					</li> */}
              {!this.state.isUserLoggedIn ? (
                <li className="nav-item account">
                  <span>
                    <Link to={{ pathname: `/login` }}>Log in</Link> /
                    <Link to={{ pathname: `/register` }}>Sign Up</Link>
                  </span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="50"
                    height="50"
                    viewBox="0 0 50 50"
                  >
                    <path
                      id="global-header-account"
                      d="M0,50V43.751c0-6.876,11.25-12.5,25-12.5s25,5.625,25,12.5V50ZM12.5,12.5A12.5,12.5,0,1,1,25,25,12.5,12.5,0,0,1,12.5,12.5Z"
                      fill="#fff"
                    />
                  </svg>
                </li>
              ) : (
                <li className="nav-item account">
                  <span>
                    <Link to={{ pathname: `/user` }}>Account</Link> /
                    <Link to={{ pathname: `/` }} onClick={this.onLogout}>
                      Log out
                    </Link>
                  </span>
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="50"
                    height="50"
                    viewBox="0 0 50 50"
                  >
                    <path
                      id="global-header-account"
                      d="M0,50V43.751c0-6.876,11.25-12.5,25-12.5s25,5.625,25,12.5V50ZM12.5,12.5A12.5,12.5,0,1,1,25,25,12.5,12.5,0,0,1,12.5,12.5Z"
                      fill="#fff"
                    />
                  </svg>
                </li>
              )}
            </ul>
          </div>

          <button className="header-nav-toggle" type="button">
            <span className="toggle-box">
              <span className="toggle-inner"></span>
            </span>
          </button>
        </div>
      </div>
    );
  }
}

export default connect(mapStateToProps, {})(Header);
