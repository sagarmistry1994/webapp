import React from "react";
import { connect } from "react-redux";
import { IUserProfileNewDetails } from "./../utils/interfacePools";
import { IAppState } from "../reducer";
import { RouteComponentProps, Link } from "react-router-dom";
import store from "../store";
import { userLogOut, getUserInfo } from "../utils/actionDispatcher";

import axios from "axios";
import {
  removeFromLocalStorage,
  // isUserLogged,
  getLocalStorage,
  setLocalStorage,
  IsUserProfileLocalStorage,
  haveErrors,
} from "../utils/commonUtils";
import Constants from "../utils/Constants";
import NotLoggedIn from "./common/NotLoggedIn";
import { userUrl, putMethod, postMethod } from "../utils/apis";
import LoadingData from "./common/LoadingData";

import { userEdit } from "./../utils/Constants";
import TextInput from "./Form/TextInput";
// import ScrollToTop from "./common/ScrollToTop";

type IUserProfileProps = any;

interface LinkProps {
  userProfile: IUserProfileNewDetails;
}

export interface IErrors {
  [key: string]: string;
}

interface IUserProfProps extends RouteComponentProps<{ id: string }> {}

type Props = IUserProfileProps & LinkProps & IUserProfProps;

const mapStateToProps = (
  state: IAppState,
  ownProps: IUserProfileProps
): LinkProps => {
  return {
    userProfile: state.user,
  };
};

interface IUserState {
  isUserLoggedIn?: boolean;
  profileData?: any;
  showLoading?: boolean;
  showEditScreen?: boolean;
  errors: IErrors;
  [key: string]: any;
  errorFromApi?: string;
  btnLoader?: boolean;
  usrname?: string;
  submitted?: boolean;
  steamId?: string;
  noUserDetail?: boolean;
  showSteamSuccess?: boolean;
}

class editProfile extends React.Component<Props, IUserState> {
  constructor(props: Props) {
    super(props);
    const errors: IErrors = {};
    this.state = {
      errors,
      profileData: props.userProfile ? props.userProfile : {},
      UserName: null,
      ProfileDisplayName: props.userProfile
        ? props.userProfile.ProfileDisplayName
        : null,
      ProfilePictureUrl: null,
      FacebookProfile: null,
      TwitterProfile: null,
      InstagramProfile: null,
      SteamProfile: null,
      DiscordProfile: null,
      XboxProfile: null,
      PsProfile: null,
      StateShortCode: null,
      CountryName: null,
      About: null,
      Followers: "",
      InGameUserID: null,
      steamId: "",
      usrname: "afgsd",
      showSteamSuccess: false,
    };
  }

  componentDidMount() {
    const a = this.props.userProfile && this.props.userProfile.UserID;
    const b = IsUserProfileLocalStorage();
    const c = getLocalStorage(Constants.userToken);
    if ((a || b) && c) {
      // console.log("have id and token");
      this.setState({ isUserLoggedIn: true }, () => this.getUser());
    } else {
      this.setState({ isUserLoggedIn: false });
    }
    window.scrollTo(0, 0);
  }

  handleSteamLogin = () => {
    const popupWindow = window.open(
      process.env.REACT_APP_API_URL + "/auth/steam",
      "_blank",
      "width=800, height=600"
    );
    window.focus && popupWindow && popupWindow.focus();
    this.steamCallack();
  };

  private steamCallack = () => {
    window.addEventListener("message", (event) => {
      if (event.origin !== process.env.REACT_APP_API_URL) return;

      const { token, ok, data } = event.data;
      if (ok) {
        console.log(token);
        console.log(data);
        data &&
          this.setState({ steamId: data }, () => {
            this.sendSteamData();
          });
      }
    });
  };

  private sendSteamData = async (): Promise<void> => {
    const { profileData, steamId } = this.state;
    let data = {
      UserID: profileData.UserID.toString(),
      SteamID: steamId,
    };
    console.log(data);
    let res = await postMethod(userUrl, data, true);
    if (res && res.response && res.response.status === 404) {
      console.log(res.response);
    } else if (res && res.data && res.data.statuscode === 200) {
      console.log(res.data);
      this.setState({ showSteamSuccess: true }, () => {
        setTimeout(() => {
          this.setState({ showSteamSuccess: false });
        }, 1000);
      });
    } else {
      console.log(res);
    }
  };

  private handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { name, value },
    } = e;
    this.setState({ [name]: value });
  };

  handleSubmit = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
    e.preventDefault();
    const {
      profileData,
      UserName,
      ProfileDisplayName,
      ProfilePictureUrl,
      FacebookProfile,
      TwitterProfile,
      InstagramProfile,
      SteamProfile,
      DiscordProfile,
      XboxProfile,
      PsProfile,
      About,
      InGameUserID,
    } = this.state;
    this.setState(
      {
        errorFromApi: "",
        errors: {
          ...this.state.errors,
        },
      },
      () => {
        if (!haveErrors(this.state.errors)) {
          const form = {
            UserID: profileData.UserID.toString(),
            UserName,
            ProfileDisplayName,
            ProfilePictureUrl,
            FacebookProfile,
            TwitterProfile,
            InstagramProfile,
            SteamProfile,
            DiscordProfile,
            XboxProfile,
            PsProfile,
            About,
            InGameUserID,
          };
          // console.log("good to go");
          this.submitForm(form);
          // let res = await putMethod(userUrl,form,true)
          // console.log(form);
        } else {
          console.log("have errors");
        }
      }
    );
  };

  submitForm = async (form: any): Promise<void> => {
    this.setState({ btnLoader: true });
    let res = await putMethod(userUrl, form, true);
    if (res && res.response && res.response.status === 404) {
      // console.log("user not found");
      this.setState({ btnLoader: false });
    } else if (res && res.response && res.response.status === 500) {
      this.setState({ btnLoader: false });
    } else if (res && res.data && res.data.statuscode === 200) {
      // successfully updated
      this.setState({ btnLoader: false, submitted: true }, () => {
        this.getUserDetails(form.UserID);
        setTimeout(() => {
          this.setState({ submitted: false, showEditScreen: false });
          this.props.history.push("/user");
        }, 1000);
      });
    }
  };

  getUser = async (): Promise<void> => {
    if (this.props.userProfile && this.props.userProfile.UserID) {
      const userId = this.props.userProfile.UserID;
      this.getUserDetails(userId);
    } else {
      let userId = IsUserProfileLocalStorage();
      userId && this.getUserDetails(userId);
    }
  };

  getUserDetails = async (id: number): Promise<void> => {
    const token = getLocalStorage(Constants.userToken);
    const head = {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    };
    this.setState({ showLoading: true });
    const url = `${userUrl}${id}`;
    let res = await axios
      .get(url, { headers: head })
      .then((res) => {
        return res;
      })
      .catch((err) => {
        console.log(err);
        return err;
      });
    if (res && res.response && res.response.status === 404) {
      // console.log("data not found");
      // store.dispatch(userLogOut(''))
      this.setState({ showLoading: false, noUserDetail: true });
    } else if (res && res.response && res.response.status === 401) {
      // store.dispatch(userLogOut(''))
      this.setState({ showLoading: false, noUserDetail: true });
    } else if (res && res.response && res.response.status === 500) {
      // store.dispatch(userLogOut(''))
      this.setState({ showLoading: false, noUserDetail: true });
      // console.log("server error");
    } else if (res && res.data && res.data.statuscode === 200) {
      const response = res.data.data[0];
      setLocalStorage(Constants.userProfile, JSON.stringify(response));
      store.dispatch(getUserInfo(response));
      this.setState(
        {
          profileData: this.props.userProfile,
          UserName: this.props.userProfile.UserName,
          ProfileDisplayName: this.props.userProfile.ProfileDisplayName,
          ProfilePictureUrl: this.props.userProfile.ProfilePictureUrl,
          FacebookProfile: this.props.userProfile.FacebookProfile,
          TwitterProfile: this.props.userProfile.TwitterProfile,
          InstagramProfile: this.props.userProfile.InstagramProfile,
          SteamProfile: this.props.userProfile.SteamProfile,
          DiscordProfile: this.props.userProfile.DiscordProfile,
          XboxProfile: this.props.userProfile.XboxProfile,
          PsProfile: this.props.userProfile.PsProfile,
          About: this.props.userProfile.About,
          InGameUserID: this.props.userProfile.InGameUserID,
        },
        () => this.setState({ showLoading: false, noUserDetail: false })
      );
      // console.log(response);
    }
  };

  onLogout = () => {
    store.dispatch(userLogOut(""));
    removeFromLocalStorage(Constants.userToken);
    removeFromLocalStorage(Constants.userProfile);
    this.props.history.push("/");
  };

  showEditScreen = () => {
    this.setState({ showEditScreen: true });
  };

  closeEditScreen = () => {
    this.setState({ showEditScreen: false });
  };

  render() {
    // const { UserID } = this.props.userProfile;
    const { steamId, showSteamSuccess } = this.state;
    return (
      <>
        {!this.state.isUserLoggedIn ? (
          <NotLoggedIn />
        ) : this.state.noUserDetail ? (
          <>
            <div className="notlogged-in">
              <p>
                Error Encountered..Plz{" "}
                <Link to={{ pathname: "/login" }}>Login</Link> again
              </p>
            </div>
          </>
        ) : this.state.profileData && this.state.profileData ? (
          <>
            <div className="game-bg-wrapperprofile">
              {/* <div className="component component-masthead userprofile ">
                    <div className="component-inner">

                    </div>
                    
                  </div> */}

              <div className="component component-user-profile">
                {/* {UserID && UserID ? (
                    <div className="logout-btn" onClick={this.onLogout}>
                      LOGOUT
                    </div>
                  ) : null} */}
                <div className="component-inner">
                  {/* <ScrollToTop /> */}
                  {this.state.submitted ? (
                    <div className="form-success">
                      <p className="loading">Updated successfully</p>
                    </div>
                  ) : null}
                  <div className="user-profile-wrapper">
                    <div className="container">
                      <>
                        <div className="user-edit-profile">
                          <div className="container">
                            <div className="col-sm-12 col-xs-12">
                              <p className="edit-btn">
                                {/* <span onClick={this.closeEditScreen}>
                                    Back to Profile
                        </span> */}
                                <Link to={{ pathname: "/user" }}>
                                  <span>Back to Profile</span>
                                </Link>
                              </p>
                            </div>
                            <div className="col-sm-12 col-xs-12">
                              <div className="steam-wrap">
                                {showSteamSuccess ? (
                                  <div
                                    className="alert alert-success"
                                    role="alert"
                                  >
                                    Steam Connected
                                  </div>
                                ) : null}
                                <button
                                  className="steam"
                                  onClick={this.handleSteamLogin}
                                  disabled={steamId ? true : false}
                                >
                                  {steamId
                                    ? "Steam Connected"
                                    : "Connect Steam"}
                                </button>
                              </div>

                              <form
                                className="user-edit-form"
                                onSubmit={this.handleSubmit}
                              >
                                <div className="row">
                                  <div className="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                    <TextInput
                                      type="text"
                                      label="username"
                                      error={
                                        this.state.errors[
                                          `${userEdit.UserName}`
                                        ]
                                      }
                                      name={`${userEdit.UserName}`}
                                      onChange={this.handleChange}
                                      value={
                                        this.state.UserName &&
                                        this.state.UserName
                                      }
                                    />
                                    <TextInput
                                      type="text"
                                      label="profile Display name"
                                      error={
                                        this.state.errors[
                                          `${userEdit.ProfileDisplayName}`
                                        ]
                                      }
                                      name={`${userEdit.ProfileDisplayName}`}
                                      onChange={this.handleChange}
                                      autoComplete={"off"}
                                      value={
                                        this.state.ProfileDisplayName &&
                                        this.state.ProfileDisplayName
                                      }
                                    />
                                    <TextInput
                                      type="text"
                                      label="facebook profile link"
                                      error={
                                        this.state.errors[
                                          `${userEdit.FacebookProfile}`
                                        ]
                                      }
                                      name={`${userEdit.FacebookProfile}`}
                                      onChange={this.handleChange}
                                      autoComplete={"off"}
                                      value={
                                        this.state.FacebookProfile &&
                                        this.state.FacebookProfile
                                      }
                                    />
                                    <TextInput
                                      type="text"
                                      label="instagram profile link"
                                      error={
                                        this.state.errors[
                                          `${userEdit.InstagramProfile}`
                                        ]
                                      }
                                      name={`${userEdit.InstagramProfile}`}
                                      onChange={this.handleChange}
                                      autoComplete={"off"}
                                      value={
                                        this.state.InstagramProfile &&
                                        this.state.InstagramProfile
                                      }
                                    />
                                    <TextInput
                                      type="text"
                                      label="twitter profile link"
                                      error={
                                        this.state.errors[
                                          `${userEdit.TwitterProfile}`
                                        ]
                                      }
                                      name={`${userEdit.TwitterProfile}`}
                                      onChange={this.handleChange}
                                      autoComplete={"off"}
                                      value={
                                        this.state.TwitterProfile &&
                                        this.state.TwitterProfile
                                      }
                                    />
                                  </div>
                                  <div className="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                    <TextInput
                                      type="text"
                                      label="psn profile link"
                                      error={
                                        this.state.errors[
                                          `${userEdit.PsProfile}`
                                        ]
                                      }
                                      name={`${userEdit.PsProfile}`}
                                      onChange={this.handleChange}
                                      autoComplete={"off"}
                                      value={
                                        this.state.PsProfile &&
                                        this.state.PsProfile
                                      }
                                    />
                                    <TextInput
                                      type="text"
                                      label="steam profile link"
                                      error={
                                        this.state.errors[
                                          `${userEdit.SteamProfile}`
                                        ]
                                      }
                                      name={`${userEdit.SteamProfile}`}
                                      onChange={this.handleChange}
                                      autoComplete={"off"}
                                      value={
                                        this.state.SteamProfile &&
                                        this.state.SteamProfile
                                      }
                                    />
                                    <TextInput
                                      type="text"
                                      label="discord profile link"
                                      error={
                                        this.state.errors[
                                          `${userEdit.DiscordProfile}`
                                        ]
                                      }
                                      name={`${userEdit.DiscordProfile}`}
                                      onChange={this.handleChange}
                                      autoComplete={"off"}
                                      value={
                                        this.state.DiscordProfile &&
                                        this.state.DiscordProfile
                                      }
                                    />
                                    <TextInput
                                      type="text"
                                      label="xbox profile link"
                                      error={
                                        this.state.errors[
                                          `${userEdit.XboxProfile}`
                                        ]
                                      }
                                      name={`${userEdit.XboxProfile}`}
                                      onChange={this.handleChange}
                                      autoComplete={"off"}
                                      value={
                                        this.state.XboxProfile &&
                                        this.state.XboxProfile
                                      }
                                    />
                                  </div>
                                </div>

                                <div
                                  className="reg-error-parent"
                                  style={{ marginBottom: "10px" }}
                                >
                                  <p className="reg-error">
                                    {this.state.errorFromApi}
                                  </p>
                                </div>

                                <div className="form-group edit-submit">
                                  <button className="register-btn full-width">
                                    {this.state.btnLoader ? (
                                      <span className="loader-btn"></span>
                                    ) : (
                                      `Update`
                                    )}
                                  </button>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </>
        ) : (
          <LoadingData isLoading={this.state.showLoading} />
        )}
      </>
    );
  }
}

export default connect(mapStateToProps, {})(editProfile);
