import React from "react";
import game1 from "./../assets/profile-leaderboard.jpg";
import { IUserProfileNewDetails } from "../utils/interfacePools";

interface Iprops {
  details?: IUserProfileNewDetails;
}
function lookup() {
  let data = [];
  for (let i = 0; i < 11; i++) {
    data.push(
      <li
        className="leaderboard-item animation fade-in animation-queued"
        key={i}
      >
        <div className="photo">
          <img src={game1} alt="spacemachine" />
        </div>
        <div className="text">
          <h3 className="game-title">1993 SPACE MACHINE</h3>
          <h4 className="leaderboard-title">HIT ACCURACY</h4>
          <p className="ranking">3rd position</p>
          <p className="points">2394 pts</p>
          <p className="offset">134 its clear of 4th</p>
        </div>
      </li>
    );
  }
  return data;
}
function ProfileGameDetails(props: Iprops) {
  return (
    <>
      <ul className="leaderboards">{lookup()}</ul>
    </>
  );
}

export default ProfileGameDetails;
