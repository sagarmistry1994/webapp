import React from "react";
// import { IRegisterState } from "../utils/interfacePools";
import Constants, { errorMsg } from "./../utils/Constants";
import { userRegistrationUrl, postMethod } from "../utils/apis";
// import { Link } from "react-router-dom";
import TextInput from "../components/Form/TextInput";
import {
  handleEmail,
  handlePassword,
  handleConfirmPassword,
  handleUserName,
} from "../components/Form/FormValdiations";
import { removeFromLocalStorage } from "../utils/commonUtils";
import store from "../store";
import { userLogOut } from "../utils/actionDispatcher";
import HeaderWB from "./HeaderWB";
import FooterWB from "./FooterWB";

type IRegisterProps = any;

// const UserFields = Constants.UserFields;

export interface IErrors {
  [key: string]: string;
}
export interface IRegisterState {
  errors: IErrors;
  submitted?: boolean;
  email: string;
  password: string;
  confirmpassword: string;
  errorFromApi?: string;
  [key: string]: any;
  btnLoader?: boolean;
}

class RegisterWB extends React.Component<IRegisterProps, IRegisterState> {
  constructor(props: IRegisterProps) {
    super(props);
    const errors: IErrors = {};
    this.state = {
      errors,
      email: "",
      username: "",
      password: "",
      confirmpassword: "",
      errorFromApi: "",
    };
  }

  componentDidMount() {
    store.dispatch(userLogOut(""));
    removeFromLocalStorage(Constants.userToken);
    removeFromLocalStorage(Constants.userProfile);
  }

  private handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { name, value },
    } = e;
    this.setState({ [name]: value.trim() });
  };

  private haveErrors(errors: IErrors) {
    let haveError: boolean = false;
    Object.keys(errors).forEach((key: string) => {
      if (errors[key].length > 0) {
        haveError = true;
      }
    });
    return haveError;
  }

  handleSubmit = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
    e.preventDefault();
    const { email, username, password, confirmpassword } = this.state;

    this.setState(
      {
        errorFromApi: "",
        errors: {
          ...this.state.errors,
          email: handleEmail(email),
          username: handleUserName(username, "username"),
          password: handlePassword(password),
          confirmpassword: handleConfirmPassword(confirmpassword, password),
        },
      },
      () => {
        if (!this.haveErrors(this.state.errors)) {
          const form = {
            emailId: email.toLowerCase(),
            password,
            ProfileDisplayName: username,
          };
          this.submitCall(form);
        } else {
          console.log("have errors");
        }
      }
    );
  };

  submitCall = async (param: object) => {
    this.setState({ btnLoader: true });
    let res = await postMethod(userRegistrationUrl, param);

    if (res && res.response && res.response.status === 404) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.forbidden });
    } else if (res && res.response && res.response.status === 400) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.generic });
    } else if (res && res.response && res.response.status === 408) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.userName });
    } else if (res && res.response && res.response.status === 409) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.registered });
    } else if (res && res.response && res.response.status === 500) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.server });
    } else if (res && res.data && res.data.statuscode === 200) {
      this.postRegister();
    } else if (res && !res.status) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.noNetwork });
    }
  };

  postRegister = () => {
    this.setState({ btnLoader: false, submitted: true }, () => {
      setTimeout(() => {
        this.props.history.push("/loginwb");
      }, 1000);
    });
  };

  render() {
    return (
      <>
      <HeaderWB/>
        <div className="register-wrapper">
          <div className="container-fluid register-container">
            <div className="col-sm-12 col-xs-12">
              <div className="register-section-wrapper">
                <form className="form-horizontal" onSubmit={this.handleSubmit}>
                  <div className="component component-page-title">
                    <div className="component-inner">
                      <h1>REGISTER</h1>
                    </div>
                  </div>
                  <div className="format-messages">
                    {/* {
                    this.state.errors["email"] || this.state.errors["password"] ? */}
                    <ul className="msg-error">
                      {this.state.errors["email"] && (
                        <li>{this.state.errors["email"]}</li>
                      )}
                      {this.state.errors["username"] && (
                        <li>{this.state.errors["username"]}</li>
                      )}
                      {this.state.errors["password"] && (
                        <li>{this.state.errors["password"]}</li>
                      )}
                      {this.state.errors["confirmpassword"] && (
                        <li>{this.state.errors["confirmpassword"]}</li>
                      )}
                      {this.state.errorFromApi && (
                        <li>{this.state.errorFromApi}</li>
                      )}
                    </ul>
                  </div>
                  {/* {this.state.submitted ? (
                    <div className="form-success">
                      <span className="loading"> Registration successful </span>
                    </div>
                  ) : null} */}
                  <TextInput
                    placeholder="user@example.com"
                    type="email"
                    label="Email *"
                    onChange={this.handleChange}
                    name={"email"}
                    // error={this.state.errors["email"]}
                    autoComplete="off"
                    autofocus={true}
                  />
                  {/* <div className='usernamevalid'> */}
                  <TextInput
                    placeholder="Enter a username"
                    type="text"
                    label="Choose Username *"
                    onChange={this.handleChange}
                    name={"username"}
                    value={this.state.username && this.state.username}
                    // error={this.state.errors["email"]}
                    autoComplete="off"
                    autofocus={false}
                  />
                  {/* <div className="tooltip"><img src={require('../assets/info-red.png')}/>
                  <span className="tooltiptext">Standard username may contain letters(a-z, A-Z) and numbers(0-9).</span>
                </div>
                  </div> */}

                  <TextInput
                    placeholder="your password"
                    type="password"
                    label="Password *"
                    onChange={this.handleChange}
                    // error={this.state.errors["password"]}
                    name={"password"}
                    autoComplete="off"
                    value={this.state.password && this.state.password}
                    autofocus={false}
                  />
                  <TextInput
                    placeholder="confirm password"
                    type="password"
                    label="Repeat Password*"
                    onChange={this.handleChange}
                    name={"confirmpassword"}
                    // error={this.state.errors["confirmpassword"]}
                    autoComplete="off"
                    value={this.state.confirmpassword && this.state.confirmpassword}
                    autofocus={false}
                  />

                  {/* <p className="help">Profile</p>
                  <TextInput
                    placeholder="First Last"
                    type="text"
                    label='Your Name *'
                    name={"name"}
                    // error={this.state.errors["confirmpassword"]}
                    autoComplete="off"
                  />
                  <TextInput
                    placeholder="Enter a username"
                    type="text"
                    label='Username *'
                    name={"username"}
                    // error={this.state.errors["confirmpassword"]}
                    autoComplete="off"
                  />
                  <TextInput
                    placeholder="State, Country"
                    type="text"
                    label='Location'
                    name={"location"}
                    // error={this.state.errors["confirmpassword"]}
                    autoComplete="off"
                  />
                  <p className="help">Social</p>
                  <TextInput
                    placeholder="https://twitter.com/username"
                    type="text"
                    label='Twitter'
                    name={"twitter"}
                    // error={this.state.errors["confirmpassword"]}
                    autoComplete="off"
                  />
                  <TextInput
                    placeholder="https://facebook.com/username"
                    type="text"
                    label='Facebook'
                    name={"facebook"}
                    // error={this.state.errors["confirmpassword"]}
                    autoComplete="off"
                  />
                  <TextInput
                    placeholder="https://instagram.com/username"
                    type="text"
                    label='Instagram'
                    name={"instagram"}
                    // error={this.state.errors["confirmpassword"]}
                    autoComplete="off"
                  /> */}
                  {/* <div
                    className="reg-error-parent"
                    style={{ marginBottom: "10px" }}
                  >
                    <p className="reg-error">{this.state.errorFromApi}</p>
                  </div> */}
                  <div className="form-group">
                    <button className="register-btn full-width">
                      {" "}
                      {this.state.btnLoader ? (
                        <span className="loader-btn"></span>
                      ) : (
                        `CREATE YOUR ACCOUNT!`
                      )}
                    </button>
                  </div>
                  {/* <div className="body-text">
                    Already a member?
                    <Link className="link bold" to={{ pathname: "/login" }}>
                      Log In
                    </Link>
                  </div> */}
                  {/* <div className="body-text extralink">
                    You already have an account ?
                    <Link className="link bold" to={{ pathname: "/login" }}>
                      Login
                </Link>
                  </div> */}
                  <p className="help">
                    YOU WILL RECEIVE A CONFIRMATION EMAIL
                    <br />
                    WITH A LINK TO YOUR ACCOUNT
                  </p>
                </form>
              </div>
            </div>
          </div>
        </div>
        <FooterWB/>
      </>
    );
  }
}

export default RegisterWB;
