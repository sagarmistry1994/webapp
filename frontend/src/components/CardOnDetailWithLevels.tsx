import React from "react";
import { capitaliseWord } from "../utils/commonUtils";
import { Link } from "react-router-dom";
import { levelBasedLb, getMethod } from "../utils/apis";
import store from "../store";
import { getLevelBasedLb } from "../utils/actionDispatcher";

interface IState {
  [key: string]: any;
}
class CardOnDetailWithLevels extends React.Component<any, IState> {
  constructor(props: any) {
    super(props);
    this.state = {
      noData: false,
      loading: false,
      subtTitle: this.props.subTitle,
    };
  }

  componentDidMount() {
    if (this.props.hasLevels) {
      const level = 1;
      const tournament = this.props.TournamentID;
      const url = `${levelBasedLb}${Number(level)}/${Number(tournament)}/1`;
      this.getDataBasedOnLevel(url);
    }
  }

  private handleChangeLevel = (e: any, tournamentID: number) => {
    const startvalue = this.props.subTitle;
    var dataset = e.target.options[e.target.selectedIndex].dataset;
    const level = e.target.value;
    const tournament = tournamentID;
    this.setState({
      changelevel: startvalue?.split("on")[0] + " on " + dataset.valuename,
    });
    const url = `${levelBasedLb}${Number(level)}/${Number(tournament)}/1`;
    this.getDataBasedOnLevel(url);
  };
  getDataBasedOnLevel = async (url: string): Promise<void> => {
    this.setState({ loading: true, noData: false, currentLevelData: [] });
    if (url) {
      let res = await getMethod(url);
      this.setState({ loading: false });
      if (res && res.data && res.data.statuscode === 200) {
        let data = res.data.data;
        // console.log('get resopnse',data)
        store.dispatch(getLevelBasedLb(data ? data[0] : {}));
        this.setState({
          currentLevelData: data,
          noData: false,
          // subttitle:data[0]['subtitle']
        });
      } else if (
        res &&
        res.response &&
        (res.response.status === 404 || res.response.status === 400)
      ) {
        this.setState({
          currentLevelData: [],
          noData: true,
          subtitle: "",
        });
      } else if (res && !res.status) {
        this.setState({
          currentLevelData: [],
          noData: true,
          subtitle: "",
        });
      }
    }
  };

  render() {
    const link = this.props.TournamentID && this.props.TournamentID;
    const noOfplayers = this.props.noOfPlayers && this.props.noOfPlayers;
    const {
      searchTerm,
      keys,
      // desc,
      title,
      // players,
      // columnName,
      userId,
      hasLevels,
      levels,
      TournamentID,
      columns,
      subTitle,
      // noOfPlayers
    } = this.props;
    const { currentLevelData, noData, loading } = this.state;
    return (
      <>
        <div
          className="leaderboard-item format-leaderboard  animation fade-in animation-queued"
          key={keys}
        >
          <div className="wrapingheader">
            <h3>{title && title}</h3>
            <p>
              {/* {currentLevelData &&
              currentLevelData.length > 0 &&
              this.state.currentLevelData[0]["subtitle"]
                ? this.state.currentLevelData[0]["subtitle"]
                : `${this.state.changelevel}`} */}
                {this.state.changelevel ? this.state.changelevel : subTitle}
            </p>
            <div className="wrapinglevlnno">
              <div className="selectwrap">
              <select onChange={(e) => this.handleChangeLevel(e, TournamentID)}>
                {levels &&
                  levels[0].map((level: any, key: number) => {
                    return (
                      <option
                        key={key}
                        data-id={level.levelId}
                        value={level.levelId}
                        data-valuename={level.levelName}
                      >
                        {level.levelName}
                      </option>
                    );
                  })}
              </select>
            </div>
            <p>No of Players: {currentLevelData && currentLevelData.length > 0 && currentLevelData[0].noOfPlayers>0?currentLevelData[0].noOfPlayers:'0'}</p>
            </div>
            
          </div>
          {/* ) : null} */}
          {hasLevels && levels && levels.length > 0 && (
            <table className="leaderboard-rankings">
              <thead>
                <tr>
                  {/* <th>#</th>
                  <th>Player</th>
                  <th>{desc && desc}</th> */}
                  {columns.map(
                      (item: any, key: number) => {
                        return <th key={key}>{item.description}</th>;
                      }
                    )}
                </tr>
              </thead>
              <tbody>
                {currentLevelData &&
                  currentLevelData.length > 0 &&
                  currentLevelData[0].players.map((index: any, key: any) => {
                    return (
                      <tr
                        className={`${
                          (index.PlayerName &&
                            index.PlayerName.toLowerCase()
                              .trim()
                              .indexOf(
                                searchTerm && searchTerm.toLowerCase().trim()
                              ) !== -1) ||
                          userId === index.UserID
                            ? "highlighted"
                            : "false"
                        }`}
                        key={key}
                      >
                        {/* <td>{key + 1}</td>
                        {index.UserID === userId ? (
                          <td>
                            <Link to={{ pathname: "`/user" }}>
                              {capitaliseWord(index.PlayerName)}
                            </Link>
                          </td>
                        ) : (
                          <td>
                            <Link to={{ pathname: `/user/${index.UserID}` }}>
                              {capitaliseWord(index.PlayerName)}
                            </Link>
                          </td>
                        )}
                        <td>{index[columnName]}</td> */}
                        {currentLevelData[0].columns.map(
                          (item: any, key: number) => {
                            return (
                              <td key={key} data-id={index.UserID}>
                                {item.columnName === "PlayerName" ? (
                                  index.UserID === userId ? (
                                    <Link to={{ pathname: `/user` }}>
                                      {capitaliseWord(index.PlayerName)}
                                    </Link>
                                  ) : (
                                    <Link
                                      to={{
                                        pathname: `/user/${index.UserID}`,
                                      }}
                                    >
                                      {capitaliseWord(index.PlayerName)}
                                    </Link>
                                  )
                                ) : (
                                  index[item.columnName]
                                )}
                              </td>
                            );
                          }
                        )}
                      </tr>
                    );
                  })}
              </tbody>
            </table>
          )}

          {loading ? (
            <p style={{ marginTop: "2rem", textAlign: "center" }}>Loading...</p>
          ) : null}

          {noData ? (
            <p style={{ marginTop: "2rem", textAlign: "center" }}>
              No data found
            </p>
          ) : null}

          <div className="cta">
            <Link
              className="format-button secondary"
              to={{ pathname: `/leagues/${link}/leaderboard/101` }}
            >
              See full leaderboard
            </Link>
          </div>
        </div>
      </>
    );
  }
}

export default CardOnDetailWithLevels;
