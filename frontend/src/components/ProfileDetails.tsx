import React from "react";

// import fb from "./../assets/social-icons/fb.png";
// import twitter from "./../assets/social-icons/twitter.png";
// import instagram from "./../assets/social-icons/instagram.png";
// import ps from "./../assets/playstation.png";
// import steam from "./../assets/steam.png";
// import discord from "./../assets/discord.png";
// import xbox from "./../assets/xbox-one.png";
import { IUserProfileNewDetails } from "../utils/interfacePools";
import { Link } from "react-router-dom";
interface IProps {
  details?: IUserProfileNewDetails;
}

function ProfileDetails(props: IProps) {
  const { details } = props;
  return (
    <>
      <div className="bio">
        <Link to={{ pathname: "/editProfile" }}>
          <span className="format-icon edit"></span>
        </Link>
        <h2 className="username">
          {details && details.UserName ? details.UserName : `GamerID`}
        </h2>
        <p className="name">
          {details && details.ProfileDisplayName
            ? details.ProfileDisplayName
            : `Rich Thomas`}
        </p>
        <p className="location">CA, USA</p>

        <h3>SOCIAL</h3>
        <ul>
          <li>
            <a
              className=""
              href={`${
                details && details.TwitterProfile ? details.TwitterProfile : ""
              }`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24.658"
                height="20"
                viewBox="0 0 24.658 20"
              >
                <path
                  id="icon-twitter"
                  d="M45.808,22A14.256,14.256,0,0,0,60.192,7.616V6.932a11.135,11.135,0,0,0,2.466-2.6,11.372,11.372,0,0,1-2.877.822,5.33,5.33,0,0,0,2.192-2.74,12.565,12.565,0,0,1-3.151,1.233A4.892,4.892,0,0,0,55.123,2a5.147,5.147,0,0,0-5.068,5.068,2.671,2.671,0,0,0,.137,1.1A14.163,14.163,0,0,1,39.781,2.822a5.246,5.246,0,0,0-.685,2.6,5.443,5.443,0,0,0,2.192,4.247,4.618,4.618,0,0,1-2.329-.685h0a5.006,5.006,0,0,0,4.11,4.932,4.223,4.223,0,0,1-1.37.137,2.331,2.331,0,0,1-.959-.137,5.19,5.19,0,0,0,4.795,3.562,10.343,10.343,0,0,1-6.3,2.192A3.793,3.793,0,0,1,38,19.534,12.93,12.93,0,0,0,45.808,22"
                  transform="translate(-38 -2)"
                  fill="#fff"
                  fillRule="evenodd"
                ></path>
              </svg>
            </a>
          </li>
          <li>
            <a
              className=""
              href={`${
                details && details.FacebookProfile
                  ? details.FacebookProfile
                  : ""
              }`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="10.444"
                height="20"
                viewBox="0 0 10.444 20"
              >
                <path
                  id="icon-facebook"
                  d="M86.778,20V10.889h3.111l.444-3.556H86.778V5.111c0-1,.333-1.778,1.778-1.778h1.889V.111C90,.111,88.889,0,87.667,0a4.289,4.289,0,0,0-4.556,4.667V7.333H80v3.556h3.111V20Z"
                  transform="translate(-80)"
                  fill="#fff"
                  fillRule="evenodd"
                ></path>
              </svg>
            </a>
          </li>
          <li>
            <a
              className=""
              href={`${
                details && details.InstagramProfile
                  ? details.InstagramProfile
                  : ""
              }`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="20"
                height="20"
                viewBox="0 0 20 20"
              >
                <path
                  id="icon-instagram"
                  d="M10,1.778a30.662,30.662,0,0,1,4,.111,5.154,5.154,0,0,1,1.889.333,3.9,3.9,0,0,1,1.889,1.889A5.154,5.154,0,0,1,18.111,6c0,1,.111,1.333.111,4a30.662,30.662,0,0,1-.111,4,5.154,5.154,0,0,1-.333,1.889,3.9,3.9,0,0,1-1.889,1.889A5.154,5.154,0,0,1,14,18.111c-1,0-1.333.111-4,.111a30.662,30.662,0,0,1-4-.111,5.154,5.154,0,0,1-1.889-.333,3.9,3.9,0,0,1-1.889-1.889A5.154,5.154,0,0,1,1.889,14c0-1-.111-1.333-.111-4a30.662,30.662,0,0,1,.111-4,5.154,5.154,0,0,1,.333-1.889A3.991,3.991,0,0,1,3,3a1.879,1.879,0,0,1,1.111-.778A5.154,5.154,0,0,1,6,1.889a30.662,30.662,0,0,1,4-.111M10,0A32.83,32.83,0,0,0,5.889.111,6.86,6.86,0,0,0,3.444.556,4.35,4.35,0,0,0,1.667,1.667,4.35,4.35,0,0,0,.556,3.444,5.063,5.063,0,0,0,.111,5.889,32.83,32.83,0,0,0,0,10a32.83,32.83,0,0,0,.111,4.111,6.86,6.86,0,0,0,.444,2.444,4.35,4.35,0,0,0,1.111,1.778,4.35,4.35,0,0,0,1.778,1.111,6.86,6.86,0,0,0,2.444.444A32.83,32.83,0,0,0,10,20a32.83,32.83,0,0,0,4.111-.111,6.86,6.86,0,0,0,2.444-.444,4.662,4.662,0,0,0,2.889-2.889,6.86,6.86,0,0,0,.444-2.444C19.889,13,20,12.667,20,10a32.83,32.83,0,0,0-.111-4.111,6.86,6.86,0,0,0-.444-2.444,4.35,4.35,0,0,0-1.111-1.778A4.35,4.35,0,0,0,16.556.556,6.86,6.86,0,0,0,14.111.111,32.83,32.83,0,0,0,10,0m0,4.889A5.029,5.029,0,0,0,4.889,10,5.111,5.111,0,1,0,10,4.889m0,8.444A3.274,3.274,0,0,1,6.667,10,3.274,3.274,0,0,1,10,6.667,3.274,3.274,0,0,1,13.333,10,3.274,3.274,0,0,1,10,13.333m5.333-9.889a1.222,1.222,0,1,0,1.222,1.222,1.233,1.233,0,0,0-1.222-1.222"
                  fill="#fff"
                  fillRule="evenodd"
                ></path>
              </svg>
            </a>
          </li>
        </ul>
      </div>
      {/* <div className="top-ranking">
        <h3>HIGHEST RANK</h3>
        <p className="rank">1st</p>
        <p className="cta"><a href="/leagues">SEE LEADERBOARD</a></p>
      </div> */}
      <div className="platform-ids">
        <h3>
          <Link to={{ pathname: "/editProfile" }}>
            <span className="format-icon edit"></span>
          </Link>{" "}
          PLATFORM ID’s
        </h3>
        <ul className="platforms">
          <li className="platform-item">
            <a
              className="usr-social-link"
              href={`${
                details && details.SteamProfile ? details.SteamProfile : ""
              }`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <div className="format-icon steam"></div>
              <div className="username">STEAM</div>
            </a>
          </li>
          <li className="platform-item">
            <a
              className="usr-social-link"
              href={`${
                details && details.DiscordProfile ? details.DiscordProfile : ""
              }`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <div className="format-icon discord"></div>
              <div className="username">DISCORD</div>
            </a>
          </li>
          <li className="platform-item">
            <a
              className="usr-social-link"
              href={`${
                details && details.XboxProfile ? details.XboxProfile : ""
              }`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <div className="format-icon xbox"></div>
              <div className="username">XBOX</div>
            </a>
          </li>
          <li className="platform-item">
            <a
              className="usr-social-link"
              href={`${details && details.PsProfile ? details.PsProfile : ""}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              <div className="format-icon playstation"></div>
              <div className="username">PLAYSTATION</div>
            </a>
          </li>
        </ul>
      </div>
    </>
  );
}

export default ProfileDetails;
