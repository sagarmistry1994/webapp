import React from "react";
import { Link } from "react-router-dom";

function NotLoggedIn(props: any) {
  return (
    <div className="notlogged-in">
      <p>Please login to continue</p>
      <p>
        <Link to={{ pathname: "/login" }}>Login</Link>
      </p>
    </div>
  );
}

export default NotLoggedIn;