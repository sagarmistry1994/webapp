import React, { Component } from "react";
// import arrow from "../../assets/scroll-top.svg";

interface IState {
  is_visible: boolean;
}

export default class ScrollToTop extends Component<any, IState> {
  constructor(props: any) {
    super(props);
    this.state = {
      is_visible: false,
    };
  }

  componentDidMount() {
    var scrollComponent = this;
    document.addEventListener("scroll", function () {
      scrollComponent.toggleVisibility();
    });
  }

  toggleVisibility() {
    if (window.pageYOffset > 30) {
      this.setState({
        is_visible: true,
      });
    } else {
      this.setState({
        is_visible: false,
      });
    }
  }

  scrollToTop() {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  }

  componentWillUnmount() {
    var scrollComponent = this;
    document.removeEventListener("scroll", function () {
      scrollComponent.toggleVisibility();
    });
  }

  render() {
    // const { is_visible } = this.state;
    const { zindex } = this.props;
    return (
      <>
      {/* <div className="scroll-to-top">
        {is_visible && (
          <div className="arrow-holder" onClick={() => this.scrollToTop()}>
            <img src={arrow} alt="arrow" />
          </div>
        )}
      </div> */}
      {/* { is_visible && */}
      <div className={`content-top ${zindex ? 'negative-zindex': ''}`} onClick={() => this.scrollToTop()}>
		{/* <a href={" "}> */}
			<svg xmlns="http://www.w3.org/2000/svg" width="29" height="45">
				<g transform="rotate(-90 13.5 12.5)">
					<circle fill="none" cx="13" cy="13" r="13"/>
					<path className="fill"
					      d="M0 13C0 5.832 5.832 0 13 0s13 5.832 13 13-5.832 13-13 13S0 20.168 0 13zm25 0c0-6.617-5.383-12-12-12S1 6.383 1 13s5.383 12 12 12 12-5.383 12-12z"/>
				</g>
				<path className="fill-tint"
				      d="M14.689 10.831L13.858 10l-.831.831-4.784 4.784.831.831 4.784-4.784 4.785 4.784.83-.831-4.784-4.784z"/>
				<path className="fill"
				      d="M18.643 17.153l-4.785-4.785-4.784 4.784-1.538-1.538 6.322-6.322 6.322 6.322-1.537 1.539zM8.95 15.615l.124.124 4.784-4.784 4.785 4.784.123-.124-4.908-4.908-4.908 4.908z"/>
				<g className="fill">
					<path d="M3.63 42v-8.839H.328v-1.183h7.943v1.183H4.956V42H3.63zM8.976 37.119c0-1.663.447-2.966 1.34-3.906.893-.941 2.046-1.412 3.459-1.412.925 0 1.759.221 2.502.663s1.31 1.059 1.698 1.849c.391.791.585 1.688.585 2.69 0 1.017-.205 1.926-.615 2.728s-.991 1.409-1.743 1.822a4.985 4.985 0 01-2.434.618c-.943 0-1.787-.228-2.529-.684s-1.306-1.078-1.688-1.866-.575-1.622-.575-2.502zm1.367.021c0 1.208.325 2.159.974 2.854.649.694 1.464 1.042 2.444 1.042.998 0 1.819-.351 2.464-1.053.646-.702.968-1.697.968-2.987 0-.815-.138-1.527-.413-2.137a3.18 3.18 0 00-1.211-1.414 3.267 3.267 0 00-1.787-.503c-.939 0-1.747.322-2.423.968-.678.645-1.016 1.721-1.016 3.23zM20.269 42V31.979h3.78c.666 0 1.174.032 1.524.096.492.082.905.238 1.237.469.332.229.601.552.803.967.203.415.305.87.305 1.367 0 .853-.271 1.573-.813 2.164-.542.59-1.522.885-2.939.885h-2.57V42h-1.327zm1.326-5.257h2.591c.857 0 1.465-.159 1.825-.479s.54-.768.54-1.347c0-.419-.105-.778-.318-1.076a1.48 1.48 0 00-.837-.592c-.224-.06-.636-.089-1.237-.089h-2.563v3.583z"/>
				</g>
			</svg>
		{/* </a> */}
	</div>
  {/* } */}
      </>
    );
  }
}
