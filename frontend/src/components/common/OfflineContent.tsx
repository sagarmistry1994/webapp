import React from "react";

function OfflineContent(props: any) {
  return (
    <React.Fragment>
        <div className="container">
            <div className="col-sm-12 col-xs-12">
                    <p className="offline-p">Sorry! you seem to be offline. Connect to the internet and try again</p>
            </div>

        </div>
        </React.Fragment>
    );
}

export default OfflineContent;
