import React from "react";

function LoadingData(props: any) {
  return (
    <>
      {props.isLoading && props.isLoading ? (
        <div className="loading-data">
          <div>
            <span className="loading">Loading data</span>
          </div>
        </div>
      ) : null}
    </>
  );
}

export default LoadingData;
