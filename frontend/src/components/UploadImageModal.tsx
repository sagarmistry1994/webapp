import React from "react";

function UpdateImageModal(props: any) {
  const {
    close,
    handleProfileClick,
    handleHeaderClick,
    profileImagesList,
    headerImagesList,
    selectedHeaderImage,
    selectedProfileImage,
  } = props;
  return (
    <React.Fragment>
      <div className="component component-modal-profile-image">
        <div className="component-inner">
          <h2>CHOOSE PROFILE</h2>

          <ul className="profile-images">
            {profileImagesList &&
              profileImagesList.map((index: any, key: number) => {
                return (
                  <li
                    className={`profile-image ${
                      selectedProfileImage === index.ProfileImageID
                        ? "selected"
                        : ""
                    }`}
                    key={key}
                    data-key={index.ProfileImageID}
                    onClick={() => handleProfileClick(index.ProfileImageID)}
                  >
                    <img src={index.ProfileImageURL} alt="profile" />
                  </li>
                );
              })}
          </ul>

          <h2>CHOOSE HEADER</h2>

          <ul className="profile-headers">
            {headerImagesList &&
              headerImagesList.map((index: any, key: number) => {
                return (
                  <li
                    className={`profile-header ${
                      selectedHeaderImage === index.HeaderImageID
                        ? "selected"
                        : ""
                    }`}
                    key={key}
                    data-key={index.HeaderImageID}
                    onClick={() => handleHeaderClick(index.HeaderImageID)}
                  >
                    <img src={index.HeaderImageURL} alt="header" />
                  </li>
                );
              })}
          </ul>
        </div>
        <button className="close" onClick={close}>
          ×
        </button>
      </div>
    </React.Fragment>
  );
}

export default UpdateImageModal;
