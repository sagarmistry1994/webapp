import React from "react";
import TextInput from "./formFields/TextInput";
import { postMethod } from "../utils/apis";
// import axios from "axios";
// import { Link } from "react-router-dom";
import { errorMsg } from "../utils/Constants";
import {
  handleEmail,
  handlePassword,
  handleConfirmPassword,
  validateOtp,
} from "./Form/FormValdiations";
import { haveErrors } from "../utils/commonUtils";
import HeaderWB from "./HeaderWB";
import FooterWB from "./FooterWB";
export interface IErrors {
  [key: string]: string;
}
export interface IState {
  errors: IErrors;
  submitted?: boolean;
  email: string;
  password: string;
  confirmpassword: string;
  errorFromApi?: string;
  [key: string]: any;
  btnLoader?: boolean;
  isOtpScreen?: boolean;
  isPassScreen?: boolean;
  pwdChanged?: boolean;
  count: number;
  resendOtp: boolean;
  resendBtnLoader?: boolean;
  isResendOtpSuccess?: boolean;
}

let generateOtp = "https://backend.indieesportsleague.com/api/v1/users/otp";
let verfiyOtp = "https://backend.indieesportsleague.com/api/v1/users/verifyotp";
let changePwd =
  "https://backend.indieesportsleague.com/api/v1/users/changepasword";

class ForgotPasswordWB extends React.Component<any, IState> {
  constructor(props: any) {
    super(props);

    const errors: IErrors = {};
    this.state = {
      errors,
      email: "",
      password: "",
      confirmpassword: "",
      errorFromApi: "",
      isOtpScreen: false,
      isPassScreen: false,
      pwdChanged: false,
      count: 30,
      resendOtp: false,
      resendBtnLoader: false,
      isResendOtpSuccess: false,
    };
  }

  componentWillUnmount() {
    this.startTimer();
  }

  startTimer = () => {
    clearInterval();
    var time = setInterval(() => {
      this.setState({ count: this.state.count - 1 });
      if (this.state.count === 0) {
        clearInterval(time);
        this.setState({ resendOtp: true });
      }
    }, 1000);
  };

  private resendOtpFn = async (): Promise<void> => {
    this.setState({ resendBtnLoader: true });
    const { email } = this.state;
    const form = {
      email: email.toLowerCase(),
    };
    let res = await postMethod(generateOtp, form);

    if (res && res.response && res.response.status === 403) {
      this.setState({
        btnLoader: false,
        errorFromApi: errorMsg.inCorrect,
      });
    } else if (res && res.response && res.response.status === 404) {
      this.setState({
        resendBtnLoader: false,
        errorFromApi: errorMsg.notFound,
      });
    } else if (res && res.response && res.response.status === 409) {
      this.setState({
        resendBtnLoader: false,
        errorFromApi: errorMsg.registered,
      });
    } else if (res && res.response && res.response.status === 500) {
      this.setState({ resendBtnLoader: false, errorFromApi: errorMsg.server });
    } else if (res && res.data && res.data.statuscode === 200) {
      this.setState(
        { resendBtnLoader: false, isResendOtpSuccess: true },
        () => {
          setTimeout(() => {
            this.setState(
              { isResendOtpSuccess: false, resendOtp: false, count: 30 },
              () => {
                this.startTimer();
              }
            );
          }, 2000);
        }
      );
    } else if (res && !res.status) {
      this.setState({
        resendBtnLoader: false,
        errorFromApi: errorMsg.noNetwork,
      });
    }
  };

  private handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const {
      target: { name, value },
    } = e;
    this.setState({ [name]: value.trim() });
  };

  handleEmail = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
    e.preventDefault();
    const { email } = this.state;

    this.setState(
      {
        errorFromApi: "",
        errors: {
          ...this.state.errors,
          email: handleEmail(email),
        },
      },
      () => {
        if (!haveErrors(this.state.errors)) {
          const form = {
            email: email.toLowerCase(),
          };
          this.submitEmail(form);
        } else {
          console.log("have errors");
        }
      }
    );
    // this.setState({ isOtpScreen: true });
  };

  submitEmail = async (param: object) => {
    this.setState({ btnLoader: true });
    let res = await postMethod(generateOtp, param);

    if (res && res.response && res.response.status === 404) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.notFound });
    } else if (res && res.response && res.response.status === 409) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.registered });
    } else if (res && res.response && res.response.status === 500) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.server });
    } else if (res && res.data && res.data.statuscode === 200) {
      this.setState({ btnLoader: false, isOtpScreen: true }, () => {
        this.startTimer();
      });
    } else if (res && !res.staus) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.noNetwork });
    }
  };

  handleOtp = async (e: React.FormEvent<HTMLFormElement>): Promise<void> => {
    e.preventDefault();
    const { otp, email } = this.state;

    this.setState(
      {
        errorFromApi: "",
        errors: {
          ...this.state.errors,
          otp: validateOtp(otp),
        },
      },
      () => {
        if (!haveErrors(this.state.errors)) {
          const form = {
            email: email.toLowerCase(),
            otp,
          };
          this.submitOtp(form);
        } else {
          console.log("have errors");
        }
      }
    );
  };

  submitOtp = async (param: object): Promise<void> => {
    this.setState({ btnLoader: true });
    let res = await postMethod(verfiyOtp, param);

    if (res && res.response && res.response.status === 401) {
      this.setState({
        btnLoader: false,
        errorFromApi: errorMsg.optNotVerified,
      });
    }
    if (res && res.response && res.response.status === 403) {
      this.setState({
        btnLoader: false,
        errorFromApi: errorMsg.inCorrect,
      });
    } else if (res && res.response && res.response.status === 409) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.registered });
    } else if (res && res.response && res.response.status === 500) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.server });
    } else if (res && res.data && res.data.statuscode === 200) {
      this.setState({
        btnLoader: false,
        isOtpScreen: false,
        isPassScreen: true,
      });
    } else if (res && !res.staus) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.noNetwork });
    }
  };

  handlePassword = async (
    e: React.FormEvent<HTMLFormElement>
  ): Promise<void> => {
    e.preventDefault();
    const { email, password, confirmpassword, otp } = this.state;

    this.setState(
      {
        errorFromApi: "",
        errors: {
          ...this.state.errors,
          password: handlePassword(password),
          confirmpassword: handleConfirmPassword(confirmpassword, password),
        },
      },
      () => {
        if (!haveErrors(this.state.errors)) {
          const form = {
            email: email.toLowerCase(),
            otp,
            password,
          };
          this.submitPassword(form);
        } else {
          console.log("have errors");
        }
      }
    );
  };

  submitPassword = async (param: object) => {
    this.setState({ btnLoader: true });
    let res = await postMethod(changePwd, param);

    if (res && res.response && res.response.status === 401) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.generic });
    } else if (res && res.response && res.response.status === 409) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.registered });
    } else if (res && res.response && res.response.status === 500) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.server });
    } else if (res && res.data && res.data.statuscode === 200) {
      this.setState({ btnLoader: false, pwdChanged: true }, () => {
        setTimeout(() => {
          this.props.history.push("/loginwb");
        }, 1000);
      });
    } else if (res && !res.staus) {
      this.setState({ btnLoader: false, errorFromApi: errorMsg.noNetwork });
    }
  };

  render() {
    const {
      btnLoader,
      isPassScreen,
      isOtpScreen,
      pwdChanged,
      count,
      resendOtp,
      resendBtnLoader,
      isResendOtpSuccess,
    } = this.state;
    return (
      <>
      <HeaderWB/>
      <div className="register-wrapper">
        <div className="container-fluid register-container">
          <div className="col-sm-12 col-xs-12">
            <div className="register-section-wrapper">
              <div className="component component-page-title forgotpassword">
                <div className="component-inner">
                  <h1>
                    {isOtpScreen
                      ? "Verify OTP"
                      : isPassScreen
                      ? "Reset password"
                      : "Recover Password"}
                  </h1>
                </div>
              </div>
              <form
                className="form-horizontal"
                onSubmit={
                  isOtpScreen
                    ? this.handleOtp
                    : isPassScreen
                    ? this.handlePassword
                    : this.handleEmail
                }
              >
                <div className="component component-page-title forgotpasswordtxt">
                  <div className="component-inner">
                    <div className="format-messages">
                      {/* {
                    this.state.errors["email"] || this.state.errors["password"] ? */}
                      <ul className="msg-error">
                        {this.state.errors["email"] && (
                          <li>{this.state.errors["email"]}</li>
                        )}
                        {this.state.errors["password"] && (
                          <li>{this.state.errors["password"]}</li>
                        )}
                        {this.state.errors["confirmpassword"] && (
                          <li>{this.state.errors["confirmpassword"]}</li>
                        )}
                        {this.state.errors["otp"] && (
                          <li>{this.state.errors["otp"]}</li>
                        )}
                        {this.state.errorFromApi && (
                          <li>{this.state.errorFromApi}</li>
                        )}
                      </ul>
                      {pwdChanged ? (
                        <p className="msg-success">
                          Password Changed Successfully
                        </p>
                      ) : null}
                      {isResendOtpSuccess ? (
                        <span className="msg-success">
                          OTP sent to your email
                        </span>
                      ) : null}
                    </div>
                    <p className="help">
                      {isOtpScreen
                        ? "Please enter the One Time Password (OTP) we have mailed you"
                        : isPassScreen
                        ? // ? "Please enter your new password"
                          "enter a new password for your account"
                        : "enter your email address to receive OTP and reset your password"}
                    </p>
                  </div>

                  {!isPassScreen && !isOtpScreen && (
                    <>
                      <TextInput
                        placeholder="user@example.com"
                        label="Email"
                        type="email"
                        onChange={this.handleChange}
                        name="email"
                        // error={this.state.errors["email"]}
                        autoComplete="off"
                        // value={this.state.email && this.state.email}
                        autofocus={true}
                      />
                    </>
                  )}
                  {isOtpScreen && (
                    <>
                      <TextInput
                        placeholder="otp"
                        label="Enter OTP"
                        type="number"
                        onChange={this.handleChange}
                        name="otp"
                        // error={this.state.errors["otp"]}
                        autoComplete="off"
                        // value={this.state.otp && this.state.otp}
                        autofocus={true}
                      />
                      {resendOtp ? (
                        <div className="resend-btn" onClick={this.resendOtpFn}>
                          {resendBtnLoader ? (
                            <span className="loader-btn"></span>
                          ) : (
                            <span className="resndtxt">Resend OTP</span>
                          )}
                        </div>
                      ) : (
                        <div className="resend-btn-count">
                          {"Resend OTP in "}
                          {count}
                        </div>
                      )}
                    </>
                  )}
                  {isPassScreen && (
                    <>
                      <TextInput
                        placeholder="your password"
                        label="Password"
                        type={"password"}
                        onChange={this.handleChange}
                        // error={this.state.errors["password"]}
                        name="password"
                        autoComplete="off"
                        customClass={"wolabel"}
                        value={this.state.password && this.state.password}
                        autofocus={true}
                      />
                      <TextInput
                        placeholder="confirm password"
                        label="Confirm"
                        type={"password"}
                        onChange={this.handleChange}
                        // error={this.state.errors["confirmpassword"]}
                        name="confirmpassword"
                        autoComplete="off"
                        value={this.state.confirmpassword && this.state.confirmpassword}
                        autofocus={false}
                      />
                    </>
                  )}

                  <div className="form-group">
                    <button className="register-btn full-width">
                      {btnLoader ? (
                        <span className="loader-btn"></span>
                      ) : isOtpScreen ? (
                        "Verify"
                      ) : isPassScreen ? (
                        "save new password"
                      ) : (
                        "Submit"
                      )}
                    </button>
                  </div>
                  {/* <div className="body-text extralink">
                    You already have an account ?
                <Link className="link bold" to={{ pathname: "/login" }}>
                      Login
                </Link>
                  </div> */}
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <FooterWB/>
    </>
    );
  }
}

export default ForgotPasswordWB;
