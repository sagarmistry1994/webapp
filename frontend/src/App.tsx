import React from "react";
// import Header from "./components/Header";
// import Footer from "./components/Footer";
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Switch,
} from "react-router-dom";
// import Home from "./components/Home";
// import About from "./components/About";
import GameDetail from "./components/GameDetail";
import GameDetailWB from "./components/GameDetail1";
import GameLeaderBoard from "./components/GameLeaderBoard";
import GameLeaderBoardWB from "./components/GameLeaderBoardWB";
import Register from "./components/Register";
// import RegisterWB from "./components/RegisterWB";
import Contact from "./components/contact";
import NotFound from "./components/NotFound";
import Login from "./components/Login";
import newLogin from "./components/anc";
// import newLoginWB from "./components/ancWB";
// import UserProfile from "./components/UserProfile";
import editProfile from "./components/editprofile";
// import editprofileWB from "./components/editprofileWB";
import userPublic from "./components/userpublic";
// import userPublicwb from "./components/userpublicWB";
import VerificationEmail from "./components/VerificationEmail";
// import VerificationEmailWB from "./components/VerificationEmailWB";
import ForgotPassword from "./components/ForgotPassword";
// import ForgotPasswordWB from "./components/ForgotPasswordWB";

function App() {
  return (
    <Router>
      <div className="App">
        {/* <Header /> */}
        {/* <div className="content-body-wrapper"> */}
        <Switch>
          {/*start 1993 space machine */}
            {/* <Route exact path="/leagues/101" component={GameDetail} /> */}
            <Route exact path="/">
              <Redirect to="/leagues/101" />
            </Route>
            <Route exact path="/leagues/101" component={GameDetail} />
            <Route path="/leagues/:id/leaderboard/101" component={GameLeaderBoard} />
            <Route exact path="/register" component={Register} />
            <Route exact path="/contact" component={Contact} />
            <Route exact path="/login1" component={Login} />
            <Route exact path="/login" component={newLogin} />
            {/* <Route exact path="/user" component={UserProfile} /> */}
            <Route exact path="/verify-email" component={VerificationEmail} />
            <Route exact path="/forgot-password" component={ForgotPassword} />
            <Route exact path="/user" component={editProfile} />
            <Route exact path="/user/:id" component={userPublic} />

           
          {/*End 1993 space machine */}
          {/*Start Wave Break */}
            <Route exact path="/leagues/102" component={GameDetailWB} />
            <Route path="/leagues/:id/leaderboard/102" component={GameLeaderBoardWB} />
            {/* <Route exact path="/userwb" component={editprofileWB} />
            <Route exact path="/loginwb" component={newLoginWB} />
            <Route exact path="/registerwb" component={RegisterWB} />
            <Route exact path="/forgot-passwordwb" component={ForgotPasswordWB} />
            <Route exact path="/verify-emailwb" component={VerificationEmailWB} />
            <Route exact path="/userwb/:id" component={userPublicwb} /> */}
          {/*End Wave Break */}
          <Route path="" component={NotFound} />
        </Switch>
        {/* </div> */}
        {/* <Footer /> */}
      </div>
    </Router>
  );
}

export default App;
