import { Actions } from "../utils/actions";
import { IMiscPool } from "../utils/interfacePools";
import { IAction } from "../utils/actionDispatcher";

const initState: IMiscPool = {
    zIndex: false,
};

const miscReducer = (state = initState, action: IAction): IMiscPool => {
  switch (action.type) {
    case Actions.NEGATIVE_Z_INDEX:
      return {
          ...state,
          zIndex: !state.zIndex
      };
    default:
      return state;
  }
};

export default miscReducer;