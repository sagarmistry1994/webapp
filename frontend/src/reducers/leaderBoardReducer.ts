import { Actions } from "../utils/actions";
import { ILeaderBoardRow, ILevelBasedLb } from "../utils/interfacePools";
import { IAction } from "../utils/actionDispatcher";
// const initState: ILeaderBoardRow[] | [] = [];

interface IState {
  leaderboard: ILeaderBoardRow | [];
  levelBasedLb: ILevelBasedLb[] | [];
}

const initState: IState = {
  leaderboard: [],
  levelBasedLb: [],
};

const leaderBoardReducer = (state = initState, action: IAction): IState => {
  const isLevelLb = (param: any): any => {
    const result = state.levelBasedLb.some(
      (item) => item.tournamentID === param.tournamentID
    );
    return result;
  };

  switch (action.type) {
    case Actions.GET_LEADERBOARD:
      return {
        ...state,
        leaderboard: action.payload,
      };
    case Actions.GET_LEVEL_BASED_LEADERBOARD:
      if (!isLevelLb(action.payload)) {
        return {
          ...state,
          levelBasedLb: [...state.levelBasedLb, action.payload],
        };
      } else {
        return {
          ...state,
        };
      }
    default:
      return state;
  }
};

export default leaderBoardReducer;

// state.levelBasedLb.some(item => item.tournamedID !== action.payload.tournamentID) && action.payload
