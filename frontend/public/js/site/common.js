/*
 * Site Common JS
 */
jQuery(function () {

	// Page Navigation
	let initPageNav = function () {
		jQuery('.header-nav-toggle').on('click', function (e) {
			e.preventDefault();

			document.documentElement.classList.toggle('header-nav-active');
		});

		jQuery('#content .content-top > a').on('click', function (e) {
			e.preventDefault();

			jQuery('html, body').animate({scrollTop: 0}, 1000);
		});
	};
	initPageNav();


	// Header Height Animation
	let initPageScrolled = function () {
		let pageScrolled = false;
		let pageScrolledLoop = function () {
			// Add 'page-scrolled' class to body node if scroll position is greater than 75px
			if (75 < window.scrollY) {
				if (!pageScrolled) {
					pageScrolled = true;
					document.documentElement.classList.add('page-scrolled');
				}
			} else {
				if (pageScrolled) {
					pageScrolled = false;
					document.documentElement.classList.remove('page-scrolled');
				}
			}
			requestAnimationFrame(pageScrolledLoop);
		}
		requestAnimationFrame(pageScrolledLoop);

	};
	initPageScrolled();


	// Sliding labels on form fields
	let initSlidingLabels = function () {
		// Add event handlers to each label and field pair
		jQuery('label.format-sliding-label:not(.init)').each(function (index, node) {
			let label = node;
			let field = document.getElementById(label.getAttribute('data-for'));

			// Rise label on focus, and clear error state
			jQuery(field).on('focus', function (e) {
				label.classList.add('active');
				label.classList.add('focus');
			}).on('blur', function (e) {
				if (this.value.length < 1) {
					label.classList.remove('active');
				}
				label.classList.remove('focus');
			});

			// Set initial state on load
			if (0 < field.value.length) {
				label.classList.add('active');
			}

			// Set as initialised
			label.classList.add('init');
		});
	};
	initSlidingLabels();
	
	// var $win = $(window);
	// 	var $footer = $("#footer");
	// 	$(window).scroll(function(){
	// 		$('.content-top').css('bottom','35px')
	// 		var windowHeight = $win.height();
	// 		var footerTop = $footer.offset().top - $win.scrollTop();
	// 		var visibleHeight = Math.min(windowHeight, $win.scrollTop()) - footerTop;
	// 		if(visibleHeight >= '35'){
	// 			$('.content-top').css('bottom',visibleHeight)
	// 		}else{
	// 			$('.content-top').css('bottom','35px')
	// 		}
			
	// 	});
});